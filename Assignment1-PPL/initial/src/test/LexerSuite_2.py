import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):

    def test_identifier(self):
        self.assertTrue(TestLexer.checkLexeme(
            """name
            """,
            """name,<EOF>""",
            100))

        self.assertTrue(TestLexer.checkLexeme(
            """name
            class_room
            SCHOOL
            """,
            """name,class_room,SCHOOL,<EOF>""",
            101))

        self.assertTrue(TestLexer.checkLexeme(
            """_hello
            """,
            """_hello,<EOF>""",
            102))

        self.assertTrue(TestLexer.checkLexeme(
            """123abc
            """,
            """123,abc,<EOF>""",
            103))

        self.assertTrue(TestLexer.checkLexeme(
            """int fibonaci () {}
            """,
            """int,fibonaci,(,),{,},<EOF>""",
            104))

        self.assertTrue(TestLexer.checkLexeme(
            """class2B
            """,
            """class2B,<EOF>""",
            105))

        self.assertTrue(TestLexer.checkLexeme(
            """abc != aBc
            """,
            """abc,!=,aBc,<EOF>""",
            106))

    def test_comment(self):
        self.assertTrue(TestLexer.checkLexeme(
            """// this is a commend
            123 int
            """,
            """123,int,<EOF>""",
            107))

        self.assertTrue(TestLexer.checkLexeme(
            """int a; // variable a // with integer type
            """,
            """int,a,;,<EOF>""",
            108))

        self.assertTrue(TestLexer.checkLexeme(
            """10
            /* hello world */
            text
            """,
            """10,text,<EOF>""",
            109))

        self.assertTrue(TestLexer.checkLexeme(
            """void main () {
            /* first block comment /*and another*/ */
            }
            """,
            """void,main,(,),{,*,/,},<EOF>""",
            110))

        self.assertTrue(TestLexer.checkLexeme(
            """int x; // inline and /* block // and inline /* and block */
            """,
            """int,x,;,<EOF>""",
            111))

        self.assertTrue(TestLexer.checkLexeme(
            """/*
            this is a block comment
            // inline comment has no special meanning
            */
            int x;
            """,
            """int,x,;,<EOF>""",
            112))

    def test_INTLIT(self):
        self.assertTrue(TestLexer.checkLexeme(
            """123
            """,
            """123,<EOF>""",
            113))

        self.assertTrue(TestLexer.checkLexeme(
            """123 + 321 = 444
            """,
            """123,+,321,=,444,<EOF>""",
            114))

        self.assertTrue(TestLexer.checkLexeme(
            """int a;
               a = -3;
            """,
            """int,a,;,a,=,-,3,;,<EOF>""",
            115))

        self.assertTrue(TestLexer.checkLexeme(
            """int a123;
            """,
            """int,a123,;,<EOF>""",
            116))

        self.assertTrue(TestLexer.checkLexeme(
            """2147483648
            """,
            """2147483648,<EOF>""",
            117))

    def test_FLOATLIT(self):
        self.assertTrue(TestLexer.checkLexeme(
            """1.0
            """,
            """1.0,<EOF>""",
            118))

        self.assertTrue(TestLexer.checkLexeme(
            """float x;
            x = 6.9e3;
            """,
            """float,x,;,x,=,6.9e3,;,<EOF>""",
            119))

        self.assertTrue(TestLexer.checkLexeme(
            """00e-3
            """,
            """00e-3,<EOF>""",
            120))

        self.assertTrue(TestLexer.checkLexeme(
            """this = -6.9e0;
            """,
            """this,=,-,6.9e0,;,<EOF>""",
            121))

        self.assertTrue(TestLexer.checkLexeme(
            """123E-3
            """,
            """123E-3,<EOF>""",
            122))

        self.assertTrue(TestLexer.checkLexeme(
            """.33e-3
            """,
            """.33e-3,<EOF>""",
            123))

        self.assertTrue(TestLexer.checkLexeme(
            """12.34.56
            """,
            """12.34,.56,<EOF>""",
            124))

        self.assertTrue(TestLexer.checkLexeme(
            """123.321e69
            """,
            """123.321e69,<EOF>""",
            125))

        self.assertTrue(TestLexer.checkLexeme(
            """.
            """,
            """Error Token .""",
            126))

        self.assertTrue(TestLexer.checkLexeme(
            """.00e020
            """,
            """.00e020,<EOF>""",
            127))

        self.assertTrue(TestLexer.checkLexeme(
            """1e
            """,
            """1,e,<EOF>""",
            128))

    def test_STRINGLIT(self):
        self.assertTrue(TestLexer.checkLexeme(
            """string x;
            x = "hello world";
            """,
            """string,x,;,x,=,hello world,;,<EOF>""",
            129))

        self.assertTrue(TestLexer.checkLexeme(
            """"this is
            """,
            """Unclosed String: this is""",
            130))

        self.assertTrue(TestLexer.checkLexeme(
            """text = "get the \\n new line" ;
            """,
            """text,=,get the \\n new line,;,<EOF>""",
            131))

        self.assertTrue(TestLexer.checkLexeme(
            """text = "this "problem 123"
            """,
            """text,=,this ,problem,123,Unclosed String: """,
            132))
        self.assertTrue(TestLexer.checkLexeme(
            """a = "";
            """,
            """a,=,,;,<EOF>""",
            134))

        self.assertTrue(TestLexer.checkLexeme(
            """text = 'string' ;
            """,
            """text,=,Error Token '""",
            135))

        self.assertTrue(TestLexer.checkLexeme(
            """x = "this string has "another string""
            """,
            """x,=,this string has ,another,string,,<EOF>""",
            136))

        self.assertTrue(TestLexer.checkLexeme(
            """x = "this string has 'another string'"
            """,
            """x,=,this string has 'another string',<EOF>""",
            137))

    def test_Seperators(self):
        self.assertTrue(TestLexer.checkLexeme(
            """( ) text 123;
            """,
            """(,),text,123,;,<EOF>""",
            138))

        self.assertTrue(TestLexer.checkLexeme(
            """][ void main []
            """,
            """],[,void,main,[,],<EOF>""",
            139))

        self.assertTrue(TestLexer.checkLexeme(
            """{
            int x;
            x = 3;
            }
            """,
            """{,int,x,;,x,=,3,;,},<EOF>""",
            140))

        self.assertTrue(TestLexer.checkLexeme(
            """, ; , ;
            """,
            """,,;,,,;,<EOF>""",
            141))

        self.assertTrue(TestLexer.checkLexeme(
            """{{int a; a = 1;}{"hello"}}
            """,
            """{,{,int,a,;,a,=,1,;,},{,hello,},},<EOF>""",
            142))

    def test_Operators(self):
        self.assertTrue(TestLexer.checkLexeme(
            """+
        -
        *
        /
        %
            """,
            """+,-,*,/,%,<EOF>""",
            143))

        self.assertTrue(TestLexer.checkLexeme(
            """&&
        ||
        !
            """,
            """&&,||,!,<EOF>""",
            144))

        self.assertTrue(TestLexer.checkLexeme(
            """==
        !=
        <
        <=
        >
        >=
            """,
            """==,!=,<,<=,>,>=,<EOF>""",
            145))

        self.assertTrue(TestLexer.checkLexeme(
            """text = "hello world" ;
            """,
            """text,=,hello world,;,<EOF>""",
            146))

    def test_Keywords(self):
        self.assertTrue(TestLexer.checkLexeme(
            """true and false
            """,
            """true,and,false,<EOF>""",
            147))

        self.assertTrue(TestLexer.checkLexeme(
            """int
        float
        boolean
        string
        void
            """,
            """int,float,boolean,string,void,<EOF>""",
            148))

        self.assertTrue(TestLexer.checkLexeme(
            """strig a;
            """,
            """strig,a,;,<EOF>""",
            149))

    def test_Others(self):
        self.assertTrue(TestLexer.checkLexeme(
            """.a
            """,
            """Error Token .""",
            150))

        self.assertTrue(TestLexer.checkLexeme(
            """$999 = Iphone X
            """,
            """Error Token $""",
            151))

        self.assertTrue(TestLexer.checkLexeme(
            """x = int getInt(3);
            """,
            """x,=,int,getInt,(,3,),;,<EOF>""",
            152))

        self.assertTrue(TestLexer.checkLexeme(
            """main = f = i = 100;
            """,
            """main,=,f,=,i,=,100,;,<EOF>""",
            153))

        self.assertTrue(TestLexer.checkLexeme(
            """int x[3];
            """,
            """int,x,[,3,],;,<EOF>""",
            154))

        self.assertTrue(TestLexer.checkLexeme(
            """int a[]3;
               a[0] = 1;
            """,
            """int,a,[,],3,;,a,[,0,],=,1,;,<EOF>""",
            155))

    def test_Statements(self):
        self.assertTrue(TestLexer.checkLexeme(
            """if ( a + 1 ) a = 0
               else a = 1;
            """,
            """if,(,a,+,1,),a,=,0,else,a,=,1,;,<EOF>""",
            156))

        self.assertTrue(TestLexer.checkLexeme(
            """if a == 3;
               else (a = 0; b = 1);
            """,
            """if,a,==,3,;,else,(,a,=,0,;,b,=,1,),;,<EOF>""",
            157))

        self.assertTrue(TestLexer.checkLexeme(
            """if (a == 0) then else if (a == 1) a = 0;
            """,
            """if,(,a,==,0,),then,else,if,(,a,==,1,),a,=,0,;,<EOF>""",
            158))

        self.assertTrue(TestLexer.checkLexeme(
            """do a; b; c while 0 ;
            """,
            """do,a,;,b,;,c,while,0,;,<EOF>""",
            159))

        self.assertTrue(TestLexer.checkLexeme(
            """a += 1 while a < 3;
            """,
            """a,+,=,1,while,a,<,3,;,<EOF>""",
            160))

        self.assertTrue(TestLexer.checkLexeme(
            """do a = a + 1 while a > 3;
            """,
            """do,a,=,a,+,1,while,a,>,3,;,<EOF>""",
            161))

        self.assertTrue(TestLexer.checkLexeme(
            """for a = 3 ; a < 3 ; a++ a = "text"
            """,
            """for,a,=,3,;,a,<,3,;,a,+,+,a,=,text,<EOF>""",
            162))

        self.assertTrue(TestLexer.checkLexeme(
            """for ( a = 1 ; a < 3 ; a = a + 1 ) x = "text"
            """,
            """for,(,a,=,1,;,a,<,3,;,a,=,a,+,1,),x,=,text,<EOF>""",
            163))

        self.assertTrue(TestLexer.checkLexeme(
            """for break;
            """,
            """for,break,;,<EOF>""",
            164))

        self.assertTrue(TestLexer.checkLexeme(
            """do x = x + 1 continue while x < 0 ;
            """,
            """do,x,=,x,+,1,continue,while,x,<,0,;,<EOF>""",
            165))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() { return }
            """,
            """void,main,(,),{,return,},<EOF>""",
            166))

        self.assertTrue(TestLexer.checkLexeme(
            """i = 1;
               a = i;
               b = a;
            """,
            """i,=,1,;,a,=,i,;,b,=,a,;,<EOF>""",
            167))

        self.assertTrue(TestLexer.checkLexeme(
            """void f(int a[]) {}
            """,
            """void,f,(,int,a,[,],),{,},<EOF>""",
            168))

        self.assertTrue(TestLexer.checkLexeme(
            """void f(int a[5]) {}
            """,
            """void,f,(,int,a,[,5,],),{,},<EOF>""",
            169))

        self.assertTrue(TestLexer.checkLexeme(
            """int[] main() {
                int a[2];
                return a[];
            }
            """,
            """int,[,],main,(,),{,int,a,[,2,],;,return,a,[,],;,},<EOF>""",
            170))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
                return ;
            }
            """,
            """void,main,(,),{,return,;,},<EOF>""",
            171))

        self.assertTrue(TestLexer.checkLexeme(
            """void foo() {
                continue ;
            }
            """,
            """void,foo,(,),{,continue,;,},<EOF>""",
            172))

        self.assertTrue(TestLexer.checkLexeme(
            """void foo() {
            for ( x = 1 ; x < 3 ; x = x + 1 ) a = a + 2 ;
        }
            """,
            """void,foo,(,),{,for,(,x,=,1,;,x,<,3,;,x,=,x,+,1,),a,=,a,+,2,;,},<EOF>""",
            173))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
            if ( a == true ) return b; else return c;
        }
            """,
            """void,main,(,),{,if,(,a,==,true,),return,b,;,else,return,c,;,},<EOF>""",
            174))

        self.assertTrue(TestLexer.checkLexeme(
            """int[] foo( int b[] ) {}
            """,
            """int,[,],foo,(,int,b,[,],),{,},<EOF>""",
            175))

        self.assertTrue(TestLexer.checkLexeme(
            """int a[5], b[4], c[3], d[2], e[1];
            e[d[c[b[a[0]]]]] = 1;
            """,
            """int,a,[,5,],,,b,[,4,],,,c,[,3,],,,d,[,2,],,,e,[,1,],;,e,[,d,[,c,[,b,[,a,[,0,],],],],],=,1,;,<EOF>""",
            176))

        self.assertTrue(TestLexer.checkLexeme(
            """void main(){
                int x
            }
            """,
            """void,main,(,),{,int,x,},<EOF>""",
            177))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
            """,
            """void,main,(,),{,<EOF>""",
            178))

        self.assertTrue(TestLexer.checkLexeme(
            """void main(float a,int x[]) {}
            """,
            """void,main,(,float,a,,,int,x,[,],),{,},<EOF>""",
            179))

        self.assertTrue(TestLexer.checkLexeme(
            """int main(){
                int x, y;
                string str;
            }
            """,
            """int,main,(,),{,int,x,,,y,;,string,str,;,},<EOF>""",
            180))

        self.assertTrue(TestLexer.checkLexeme(
            """int arr[1][2];
            """,
            """int,arr,[,1,],[,2,],;,<EOF>""",
            181))

        self.assertTrue(TestLexer.checkLexeme(
            """string str = "hello world";
            """,
            """string,str,=,hello world,;,<EOF>""",
            182))

        self.assertTrue(TestLexer.checkLexeme(
            """int () {}
            """,
            """int,(,),{,},<EOF>""",
            183))

        self.assertTrue(TestLexer.checkLexeme(
            """str = " ! @ # % ^ & * ( ) ";
            """,
            """str,=, ! @ # % ^ & * ( ) ,;,<EOF>""",
            184))

        self.assertTrue(TestLexer.checkLexeme(
            """void main ( ) {return ;}
            """,
            """void,main,(,),{,return,;,},<EOF>""",
            185))

        self.assertTrue(TestLexer.checkLexeme(
            """int foo(int a, int b) {}
            """,
            """int,foo,(,int,a,,,int,b,),{,},<EOF>""",
            186))

        self.assertTrue(TestLexer.checkLexeme(
            """void foo() {
                int child(){}
            }
            """,
            """void,foo,(,),{,int,child,(,),{,},},<EOF>""",
            187))

        self.assertTrue(TestLexer.checkLexeme(
            """void main () {}
            int foo() {}
            """,
            """void,main,(,),{,},int,foo,(,),{,},<EOF>""",
            188))

        self.assertTrue(TestLexer.checkLexeme(
            """boolean = bool = { false };
            """,
            """boolean,=,bool,=,{,false,},;,<EOF>""",
            189))

        self.assertTrue(TestLexer.checkLexeme(
            """if ( a >= b ) return b;
            """,
            """if,(,a,>=,b,),return,b,;,<EOF>""",
            190))

        self.assertTrue(TestLexer.checkLexeme(
            """if ( a != b ) return a;
            """,
            """if,(,a,!=,b,),return,a,;,<EOF>""",
            191))

        self.assertTrue(TestLexer.checkLexeme(
            """if ( a != b ) return a;
            """,
            """if,(,a,!=,b,),return,a,;,<EOF>""",
            192))

        self.assertTrue(TestLexer.checkLexeme(
            """if ( a > 0 || b > 0) return 1;
            """,
            """if,(,a,>,0,||,b,>,0,),return,1,;,<EOF>""",
            193))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
                int a;
                a = 3.14159;
            }
            """,
            """void,main,(,),{,int,a,;,a,=,3.14159,;,},<EOF>""",
            194))

        self.assertTrue(TestLexer.checkLexeme(
            """void main(){
            // first block
                {
                // second block
                    {
                    // third block
                        {
                        // fourth block
                    }
                }
            }
        }
            """,
            """void,main,(,),{,{,{,{,},},},},<EOF>""",
            195))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
                int a;
                int b
            }
            """,
            """void,main,(,),{,int,a,;,int,b,},<EOF>""",
            196))

        self.assertTrue(TestLexer.checkLexeme(
                    """int f() {
            return 69;
        }
            """,
            """int,f,(,),{,return,69,;,},<EOF>""",
            197))

        self.assertTrue(TestLexer.checkLexeme(
            """void main() {
            putString("Hello World!");
        }
            """,
            """void,main,(,),{,putString,(,Hello World!,),;,},<EOF>""",
            198))

        self.assertTrue(TestLexer.checkLexeme(
            """Final "TEST"
            """,
            """Final,TEST,<EOF>""",
            199))
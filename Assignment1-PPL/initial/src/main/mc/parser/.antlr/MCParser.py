# Generated from /Users/new/Desktop/Lab/Assignment1-PPL/initial/src/main/mc/parser/MC.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\62")
        buf.write("\u0144\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\3\2\3\2\6\2E\n\2\r\2\16\2F\3\2")
        buf.write("\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3S\n\3\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\5")
        buf.write("\6d\n\6\3\7\3\7\3\7\3\7\3\7\5\7k\n\7\3\b\3\b\3\b\7\bp")
        buf.write("\n\b\f\b\16\bs\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5")
        buf.write("\t}\n\t\3\n\3\n\3\n\7\n\u0082\n\n\f\n\16\n\u0085\13\n")
        buf.write("\5\n\u0087\n\n\3\13\3\13\3\13\7\13\u008c\n\13\f\13\16")
        buf.write("\13\u008f\13\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\5\f\u009b\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r")
        buf.write("\u00a5\n\r\3\16\3\16\6\16\u00a9\n\16\r\16\16\16\u00aa")
        buf.write("\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22")
        buf.write("\5\22\u00c3\n\22\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3")
        buf.write("\24\3\24\3\24\5\24\u00cf\n\24\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\7\25\u00d7\n\25\f\25\16\25\u00da\13\25\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\7\26\u00e2\n\26\f\26\16\26\u00e5")
        buf.write("\13\26\3\27\3\27\3\27\3\27\3\27\5\27\u00ec\n\27\3\30\3")
        buf.write("\30\3\30\3\30\3\30\5\30\u00f3\n\30\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\7\31\u00fb\n\31\f\31\16\31\u00fe\13\31\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\7\32\u0106\n\32\f\32\16\32\u0109")
        buf.write("\13\32\3\33\3\33\3\33\5\33\u010e\n\33\3\34\3\34\3\34\3")
        buf.write("\34\3\34\3\34\5\34\u0116\n\34\3\35\3\35\3\36\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\5\36\u0122\n\36\3\36\3\36\3")
        buf.write("\36\3\36\3\36\7\36\u0129\n\36\f\36\16\36\u012c\13\36\3")
        buf.write("\37\3\37\3\37\3\37\5\37\u0132\n\37\3 \3 \3 \3 \3 \7 \u0139")
        buf.write("\n \f \16 \u013c\13 \5 \u013e\n \3 \3 \3!\3!\3!\2\7(*")
        buf.write("\60\62:\"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&")
        buf.write("(*,.\60\62\64\668:<>@\2\t\6\2\b\b\r\r\17\17\26\26\4\2")
        buf.write("\36\36  \3\2!$\3\2\27\30\4\2\31\32\37\37\4\2\30\30\33")
        buf.write("\33\3\2\24\25\2\u014b\2D\3\2\2\2\4R\3\2\2\2\6T\3\2\2\2")
        buf.write("\b[\3\2\2\2\nc\3\2\2\2\fj\3\2\2\2\16l\3\2\2\2\20|\3\2")
        buf.write("\2\2\22\u0086\3\2\2\2\24\u0088\3\2\2\2\26\u009a\3\2\2")
        buf.write("\2\30\u009c\3\2\2\2\32\u00a6\3\2\2\2\34\u00b0\3\2\2\2")
        buf.write("\36\u00ba\3\2\2\2 \u00bd\3\2\2\2\"\u00c0\3\2\2\2$\u00c6")
        buf.write("\3\2\2\2&\u00ce\3\2\2\2(\u00d0\3\2\2\2*\u00db\3\2\2\2")
        buf.write(",\u00eb\3\2\2\2.\u00f2\3\2\2\2\60\u00f4\3\2\2\2\62\u00ff")
        buf.write("\3\2\2\2\64\u010d\3\2\2\2\66\u0115\3\2\2\28\u0117\3\2")
        buf.write("\2\2:\u0121\3\2\2\2<\u0131\3\2\2\2>\u0133\3\2\2\2@\u0141")
        buf.write("\3\2\2\2BE\5\4\3\2CE\5\6\4\2DB\3\2\2\2DC\3\2\2\2EF\3\2")
        buf.write("\2\2FD\3\2\2\2FG\3\2\2\2GH\3\2\2\2HI\7\2\2\3I\3\3\2\2")
        buf.write("\2JK\5\b\5\2KL\5\f\7\2LM\7*\2\2MS\3\2\2\2NO\5\b\5\2OP")
        buf.write("\5\16\b\2PQ\7*\2\2QS\3\2\2\2RJ\3\2\2\2RN\3\2\2\2S\5\3")
        buf.write("\2\2\2TU\5\n\6\2UV\7.\2\2VW\7+\2\2WX\5\22\n\2XY\7,\2\2")
        buf.write("YZ\5\24\13\2Z\7\3\2\2\2[\\\t\2\2\2\\\t\3\2\2\2]d\5\b\5")
        buf.write("\2^d\7\20\2\2_`\5\b\5\2`a\7&\2\2ab\7\'\2\2bd\3\2\2\2c")
        buf.write("]\3\2\2\2c^\3\2\2\2c_\3\2\2\2d\13\3\2\2\2ek\7.\2\2fg\7")
        buf.write(".\2\2gh\7&\2\2hi\7\5\2\2ik\7\'\2\2je\3\2\2\2jf\3\2\2\2")
        buf.write("k\r\3\2\2\2lq\5\f\7\2mn\7-\2\2np\5\f\7\2om\3\2\2\2ps\3")
        buf.write("\2\2\2qo\3\2\2\2qr\3\2\2\2r\17\3\2\2\2sq\3\2\2\2tu\5\b")
        buf.write("\5\2uv\7.\2\2v}\3\2\2\2wx\5\b\5\2xy\7.\2\2yz\7&\2\2z{")
        buf.write("\7\'\2\2{}\3\2\2\2|t\3\2\2\2|w\3\2\2\2}\21\3\2\2\2~\u0083")
        buf.write("\5\20\t\2\177\u0080\7-\2\2\u0080\u0082\5\20\t\2\u0081")
        buf.write("\177\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083")
        buf.write("\u0084\3\2\2\2\u0084\u0087\3\2\2\2\u0085\u0083\3\2\2\2")
        buf.write("\u0086~\3\2\2\2\u0086\u0087\3\2\2\2\u0087\23\3\2\2\2\u0088")
        buf.write("\u008d\7(\2\2\u0089\u008c\5\4\3\2\u008a\u008c\5\26\f\2")
        buf.write("\u008b\u0089\3\2\2\2\u008b\u008a\3\2\2\2\u008c\u008f\3")
        buf.write("\2\2\2\u008d\u008b\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u0090")
        buf.write("\3\2\2\2\u008f\u008d\3\2\2\2\u0090\u0091\7)\2\2\u0091")
        buf.write("\25\3\2\2\2\u0092\u009b\5\30\r\2\u0093\u009b\5\32\16\2")
        buf.write("\u0094\u009b\5\34\17\2\u0095\u009b\5\36\20\2\u0096\u009b")
        buf.write("\5 \21\2\u0097\u009b\5\"\22\2\u0098\u009b\5$\23\2\u0099")
        buf.write("\u009b\5\24\13\2\u009a\u0092\3\2\2\2\u009a\u0093\3\2\2")
        buf.write("\2\u009a\u0094\3\2\2\2\u009a\u0095\3\2\2\2\u009a\u0096")
        buf.write("\3\2\2\2\u009a\u0097\3\2\2\2\u009a\u0098\3\2\2\2\u009a")
        buf.write("\u0099\3\2\2\2\u009b\27\3\2\2\2\u009c\u009d\7\16\2\2\u009d")
        buf.write("\u009e\7+\2\2\u009e\u009f\5&\24\2\u009f\u00a0\7,\2\2\u00a0")
        buf.write("\u00a1\5\26\f\2\u00a1\u00a4\3\2\2\2\u00a2\u00a3\7\13\2")
        buf.write("\2\u00a3\u00a5\5\26\f\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5")
        buf.write("\3\2\2\2\u00a5\31\3\2\2\2\u00a6\u00a8\7\22\2\2\u00a7\u00a9")
        buf.write("\5\26\f\2\u00a8\u00a7\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa")
        buf.write("\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2\2\2")
        buf.write("\u00ac\u00ad\7\23\2\2\u00ad\u00ae\5&\24\2\u00ae\u00af")
        buf.write("\7*\2\2\u00af\33\3\2\2\2\u00b0\u00b1\7\f\2\2\u00b1\u00b2")
        buf.write("\7+\2\2\u00b2\u00b3\5&\24\2\u00b3\u00b4\7*\2\2\u00b4\u00b5")
        buf.write("\5&\24\2\u00b5\u00b6\7*\2\2\u00b6\u00b7\5&\24\2\u00b7")
        buf.write("\u00b8\7,\2\2\u00b8\u00b9\5\26\f\2\u00b9\35\3\2\2\2\u00ba")
        buf.write("\u00bb\7\t\2\2\u00bb\u00bc\7*\2\2\u00bc\37\3\2\2\2\u00bd")
        buf.write("\u00be\7\n\2\2\u00be\u00bf\7*\2\2\u00bf!\3\2\2\2\u00c0")
        buf.write("\u00c2\7\21\2\2\u00c1\u00c3\5&\24\2\u00c2\u00c1\3\2\2")
        buf.write("\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5")
        buf.write("\7*\2\2\u00c5#\3\2\2\2\u00c6\u00c7\5&\24\2\u00c7\u00c8")
        buf.write("\7*\2\2\u00c8%\3\2\2\2\u00c9\u00ca\5(\25\2\u00ca\u00cb")
        buf.write("\7%\2\2\u00cb\u00cc\5&\24\2\u00cc\u00cf\3\2\2\2\u00cd")
        buf.write("\u00cf\5(\25\2\u00ce\u00c9\3\2\2\2\u00ce\u00cd\3\2\2\2")
        buf.write("\u00cf\'\3\2\2\2\u00d0\u00d1\b\25\1\2\u00d1\u00d2\5*\26")
        buf.write("\2\u00d2\u00d8\3\2\2\2\u00d3\u00d4\f\4\2\2\u00d4\u00d5")
        buf.write("\7\34\2\2\u00d5\u00d7\5*\26\2\u00d6\u00d3\3\2\2\2\u00d7")
        buf.write("\u00da\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d8\u00d9\3\2\2\2")
        buf.write("\u00d9)\3\2\2\2\u00da\u00d8\3\2\2\2\u00db\u00dc\b\26\1")
        buf.write("\2\u00dc\u00dd\5,\27\2\u00dd\u00e3\3\2\2\2\u00de\u00df")
        buf.write("\f\4\2\2\u00df\u00e0\7\35\2\2\u00e0\u00e2\5,\27\2\u00e1")
        buf.write("\u00de\3\2\2\2\u00e2\u00e5\3\2\2\2\u00e3\u00e1\3\2\2\2")
        buf.write("\u00e3\u00e4\3\2\2\2\u00e4+\3\2\2\2\u00e5\u00e3\3\2\2")
        buf.write("\2\u00e6\u00e7\5.\30\2\u00e7\u00e8\t\3\2\2\u00e8\u00e9")
        buf.write("\5.\30\2\u00e9\u00ec\3\2\2\2\u00ea\u00ec\5.\30\2\u00eb")
        buf.write("\u00e6\3\2\2\2\u00eb\u00ea\3\2\2\2\u00ec-\3\2\2\2\u00ed")
        buf.write("\u00ee\5\60\31\2\u00ee\u00ef\t\4\2\2\u00ef\u00f0\5\60")
        buf.write("\31\2\u00f0\u00f3\3\2\2\2\u00f1\u00f3\5\60\31\2\u00f2")
        buf.write("\u00ed\3\2\2\2\u00f2\u00f1\3\2\2\2\u00f3/\3\2\2\2\u00f4")
        buf.write("\u00f5\b\31\1\2\u00f5\u00f6\5\62\32\2\u00f6\u00fc\3\2")
        buf.write("\2\2\u00f7\u00f8\f\4\2\2\u00f8\u00f9\t\5\2\2\u00f9\u00fb")
        buf.write("\5\62\32\2\u00fa\u00f7\3\2\2\2\u00fb\u00fe\3\2\2\2\u00fc")
        buf.write("\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\61\3\2\2\2\u00fe")
        buf.write("\u00fc\3\2\2\2\u00ff\u0100\b\32\1\2\u0100\u0101\5\64\33")
        buf.write("\2\u0101\u0107\3\2\2\2\u0102\u0103\f\4\2\2\u0103\u0104")
        buf.write("\t\6\2\2\u0104\u0106\5\64\33\2\u0105\u0102\3\2\2\2\u0106")
        buf.write("\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2")
        buf.write("\u0108\63\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u010b\t\7")
        buf.write("\2\2\u010b\u010e\5\64\33\2\u010c\u010e\5\66\34\2\u010d")
        buf.write("\u010a\3\2\2\2\u010d\u010c\3\2\2\2\u010e\65\3\2\2\2\u010f")
        buf.write("\u0110\58\35\2\u0110\u0111\7&\2\2\u0111\u0112\58\35\2")
        buf.write("\u0112\u0113\7\'\2\2\u0113\u0116\3\2\2\2\u0114\u0116\5")
        buf.write("8\35\2\u0115\u010f\3\2\2\2\u0115\u0114\3\2\2\2\u0116\67")
        buf.write("\3\2\2\2\u0117\u0118\5:\36\2\u01189\3\2\2\2\u0119\u011a")
        buf.write("\b\36\1\2\u011a\u0122\5<\37\2\u011b\u0122\7.\2\2\u011c")
        buf.write("\u0122\5> \2\u011d\u011e\7+\2\2\u011e\u011f\5&\24\2\u011f")
        buf.write("\u0120\7,\2\2\u0120\u0122\3\2\2\2\u0121\u0119\3\2\2\2")
        buf.write("\u0121\u011b\3\2\2\2\u0121\u011c\3\2\2\2\u0121\u011d\3")
        buf.write("\2\2\2\u0122\u012a\3\2\2\2\u0123\u0124\f\3\2\2\u0124\u0125")
        buf.write("\7&\2\2\u0125\u0126\5&\24\2\u0126\u0127\7\'\2\2\u0127")
        buf.write("\u0129\3\2\2\2\u0128\u0123\3\2\2\2\u0129\u012c\3\2\2\2")
        buf.write("\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b;\3\2\2")
        buf.write("\2\u012c\u012a\3\2\2\2\u012d\u0132\7\5\2\2\u012e\u0132")
        buf.write("\7\7\2\2\u012f\u0132\7\6\2\2\u0130\u0132\5@!\2\u0131\u012d")
        buf.write("\3\2\2\2\u0131\u012e\3\2\2\2\u0131\u012f\3\2\2\2\u0131")
        buf.write("\u0130\3\2\2\2\u0132=\3\2\2\2\u0133\u0134\7.\2\2\u0134")
        buf.write("\u013d\7+\2\2\u0135\u013a\5&\24\2\u0136\u0137\7-\2\2\u0137")
        buf.write("\u0139\5&\24\2\u0138\u0136\3\2\2\2\u0139\u013c\3\2\2\2")
        buf.write("\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e\3")
        buf.write("\2\2\2\u013c\u013a\3\2\2\2\u013d\u0135\3\2\2\2\u013d\u013e")
        buf.write("\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0140\7,\2\2\u0140")
        buf.write("?\3\2\2\2\u0141\u0142\t\b\2\2\u0142A\3\2\2\2\37DFRcjq")
        buf.write("|\u0083\u0086\u008b\u008d\u009a\u00a4\u00aa\u00c2\u00ce")
        buf.write("\u00d8\u00e3\u00eb\u00f2\u00fc\u0107\u010d\u0115\u0121")
        buf.write("\u012a\u0131\u013a\u013d")
        return buf.getvalue()


class MCParser ( Parser ):

    grammarFileName = "MC.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "'boolean'", "'break'", "'continue'", 
                     "'else'", "'for'", "'float'", "'if'", "'int'", "'void'", 
                     "'return'", "'do'", "'while'", "'true'", "'false'", 
                     "'string'", "'+'", "'-'", "'*'", "'/'", "'!'", "'||'", 
                     "'&&'", "'=='", "'%'", "'!='", "'<'", "'<='", "'>'", 
                     "'>='", "'='", "'['", "']'", "'{'", "'}'", "';'", "'('", 
                     "')'", "','" ]

    symbolicNames = [ "<INVALID>", "BLOCK_COMMENT", "LINE_COMMENT", "INTLIT", 
                      "FPLIT", "STRINGLIT", "BOOLEAN", "BREAK", "CONTINUE", 
                      "ELSE", "FOR", "FLOAT", "IF", "INTTYPE", "VOIDTYPE", 
                      "RETURN", "DO", "WHILE", "TRUE", "FALSE", "STRING", 
                      "ADD", "SUB", "MUL", "DIV", "NOT", "OR", "AND", "EQUAL", 
                      "MODULUS", "NOTEQUAL", "LT", "LTE", "GT", "GTE", "ASSIGN", 
                      "LSB", "RSB", "LP", "RP", "SEMI", "LB", "RB", "COMMA", 
                      "ID", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_var_declaration = 1
    RULE_func_declaration = 2
    RULE_primitive_type = 3
    RULE_type_return = 4
    RULE_variable = 5
    RULE_many_variables = 6
    RULE_parameter = 7
    RULE_parameter_list = 8
    RULE_block_statement = 9
    RULE_statement = 10
    RULE_if_statement = 11
    RULE_do_while_statement = 12
    RULE_for_statement = 13
    RULE_break_statement = 14
    RULE_continue_statement = 15
    RULE_return_statement = 16
    RULE_expression_statement = 17
    RULE_exp = 18
    RULE_exp1 = 19
    RULE_exp2 = 20
    RULE_exp3 = 21
    RULE_exp4 = 22
    RULE_exp5 = 23
    RULE_exp6 = 24
    RULE_exp7 = 25
    RULE_exp8 = 26
    RULE_exp9 = 27
    RULE_operand = 28
    RULE_literal = 29
    RULE_func_call = 30
    RULE_booleanlit = 31

    ruleNames =  [ "program", "var_declaration", "func_declaration", "primitive_type", 
                   "type_return", "variable", "many_variables", "parameter", 
                   "parameter_list", "block_statement", "statement", "if_statement", 
                   "do_while_statement", "for_statement", "break_statement", 
                   "continue_statement", "return_statement", "expression_statement", 
                   "exp", "exp1", "exp2", "exp3", "exp4", "exp5", "exp6", 
                   "exp7", "exp8", "exp9", "operand", "literal", "func_call", 
                   "booleanlit" ]

    EOF = Token.EOF
    BLOCK_COMMENT=1
    LINE_COMMENT=2
    INTLIT=3
    FPLIT=4
    STRINGLIT=5
    BOOLEAN=6
    BREAK=7
    CONTINUE=8
    ELSE=9
    FOR=10
    FLOAT=11
    IF=12
    INTTYPE=13
    VOIDTYPE=14
    RETURN=15
    DO=16
    WHILE=17
    TRUE=18
    FALSE=19
    STRING=20
    ADD=21
    SUB=22
    MUL=23
    DIV=24
    NOT=25
    OR=26
    AND=27
    EQUAL=28
    MODULUS=29
    NOTEQUAL=30
    LT=31
    LTE=32
    GT=33
    GTE=34
    ASSIGN=35
    LSB=36
    RSB=37
    LP=38
    RP=39
    SEMI=40
    LB=41
    RB=42
    COMMA=43
    ID=44
    WS=45
    UNCLOSE_STRING=46
    ILLEGAL_ESCAPE=47
    ERROR_CHAR=48

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MCParser.EOF, 0)

        def var_declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Var_declarationContext)
            else:
                return self.getTypedRuleContext(MCParser.Var_declarationContext,i)


        def func_declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Func_declarationContext)
            else:
                return self.getTypedRuleContext(MCParser.Func_declarationContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_program




    def program(self):

        localctx = MCParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 66 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 66
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
                if la_ == 1:
                    self.state = 64
                    self.var_declaration()
                    pass

                elif la_ == 2:
                    self.state = 65
                    self.func_declaration()
                    pass


                self.state = 68 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INTTYPE) | (1 << MCParser.VOIDTYPE) | (1 << MCParser.STRING))) != 0)):
                    break

            self.state = 70
            self.match(MCParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Var_declarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitive_type(self):
            return self.getTypedRuleContext(MCParser.Primitive_typeContext,0)


        def variable(self):
            return self.getTypedRuleContext(MCParser.VariableContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def many_variables(self):
            return self.getTypedRuleContext(MCParser.Many_variablesContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_var_declaration




    def var_declaration(self):

        localctx = MCParser.Var_declarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_var_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 72
                self.primitive_type()
                self.state = 73
                self.variable()
                self.state = 74
                self.match(MCParser.SEMI)
                pass

            elif la_ == 2:
                self.state = 76
                self.primitive_type()
                self.state = 77
                self.many_variables()
                self.state = 78
                self.match(MCParser.SEMI)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Func_declarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_return(self):
            return self.getTypedRuleContext(MCParser.Type_returnContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def parameter_list(self):
            return self.getTypedRuleContext(MCParser.Parameter_listContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def block_statement(self):
            return self.getTypedRuleContext(MCParser.Block_statementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_func_declaration




    def func_declaration(self):

        localctx = MCParser.Func_declarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_func_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 82
            self.type_return()
            self.state = 83
            self.match(MCParser.ID)
            self.state = 84
            self.match(MCParser.LB)
            self.state = 85
            self.parameter_list()
            self.state = 86
            self.match(MCParser.RB)
            self.state = 87
            self.block_statement()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Primitive_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOOLEAN(self):
            return self.getToken(MCParser.BOOLEAN, 0)

        def INTTYPE(self):
            return self.getToken(MCParser.INTTYPE, 0)

        def FLOAT(self):
            return self.getToken(MCParser.FLOAT, 0)

        def STRING(self):
            return self.getToken(MCParser.STRING, 0)

        def getRuleIndex(self):
            return MCParser.RULE_primitive_type




    def primitive_type(self):

        localctx = MCParser.Primitive_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_primitive_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 89
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INTTYPE) | (1 << MCParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Type_returnContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitive_type(self):
            return self.getTypedRuleContext(MCParser.Primitive_typeContext,0)


        def VOIDTYPE(self):
            return self.getToken(MCParser.VOIDTYPE, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_type_return




    def type_return(self):

        localctx = MCParser.Type_returnContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_type_return)
        try:
            self.state = 97
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 91
                self.primitive_type()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 92
                self.match(MCParser.VOIDTYPE)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 93
                self.primitive_type()
                self.state = 94
                self.match(MCParser.LSB)
                self.state = 95
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_variable




    def variable(self):

        localctx = MCParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_variable)
        try:
            self.state = 104
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 99
                self.match(MCParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 100
                self.match(MCParser.ID)
                self.state = 101
                self.match(MCParser.LSB)
                self.state = 102
                self.match(MCParser.INTLIT)
                self.state = 103
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Many_variablesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VariableContext)
            else:
                return self.getTypedRuleContext(MCParser.VariableContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_many_variables




    def many_variables(self):

        localctx = MCParser.Many_variablesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_many_variables)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 106
            self.variable()
            self.state = 111
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.COMMA:
                self.state = 107
                self.match(MCParser.COMMA)
                self.state = 108
                self.variable()
                self.state = 113
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ParameterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitive_type(self):
            return self.getTypedRuleContext(MCParser.Primitive_typeContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_parameter




    def parameter(self):

        localctx = MCParser.ParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_parameter)
        try:
            self.state = 122
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 114
                self.primitive_type()
                self.state = 115
                self.match(MCParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 117
                self.primitive_type()
                self.state = 118
                self.match(MCParser.ID)
                self.state = 119
                self.match(MCParser.LSB)
                self.state = 120
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Parameter_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parameter(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ParameterContext)
            else:
                return self.getTypedRuleContext(MCParser.ParameterContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_parameter_list




    def parameter_list(self):

        localctx = MCParser.Parameter_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_parameter_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INTTYPE) | (1 << MCParser.STRING))) != 0):
                self.state = 124
                self.parameter()
                self.state = 129
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 125
                    self.match(MCParser.COMMA)
                    self.state = 126
                    self.parameter()
                    self.state = 131
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Block_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(MCParser.LP, 0)

        def RP(self):
            return self.getToken(MCParser.RP, 0)

        def var_declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Var_declarationContext)
            else:
                return self.getTypedRuleContext(MCParser.Var_declarationContext,i)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_block_statement




    def block_statement(self):

        localctx = MCParser.Block_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_block_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 134
            self.match(MCParser.LP)
            self.state = 139
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FPLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BOOLEAN) | (1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.FOR) | (1 << MCParser.FLOAT) | (1 << MCParser.IF) | (1 << MCParser.INTTYPE) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.TRUE) | (1 << MCParser.FALSE) | (1 << MCParser.STRING) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LP) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0):
                self.state = 137
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [MCParser.BOOLEAN, MCParser.FLOAT, MCParser.INTTYPE, MCParser.STRING]:
                    self.state = 135
                    self.var_declaration()
                    pass
                elif token in [MCParser.INTLIT, MCParser.FPLIT, MCParser.STRINGLIT, MCParser.BREAK, MCParser.CONTINUE, MCParser.FOR, MCParser.IF, MCParser.RETURN, MCParser.DO, MCParser.TRUE, MCParser.FALSE, MCParser.SUB, MCParser.NOT, MCParser.LP, MCParser.LB, MCParser.ID]:
                    self.state = 136
                    self.statement()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 141
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 142
            self.match(MCParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def if_statement(self):
            return self.getTypedRuleContext(MCParser.If_statementContext,0)


        def do_while_statement(self):
            return self.getTypedRuleContext(MCParser.Do_while_statementContext,0)


        def for_statement(self):
            return self.getTypedRuleContext(MCParser.For_statementContext,0)


        def break_statement(self):
            return self.getTypedRuleContext(MCParser.Break_statementContext,0)


        def continue_statement(self):
            return self.getTypedRuleContext(MCParser.Continue_statementContext,0)


        def return_statement(self):
            return self.getTypedRuleContext(MCParser.Return_statementContext,0)


        def expression_statement(self):
            return self.getTypedRuleContext(MCParser.Expression_statementContext,0)


        def block_statement(self):
            return self.getTypedRuleContext(MCParser.Block_statementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_statement




    def statement(self):

        localctx = MCParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_statement)
        try:
            self.state = 152
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.IF]:
                self.enterOuterAlt(localctx, 1)
                self.state = 144
                self.if_statement()
                pass
            elif token in [MCParser.DO]:
                self.enterOuterAlt(localctx, 2)
                self.state = 145
                self.do_while_statement()
                pass
            elif token in [MCParser.FOR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 146
                self.for_statement()
                pass
            elif token in [MCParser.BREAK]:
                self.enterOuterAlt(localctx, 4)
                self.state = 147
                self.break_statement()
                pass
            elif token in [MCParser.CONTINUE]:
                self.enterOuterAlt(localctx, 5)
                self.state = 148
                self.continue_statement()
                pass
            elif token in [MCParser.RETURN]:
                self.enterOuterAlt(localctx, 6)
                self.state = 149
                self.return_statement()
                pass
            elif token in [MCParser.INTLIT, MCParser.FPLIT, MCParser.STRINGLIT, MCParser.TRUE, MCParser.FALSE, MCParser.SUB, MCParser.NOT, MCParser.LB, MCParser.ID]:
                self.enterOuterAlt(localctx, 7)
                self.state = 150
                self.expression_statement()
                pass
            elif token in [MCParser.LP]:
                self.enterOuterAlt(localctx, 8)
                self.state = 151
                self.block_statement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class If_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def ELSE(self):
            return self.getToken(MCParser.ELSE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_if_statement




    def if_statement(self):

        localctx = MCParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_if_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self.match(MCParser.IF)
            self.state = 155
            self.match(MCParser.LB)
            self.state = 156
            self.exp()
            self.state = 157
            self.match(MCParser.RB)
            self.state = 158
            self.statement()
            self.state = 162
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.state = 160
                self.match(MCParser.ELSE)
                self.state = 161
                self.statement()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Do_while_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(MCParser.DO, 0)

        def WHILE(self):
            return self.getToken(MCParser.WHILE, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_do_while_statement




    def do_while_statement(self):

        localctx = MCParser.Do_while_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_do_while_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 164
            self.match(MCParser.DO)
            self.state = 166 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 165
                self.statement()
                self.state = 168 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FPLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.FOR) | (1 << MCParser.IF) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.TRUE) | (1 << MCParser.FALSE) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LP) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0)):
                    break

            self.state = 170
            self.match(MCParser.WHILE)
            self.state = 171
            self.exp()
            self.state = 172
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class For_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(MCParser.FOR, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpContext,i)


        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.SEMI)
            else:
                return self.getToken(MCParser.SEMI, i)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def statement(self):
            return self.getTypedRuleContext(MCParser.StatementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_for_statement




    def for_statement(self):

        localctx = MCParser.For_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_for_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(MCParser.FOR)
            self.state = 175
            self.match(MCParser.LB)
            self.state = 176
            self.exp()
            self.state = 177
            self.match(MCParser.SEMI)
            self.state = 178
            self.exp()
            self.state = 179
            self.match(MCParser.SEMI)
            self.state = 180
            self.exp()
            self.state = 181
            self.match(MCParser.RB)
            self.state = 182
            self.statement()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Break_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(MCParser.BREAK, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_break_statement




    def break_statement(self):

        localctx = MCParser.Break_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_break_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            self.match(MCParser.BREAK)
            self.state = 185
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Continue_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONTINUE(self):
            return self.getToken(MCParser.CONTINUE, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_continue_statement




    def continue_statement(self):

        localctx = MCParser.Continue_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_continue_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 187
            self.match(MCParser.CONTINUE)
            self.state = 188
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Return_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MCParser.RETURN, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_return_statement




    def return_statement(self):

        localctx = MCParser.Return_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_return_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 190
            self.match(MCParser.RETURN)
            self.state = 192
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FPLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.TRUE) | (1 << MCParser.FALSE) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0):
                self.state = 191
                self.exp()


            self.state = 194
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Expression_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression_statement




    def expression_statement(self):

        localctx = MCParser.Expression_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_expression_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 196
            self.exp()
            self.state = 197
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp1(self):
            return self.getTypedRuleContext(MCParser.Exp1Context,0)


        def ASSIGN(self):
            return self.getToken(MCParser.ASSIGN, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_exp




    def exp(self):

        localctx = MCParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_exp)
        try:
            self.state = 204
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 199
                self.exp1(0)
                self.state = 200
                self.match(MCParser.ASSIGN)
                self.state = 201
                self.exp()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 203
                self.exp1(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exp1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp2(self):
            return self.getTypedRuleContext(MCParser.Exp2Context,0)


        def exp1(self):
            return self.getTypedRuleContext(MCParser.Exp1Context,0)


        def OR(self):
            return self.getToken(MCParser.OR, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp1



    def exp1(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Exp1Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 38
        self.enterRecursionRule(localctx, 38, self.RULE_exp1, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.exp2(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 214
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Exp1Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp1)
                    self.state = 209
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 210
                    self.match(MCParser.OR)
                    self.state = 211
                    self.exp2(0) 
                self.state = 216
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Exp2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp3(self):
            return self.getTypedRuleContext(MCParser.Exp3Context,0)


        def exp2(self):
            return self.getTypedRuleContext(MCParser.Exp2Context,0)


        def AND(self):
            return self.getToken(MCParser.AND, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp2



    def exp2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Exp2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 40
        self.enterRecursionRule(localctx, 40, self.RULE_exp2, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 218
            self.exp3()
            self._ctx.stop = self._input.LT(-1)
            self.state = 225
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,17,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Exp2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp2)
                    self.state = 220
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 221
                    self.match(MCParser.AND)
                    self.state = 222
                    self.exp3() 
                self.state = 227
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,17,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Exp3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp4(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp4Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp4Context,i)


        def EQUAL(self):
            return self.getToken(MCParser.EQUAL, 0)

        def NOTEQUAL(self):
            return self.getToken(MCParser.NOTEQUAL, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp3




    def exp3(self):

        localctx = MCParser.Exp3Context(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_exp3)
        self._la = 0 # Token type
        try:
            self.state = 233
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 228
                self.exp4()
                self.state = 229
                _la = self._input.LA(1)
                if not(_la==MCParser.EQUAL or _la==MCParser.NOTEQUAL):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 230
                self.exp4()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 232
                self.exp4()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exp4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp5(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp5Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp5Context,i)


        def GT(self):
            return self.getToken(MCParser.GT, 0)

        def GTE(self):
            return self.getToken(MCParser.GTE, 0)

        def LT(self):
            return self.getToken(MCParser.LT, 0)

        def LTE(self):
            return self.getToken(MCParser.LTE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp4




    def exp4(self):

        localctx = MCParser.Exp4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_exp4)
        self._la = 0 # Token type
        try:
            self.state = 240
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 235
                self.exp5(0)
                self.state = 236
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.LT) | (1 << MCParser.LTE) | (1 << MCParser.GT) | (1 << MCParser.GTE))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 237
                self.exp5(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 239
                self.exp5(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exp5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp6(self):
            return self.getTypedRuleContext(MCParser.Exp6Context,0)


        def exp5(self):
            return self.getTypedRuleContext(MCParser.Exp5Context,0)


        def ADD(self):
            return self.getToken(MCParser.ADD, 0)

        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp5



    def exp5(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Exp5Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 46
        self.enterRecursionRule(localctx, 46, self.RULE_exp5, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 243
            self.exp6(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 250
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,20,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Exp5Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp5)
                    self.state = 245
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 246
                    _la = self._input.LA(1)
                    if not(_la==MCParser.ADD or _la==MCParser.SUB):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 247
                    self.exp6(0) 
                self.state = 252
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,20,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Exp6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self):
            return self.getTypedRuleContext(MCParser.Exp7Context,0)


        def exp6(self):
            return self.getTypedRuleContext(MCParser.Exp6Context,0)


        def DIV(self):
            return self.getToken(MCParser.DIV, 0)

        def MUL(self):
            return self.getToken(MCParser.MUL, 0)

        def MODULUS(self):
            return self.getToken(MCParser.MODULUS, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp6



    def exp6(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Exp6Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 48
        self.enterRecursionRule(localctx, 48, self.RULE_exp6, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 254
            self.exp7()
            self._ctx.stop = self._input.LT(-1)
            self.state = 261
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,21,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Exp6Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp6)
                    self.state = 256
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 257
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.MUL) | (1 << MCParser.DIV) | (1 << MCParser.MODULUS))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 258
                    self.exp7() 
                self.state = 263
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,21,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Exp7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self):
            return self.getTypedRuleContext(MCParser.Exp7Context,0)


        def NOT(self):
            return self.getToken(MCParser.NOT, 0)

        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def exp8(self):
            return self.getTypedRuleContext(MCParser.Exp8Context,0)


        def getRuleIndex(self):
            return MCParser.RULE_exp7




    def exp7(self):

        localctx = MCParser.Exp7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_exp7)
        self._la = 0 # Token type
        try:
            self.state = 267
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.SUB, MCParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 264
                _la = self._input.LA(1)
                if not(_la==MCParser.SUB or _la==MCParser.NOT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 265
                self.exp7()
                pass
            elif token in [MCParser.INTLIT, MCParser.FPLIT, MCParser.STRINGLIT, MCParser.TRUE, MCParser.FALSE, MCParser.LB, MCParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 266
                self.exp8()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exp8Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp9(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp9Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp9Context,i)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp8




    def exp8(self):

        localctx = MCParser.Exp8Context(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_exp8)
        try:
            self.state = 275
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 269
                self.exp9()
                self.state = 270
                self.match(MCParser.LSB)
                self.state = 271
                self.exp9()
                self.state = 272
                self.match(MCParser.RSB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 274
                self.exp9()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Exp9Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def operand(self):
            return self.getTypedRuleContext(MCParser.OperandContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_exp9




    def exp9(self):

        localctx = MCParser.Exp9Context(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_exp9)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 277
            self.operand(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def literal(self):
            return self.getTypedRuleContext(MCParser.LiteralContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def func_call(self):
            return self.getTypedRuleContext(MCParser.Func_callContext,0)


        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def operand(self):
            return self.getTypedRuleContext(MCParser.OperandContext,0)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_operand



    def operand(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.OperandContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 56
        self.enterRecursionRule(localctx, 56, self.RULE_operand, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 287
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,24,self._ctx)
            if la_ == 1:
                self.state = 280
                self.literal()
                pass

            elif la_ == 2:
                self.state = 281
                self.match(MCParser.ID)
                pass

            elif la_ == 3:
                self.state = 282
                self.func_call()
                pass

            elif la_ == 4:
                self.state = 283
                self.match(MCParser.LB)
                self.state = 284
                self.exp()
                self.state = 285
                self.match(MCParser.RB)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 296
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,25,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.OperandContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_operand)
                    self.state = 289
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 290
                    self.match(MCParser.LSB)
                    self.state = 291
                    self.exp()
                    self.state = 292
                    self.match(MCParser.RSB) 
                self.state = 298
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,25,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class LiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def STRINGLIT(self):
            return self.getToken(MCParser.STRINGLIT, 0)

        def FPLIT(self):
            return self.getToken(MCParser.FPLIT, 0)

        def booleanlit(self):
            return self.getTypedRuleContext(MCParser.BooleanlitContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_literal




    def literal(self):

        localctx = MCParser.LiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_literal)
        try:
            self.state = 303
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.INTLIT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 299
                self.match(MCParser.INTLIT)
                pass
            elif token in [MCParser.STRINGLIT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 300
                self.match(MCParser.STRINGLIT)
                pass
            elif token in [MCParser.FPLIT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 301
                self.match(MCParser.FPLIT)
                pass
            elif token in [MCParser.TRUE, MCParser.FALSE]:
                self.enterOuterAlt(localctx, 4)
                self.state = 302
                self.booleanlit()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Func_callContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_func_call




    def func_call(self):

        localctx = MCParser.Func_callContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_func_call)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 305
            self.match(MCParser.ID)
            self.state = 306
            self.match(MCParser.LB)
            self.state = 315
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FPLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.TRUE) | (1 << MCParser.FALSE) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0):
                self.state = 307
                self.exp()
                self.state = 312
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 308
                    self.match(MCParser.COMMA)
                    self.state = 309
                    self.exp()
                    self.state = 314
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 317
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BooleanlitContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TRUE(self):
            return self.getToken(MCParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(MCParser.FALSE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_booleanlit




    def booleanlit(self):

        localctx = MCParser.BooleanlitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_booleanlit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 319
            _la = self._input.LA(1)
            if not(_la==MCParser.TRUE or _la==MCParser.FALSE):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[19] = self.exp1_sempred
        self._predicates[20] = self.exp2_sempred
        self._predicates[23] = self.exp5_sempred
        self._predicates[24] = self.exp6_sempred
        self._predicates[28] = self.operand_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp1_sempred(self, localctx:Exp1Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def exp2_sempred(self, localctx:Exp2Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def exp5_sempred(self, localctx:Exp5Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

    def exp6_sempred(self, localctx:Exp6Context, predIndex:int):
            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         

    def operand_sempred(self, localctx:OperandContext, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 1)
         





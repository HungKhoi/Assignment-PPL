// ID: 1711611
grammar MC;

@lexer::header {
from lexererr import *
}

@lexer::members {
def emit(self):
    tk = self.type
    if tk == self.UNCLOSE_STRING:
        result = super().emit();
        raise UncloseString(result.text);
    elif tk == self.ILLEGAL_ESCAPE:
        result = super().emit();
        raise IllegalEscape(result.text);
    elif tk == self.ERROR_CHAR:
        result = super().emit();
        raise ErrorToken(result.text);
    else:
        return super().emit();
}

options{
	language=Python3;
}

/*program: mctype 'main' LB RB LP body? RP EOF ;

mctype: INTTYPE | VOIDTYPE ;

body: funcall SEMI;

exp: funcall | INTLIT ;

funcall: ID LB exp? RB ;*/

program: (var_declaration | func_declaration)+ EOF;

var_declaration: (primitive_type variable SEMI | primitive_type many_variables SEMI);

func_declaration: type_return ID LB parameter_list RB block_statement;

primitive_type: BOOLEAN | INTTYPE | FLOAT | STRING;

type_return: primitive_type | VOIDTYPE | primitive_type LSB RSB;

variable: ID | ID LSB INTLIT RSB;

many_variables: variable (COMMA variable)*;

parameter: primitive_type ID | primitive_type ID LSB RSB;

parameter_list: (parameter (COMMA parameter)*)?;

block_statement: LP (var_declaration | statement)* RP;

statement: if_statement | do_while_statement | for_statement | break_statement | continue_statement | return_statement | expression_statement | block_statement;

if_statement: (IF LB exp RB statement) (ELSE statement)?;

do_while_statement: (DO statement+ WHILE exp SEMI);

for_statement: FOR LB exp SEMI exp SEMI exp RB statement;

break_statement: BREAK SEMI;

continue_statement: CONTINUE SEMI;

return_statement: RETURN (exp)? SEMI;

expression_statement: exp SEMI;

exp: exp1 ASSIGN exp | exp1;

exp1: exp1 OR exp2 | exp2;

exp2: exp2 AND exp3 | exp3;

exp3: exp4 (EQUAL | NOTEQUAL) exp4 | exp4;

exp4: exp5 (GT | GTE | LT | LTE) exp5 | exp5;

exp5: exp5 (ADD | SUB) exp6 | exp6;

exp6: exp6 (DIV | MUL | MODULUS) exp7 | exp7;

exp7: (NOT | SUB) exp7 | exp8;

exp8: exp9 LSB exp9 RSB | exp9;

exp9: operand;

operand: literal | ID | func_call | LB exp RB | operand LSB exp RSB;

literal: INTLIT | STRINGLIT | FPLIT | booleanlit;

func_call: ID LB (exp (COMMA exp)*)? RB;

/**
* Ignore comment
**/

BLOCK_COMMENT: '/*' (.)*? '*/' -> skip;
LINE_COMMENT: '//' (~[\n])* -> skip;  

/**
 * LITERALS
 **/

INTLIT: [0-9]+;

FPLIT: ([0-9]+('.'[0-9]*)? | '.'[0-9]+)([eE](SUB?[0-9]+))?;

booleanlit: TRUE | FALSE;

STRINGLIT: '"' ('\\'[bfrnt"\\] | ~[\b\f\r\n\t"\\])* '"'
    {
        text_String = str(self.text)
        self.text = text_String[1:-1]
    };

/**
* KEYWORDS
**/

BOOLEAN: 'boolean';

BREAK: 'break';

CONTINUE: 'continue';

ELSE: 'else';

FOR: 'for';

FLOAT: 'float';

IF: 'if';

INTTYPE: 'int' ;

VOIDTYPE: 'void' ;

RETURN: 'return';

DO: 'do';

WHILE: 'while';

TRUE: 'true';

FALSE: 'false';

STRING: 'string';

/**
 * OPERATORS
 **/

ADD: '+'; // Addition

SUB: '-'; // Subtraction or negation

MUL: '*'; // Multiplication

DIV: '/'; // Division

NOT: '!'; // Logical NOT

OR: '||'; // Logical OR

AND: '&&'; // Logical AND

EQUAL: '=='; 

MODULUS: '%';

NOTEQUAL: '!=';

LT: '<'; // Less than

LTE: '<='; // Less than or equal

GT: '>'; // Great than

GTE: '>='; // Great than or equal

ASSIGN: '=';

/**
 * SEPARATORS
 **/

LSB: '['; // Left square bracket

RSB: ']'; // Right square bracket

LP: '{'; // Left parenthesis

RP: '}'; // Right 

SEMI: ';'; // Semicolon

LB: '('; // Left bracket

RB: ')'; // Right bracket

COMMA: ',';

/**
* IDENTIFIERS
**/

ID: [_a-zA-Z][_a-zA-Z0-9]*;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

UNCLOSE_STRING: '"' ('\\'[bfrnt"\\] | ~[\b\f\r\n\t"\\])* ([\b\f\r\n\t"\\] | EOF)
    {
        terminate_Char = ["\b", "\f", "\r", "\n", "\t", "\\", "\""]
        self.text = self.text[1:-1] if (self.text[-1] in terminate_Char) else self.text[1:]
    };

ILLEGAL_ESCAPE: '"' ('\\'[bfrnt"\\] | ~[\b\f\r\n\t"\\])* '\\'~[bfrnt"\\]
    {
        text_String = str(self.text)
        self.text = text_String[1:]
    };

ERROR_CHAR: .;

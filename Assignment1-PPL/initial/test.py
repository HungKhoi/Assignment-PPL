"""
ILLEGAL_ESCAPE: '"' ('\\'[bfrnt"\\] | ~[\b\f\r\n\t"\\] | '\\'~[bfrnt"\\])* '"'
    {
        legal_Char = ["b", "f", "r", "n", "t", "\"", "\\"]
        for i in range(len(self.text)):
           if (self.text[i] == "\\"):
               if ((i + 1 < len(self.text)) and (self.text[i + 1] in legal_Char)):
                   continue
               index = i + 1
               break
       self.text = self.text[1:index + 1]
   };
"""




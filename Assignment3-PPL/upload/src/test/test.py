class A:
    pass

class B(A):
    def __init__(self):
        self.x = 2
    
    def test(self):
        return self.x

class C(A):
    def __init__(self):
        self.x = 3
    
    def test(self):
        return self.x

a = B()
print(a.test())
a = C()
print(a.test())

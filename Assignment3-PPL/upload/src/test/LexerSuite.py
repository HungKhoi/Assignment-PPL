#ID: 1711611
import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):  
    def test_identifier1(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("skdo","skdo,<EOF>",101))
    def test_identifier2(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("24142kdos0329","24142,kdos0329,<EOF>",102))
    def test_identifier3(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("_jrisid","_jrisid,<EOF>",103))
    def test_identifier4(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("_3299.24","_3299,.24,<EOF>",104))
    def test_identifier5(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme(".2E23kslduof",".2E23,kslduof,<EOF>",105))
    def test_identifier6(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("soaufo 42kdojvoo","soaufo,42,kdojvoo,<EOF>",106))
    def test_identifier7(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("djos03 1.MEME 29du9","djos03,1.,MEME,29,du9,<EOF>",107))
    def test_identifier8(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("djsjFPXRNGIG1.2EE.24e283lpl","djsjFPXRNGIG1,.2,EE,.24e283,lpl,<EOF>",108))
    def test_identifier9(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("4a2ea94.00 ssofsSKTGRFDWG1.4e2341_lck","4,a2ea94,.00,ssofsSKTGRFDWG1,.4e2341,_lck,<EOF>",109))
    def test_identifier10(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("49jdgChhrlnd8xI1dsxd sisyfiij dwdski6E","49,jdgChhrlnd8xI1dsxd,sisyfiij,dwdski6E,<EOF>",110))
    
    
    def test_keyword1(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("break continue else for if return do while","break,continue,else,for,if,return,do,while,<EOF>",111))
    def test_keyword2(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("stringdsk IF if","stringdsk,IF,if,<EOF>",112))
    def test_keyword3(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("void while9 do return float for continue intvoid","void,while9,do,return,float,for,continue,intvoid,<EOF>",113))
    def test_keyword4(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("int.12 for bool if do while else for return","int,.12,for,bool,if,do,while,else,for,return,<EOF>",114))
    def test_keyword5(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("string continue else for float if","string,continue,else,for,float,if,<EOF>",115))
    def test_keyword6(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("iF stringf else24.5 123return 12.4break","iF,stringf,else24,.5,123,return,12.4,break,<EOF>",116))
    def test_keyword7(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("anD then elsediMod doWnTO.1trueds void","anD,then,elsediMod,doWnTO,.1,trueds,void,<EOF>",117))
    def test_keyword8(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("if SKT else if GRF else DWG","if,SKT,else,if,GRF,else,DWG,<EOF>",118))
    def test_keyword9(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("returnc hsdasddo do swhile void int if return string float for 12.4else 54continue","returnc,hsdasddo,do,swhile,void,int,if,return,string,float,for,12.4,else,54,continue,<EOF>",119))
    def test_keyword10(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("12.4boolean12e5 .2e5break continue else 181299for float ssif int Hungvoid Tien return do while Nguyenstring","12.4,boolean12e5,.2e5,break,continue,else,181299,for,float,ssif,int,Hungvoid,Tien,return,do,while,Nguyenstring,<EOF>",120))


    def test_operator1(self):
        self.assertTrue(TestLexer.checkLexeme("+ - * / % ! || && != == < > <= >= =","+,-,*,/,%,!,||,&&,!=,==,<,>,<=,>=,=,<EOF>",121))
    def test_operator2(self):
        self.assertTrue(TestLexer.checkLexeme("dlsd+1ds-*dmdsa/<mdks","dlsd,+,1,ds,-,*,dmdsa,/,<,mdks,<EOF>",122))
    def test_operator3(self):
        self.assertTrue(TestLexer.checkLexeme("lsddl||=1&&=112>=<=d1","lsddl,||,=,1,&&,=,112,>=,<=,d1,<EOF>",123))
    def test_operator4(self):
        self.assertTrue(TestLexer.checkLexeme("13ek3<9e=9eend%=Edasdndm<=>erE","13,ek3,<,9,e,=,9,eend,%,=,Edasdndm,<=,>,erE,<EOF>",124))
    def test_operator5(self):
        self.assertTrue(TestLexer.checkLexeme("djeiwjd1A<=>12==>=<=d","djeiwjd1A,<=,>,12,==,>=,<=,d,<EOF>",125))
    def test_operator6(self):
        self.assertTrue(TestLexer.checkLexeme("<-mod>=not+mod+and+not","<,-,mod,>=,not,+,mod,+,and,+,not,<EOF>",126))
    def test_operator7(self):
        self.assertTrue(TestLexer.checkLexeme("*and<=>mod</!==<=","*,and,<=,>,mod,<,/,!=,=,<=,<EOF>",127))
    def test_operator8(self):
        self.assertTrue(TestLexer.checkLexeme("=or<=!==!!!=!==-<=>","=,or,<=,!=,=,!,!,!=,!=,=,-,<=,>,<EOF>",128))
    def test_operator9(self):
        self.assertTrue(TestLexer.checkLexeme("not<===and>=mod<=-and","not,<=,==,and,>=,mod,<=,-,and,<EOF>",129))
    def test_operator10(self):
        self.assertTrue(TestLexer.checkLexeme("==!=<>=<===!=<>==%&&===*/||+===!=","==,!=,<,>=,<=,==,!=,<,>=,=,%,&&,==,=,*,/,||,+,==,=,!=,<EOF>",130))

    
    def test_seperator1(self):
        self.assertTrue(TestLexer.checkLexeme(
            """( ) text 123;
            """,
            """(,),text,123,;,<EOF>""",
            131))
    def test_seperator2(self):
        self.assertTrue(TestLexer.checkLexeme(
            """][ void main []
            """,
            """],[,void,main,[,],<EOF>""",
            132))
    def test_seperator3(self):
        self.assertTrue(TestLexer.checkLexeme(
            """{
            int x;
            x = 3;
            }
            """,
            """{,int,x,;,x,=,3,;,},<EOF>""",
            133))
    def test_seperator4(self):
        self.assertTrue(TestLexer.checkLexeme(
            """, ; , ;
            """,
            """,,;,,,;,<EOF>""",
            134))
    def test_seperator5(self):
        self.assertTrue(TestLexer.checkLexeme(
            """{{int a; a = 1;}{"hello"}}
            """,
            """{,{,int,a,;,a,=,1,;,},{,hello,},},<EOF>""",
            135))
    

    def test_comment1(self):
        self.assertTrue(TestLexer.checkLexeme(
            """// this is a commend
            123 int
            """,
            """123,int,<EOF>""",
            136))
    def test_comment2(self):
        self.assertTrue(TestLexer.checkLexeme(
            """int a; // variable a // with integer type
            """,
            """int,a,;,<EOF>""",
            137))
    def test_comment3(self):
        self.assertTrue(TestLexer.checkLexeme(
            """10
            /* hello world */
            text
            """,
            """10,text,<EOF>""",
            138))
    def test_comment4(self):
        self.assertTrue(TestLexer.checkLexeme(
            """void main () {
            /* first block comment /*and another*/ */
            }
            """,
            """void,main,(,),{,*,/,},<EOF>""",
            139))
    def test_comment5(self):
        self.assertTrue(TestLexer.checkLexeme(
            """int x; // inline and /* block // and inline /* and block */
            """,
            """int,x,;,<EOF>""",
            140))
    
    def test_int_float_literal1(self):
        self.assertTrue(TestLexer.checkLexeme("123","123,<EOF>",141))
    def test_int_float_literal2(self):
        self.assertTrue(TestLexer.checkLexeme("428949 .2e5 4213.e5 1.2 2.","428949,.2e5,4213.e5,1.2,2.,<EOF>",142))
    def test_int_float_literal3(self):
        self.assertTrue(TestLexer.checkLexeme("423892839.443e5 124204 2.3e5","423892839.443e5,124204,2.3e5,<EOF>",143))
    def test_int_float_literal4(self):
        self.assertTrue(TestLexer.checkLexeme("21321e 321324e.24 2341 12.e35","21321,e,321324,e,.24,2341,12.e35,<EOF>",144))
    def test_int_float_literal5(self):
        self.assertTrue(TestLexer.checkLexeme("321049012e4.41e4 -1231.e 3244141e-23004 -e2341","321049012e4,.41e4,-,1231.,e,3244141e-23004,-,e2341,<EOF>",145))
    
    def test_string_literal1(self):
        self.assertTrue(TestLexer.checkLexeme("\"ahihi\\\"\"","ahihi\\\",<EOF>",146))
    def test_string_literal2(self):
        self.assertTrue(TestLexer.checkLexeme("\"abc\\nabc\"","abc\\nabc,<EOF>",147))
    def test_string_literal3(self):
        self.assertTrue(TestLexer.checkLexeme("\"abc\\ta\\nbc\"","abc\\ta\\nbc,<EOF>",148))
    def test_string_literal4(self):
        self.assertTrue(TestLexer.checkLexeme("\"abc\" 0 \"12ab\\fc0.1\"","abc,0,12ab\\fc0.1,<EOF>",149))
    def test_string_literal5(self):
        self.assertTrue(TestLexer.checkLexeme("\"0.1anc\\fcv\\\" 0.mne \\\"12\\\\3\"","0.1anc\\fcv\\\" 0.mne \\\"12\\\\3,<EOF>",150))
    def test_string_literal6(self):
        self.assertTrue(TestLexer.checkLexeme("abc \"abc1!!@#$$%^i\\n\" 12yz","abc,abc1!!@#$$%^i\\n,12,yz,<EOF>",151))
    def test_string_literal7(self):
        self.assertTrue(TestLexer.checkLexeme("\"!h$5FBi6\"_q\"!SZR,H}\"sIfpw","!h$5FBi6,_q,!SZR,H},sIfpw,<EOF>",152))
    def test_string_literal8(self):
        self.assertTrue(TestLexer.checkLexeme("4\"&J^1a_.\\t QGn\\b?67Sp\\r{,}6Asz\\\\Yx](\"","4,&J^1a_.\\t QGn\\b?67Sp\\r{,}6Asz\\\\Yx](,<EOF>",153))
    def test_string_literal9(self):
        self.assertTrue(TestLexer.checkLexeme("0f1_\"^VLR@\\\\OusM;\"uGM+jE","0,f1_,^VLR@\\\\OusM;,uGM,+,jE,<EOF>",154))
    def test_string_literal10(self):
        self.assertTrue(TestLexer.checkLexeme("\"(IFq+lq(\"IhK6\"we(*.*)GdvS{(}r9e\\nhuhur sdasdsfew\"","(IFq+lq(,IhK6,we(*.*)GdvS{(}r9e\\nhuhur sdasdsfew,<EOF>",155))
    
    def test_unclose_String1(self):
        self.assertTrue(TestLexer.checkLexeme("\"bacxyc","Unclosed String: bacxyc",156))
    def test_unclose_String2(self):
        self.assertTrue(TestLexer.checkLexeme("NmkobYn+I1+\"`YS2h.J(\t","NmkobYn,+,I1,+,Unclosed String: `YS2h.J(",157))
    def test_unclose_String3(self):
        self.assertTrue(TestLexer.checkLexeme("\"acnv \" \"abc\b","acnv ,Unclosed String: abc",158))
    def test_unclose_String4(self):
        self.assertTrue(TestLexer.checkLexeme("\"acms!,lds \" {\"abc\"} 123\"abcfsfasHung\\","acms!,lds ,{,abc,},123,Unclosed String: abcfsfasHung",159))
    def test_unclose_String5(self):
        self.assertTrue(TestLexer.checkLexeme("a+11.2+\"mam.123\" 12 \"%^&\f","a,+,11.2,+,mam.123,12,Unclosed String: %^&",160))
    def test_unclose_String6(self):
        self.assertTrue(TestLexer.checkLexeme("38n\"[#Ffs?0ED\"0.\"T`#!7n\r","38,n,[#Ffs?0ED,0.,Unclosed String: T`#!7n",161))
    def test_unclose_String7(self):
        self.assertTrue(TestLexer.checkLexeme("\".Hub`22Y\"<dsf\"Y`=DxXhZKh\n",".Hub`22Y,<,dsf,Unclosed String: Y`=DxXhZKh",162))
    def test_unclose_String8(self):
        self.assertTrue(TestLexer.checkLexeme("\"ULxM*`~.~+C_DISD2","Unclosed String: ULxM*`~.~+C_DISD2",163))
    def test_unclose_String9(self):
        self.assertTrue(TestLexer.checkLexeme("""\"jfdofoskd;pds\\t\\b\\n\\r\\f\\\\\\\"\"21.e-2244sfaHung\"0fa99sk\\t\\b\\n\\r\\f\\\\sf\\""","jfdofoskd;pds\\t\\b\\n\\r\\f\\\\\\\",21.e-2244,sfaHung,Unclosed String: 0fa99sk\\t\\b\\n\\r\\f\\\\sf",164))
    def test_unclose_String10(self):
        self.assertTrue(TestLexer.checkLexeme("\"o|F&)LqX\"+>X+\"#Fft","o|F&)LqX,+,>,X,+,Unclosed String: #Fft",165))
    
    def test_illegal_string1(self):
        self.assertTrue(TestLexer.checkLexeme(r""" "89du9j\t\b\f\k4903i0d\g" ""","Illegal Escape In String: 89du9j\\t\\b\\f\\k",166))
    def test_illegal_string2(self):
        self.assertTrue(TestLexer.checkLexeme(r""" ud9jds9.219e304DWG "joods\f" "pspd-3\"\k" ""","ud9jds9,.219e304,DWG,joods\\f,Illegal Escape In String: pspd-3\\\"\\k",167))
    def test_illegal_string3(self):
        self.assertTrue(TestLexer.checkLexeme(""" 123 "123a\\m123" ""","""123,Illegal Escape In String: 123a\\m""",168))
    def test_illegal_string4(self):
        self.assertTrue(TestLexer.checkLexeme(r""" jidjid% int 003+(sdf) true "djosfor2\t\v" "i4i3203e0230" ""","jidjid,%,int,003,+,(,sdf,),true,Illegal Escape In String: djosfor2\\t\\v",169))
    def test_illegal_string5(self):
        self.assertTrue(TestLexer.checkLexeme(r""" dsaasSKTGRFDWG"sidhisdhiadis\b\n"FPXRNGIGdjosokd"dsjoofwor\n\mdsdaffw" ""","dsaasSKTGRFDWG,sidhisdhiadis\\b\\n,FPXRNGIGdjosokd,Illegal Escape In String: dsjoofwor\\n\\m",170))
    def test_illegal_string6(self):
        self.assertTrue(TestLexer.checkLexeme(r""" "Hi, this is illegall escape \i" ""","Illegal Escape In String: Hi, this is illegall escape \\i" ,171))
    def test_illegal_string7(self):
        self.assertTrue(TestLexer.checkLexeme("||nac[]+1.1 \"ba\\qm\f\"","||,nac,[,],+,1.1,Illegal Escape In String: ba\\q",172))
    def test_illegal_string8(self):
        self.assertTrue(TestLexer.checkLexeme("ba+12+\"na\"\"md+1.2-468\lb","ba,+,12,+,na,Illegal Escape In String: md+1.2-468\l",173))
    def test_illegal_string9(self):
        self.assertTrue(TestLexer.checkLexeme(r""" jdi91kr00304 "3244" "djsjods\t\b\y" sdjojof[]{}$%^% ""","jdi91kr00304,3244,Illegal Escape In String: djsjods\\t\\b\\y",174))
    def test_illegal_string10(self):
        self.assertTrue(TestLexer.checkLexeme(r""" "i0kr\t\no0re0k0e0e0e0e0e0e0ps" "slpdslppd\t\k" "i92u03 ""","i0kr\\t\\no0re0k0e0e0e0e0e0e0ps,Illegal Escape In String: slpdslppd\\t\\k",175))


    def test_error_char1(self):
        self.assertTrue(TestLexer.checkLexeme("fudfjd9s9o@jdo","fudfjd9s9o,Error Token @",176))
    def test_error_char2(self):
        self.assertTrue(TestLexer.checkLexeme("12499893djksk\"321@dsfs\"12.4e524@","12499893,djksk,321@dsfs,12.4e524,Error Token @",177))
    def test_error_char3(self):
        self.assertTrue(TestLexer.checkLexeme("C+9and+EG9342hFAX|>sdafas","C,+,9,and,+,EG9342hFAX,Error Token |",178))
    def test_error_char4(self):
        self.assertTrue(TestLexer.checkLexeme(";J~%IbnQL!x-OBd",";,J,Error Token ~",179))
    def test_error_char5(self):
        self.assertTrue(TestLexer.checkLexeme("58291892849849352^4930430200 dksojsjfiaj","58291892849849352,Error Token ^",180))
    
    
    def test_statement1(self):   
        self.assertTrue(TestLexer.checkLexeme(
            """if ( a + 1 ) a = 0
               else a = 1;
            """,
            """if,(,a,+,1,),a,=,0,else,a,=,1,;,<EOF>""",
            181))
    def test_statement2(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """if a == 3;
               else (a = 0; b = 1);
            """,
            """if,a,==,3,;,else,(,a,=,0,;,b,=,1,),;,<EOF>""",
            182))
    def test_statement3(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """if (a == 0) then else if (a == 1) a = 0;
            """,
            """if,(,a,==,0,),then,else,if,(,a,==,1,),a,=,0,;,<EOF>""",
            183))
    def test_statement4(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """do a; b; c while 0 ;
            """,
            """do,a,;,b,;,c,while,0,;,<EOF>""",
            184))
    def test_statement5(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """a += 1 while a < 3;
            """,
            """a,+,=,1,while,a,<,3,;,<EOF>""",
            185))
    def test_statement6(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """do a = a + 1 while a > 3;
            """,
            """do,a,=,a,+,1,while,a,>,3,;,<EOF>""",
            186))
    def test_statement7(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """for a = 3 ; a < 3 ; a++ a = "text"
            """,
            """for,a,=,3,;,a,<,3,;,a,+,+,a,=,text,<EOF>""",
            187))
    def test_statement8(self): 
        self.assertTrue(TestLexer.checkLexeme(
            """void main(){
            // first block
                {
                // second block
                    {
                    // third block
                        {
                        // fourth block
                    }
                }
            }
        }
            """,
            """void,main,(,),{,{,{,{,},},},},<EOF>""",
            188))
    def test_statement9(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """for ( a = 1 ; a < 3 ; a = a + 1 ) x = "text"
            """,
            """for,(,a,=,1,;,a,<,3,;,a,=,a,+,1,),x,=,text,<EOF>""",
            189))
    def test_statement10(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """for break;
            """,
            """for,break,;,<EOF>""",
            190))
    def test_statement11(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """do x = x + 1 continue while x < 0 ;
            """,
            """do,x,=,x,+,1,continue,while,x,<,0,;,<EOF>""",
            191))
    def test_statement12(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """str = " ! @ # % ^ & * ( ) ";
            """,
            """str,=, ! @ # % ^ & * ( ) ,;,<EOF>""",
            192))
    def test_statement13(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """void main ( ) {return ;}
            """,
            """void,main,(,),{,return,;,},<EOF>""",
            193))
    def test_statement14(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """int foo(int a, int b) {}
            """,
            """int,foo,(,int,a,,,int,b,),{,},<EOF>""",
            194))
    def test_statement15(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """void foo() {
                int child(){}
            }
            """,
            """void,foo,(,),{,int,child,(,),{,},},<EOF>""",
            195))
    def test_statement16(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """void main () {}
            int foo() {}
            """,
            """void,main,(,),{,},int,foo,(,),{,},<EOF>""",
            196))
    def test_statement17(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """boolean = bool = { false };
            """,
            """boolean,=,bool,=,{,false,},;,<EOF>""",
            197))
    def test_statement18(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """if ( a >= b ) return b;
            """,
            """if,(,a,>=,b,),return,b,;,<EOF>""",
            198))
    def test_statement19(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """if ( a != b ) return a;
            """,
            """if,(,a,!=,b,),return,a,;,<EOF>""",
            199))
    def test_statement20(self):  
        self.assertTrue(TestLexer.checkLexeme(
            """if ( a != b ) return a;
            """,
            """if,(,a,!=,b,),return,a,;,<EOF>""",
            200))

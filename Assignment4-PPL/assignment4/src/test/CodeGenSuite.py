import unittest
from TestUtils import TestCodeGen
from AST import *


class CheckCodeGenSuite(unittest.TestCase):
    def test_int(self):
        """Simple program: int main() {} """
        input = """void main() {putInt(100);}"""
        expect = "100"
        self.assertTrue(TestCodeGen.test(input,expect,500))
    def test_int_ast(self):
    	input = Program([
    		FuncDecl(Id("main"),[],VoidType(),Block([
    			CallExpr(Id("putInt"),[IntLiteral(5)])]))])
    	expect = "5"
    	self.assertTrue(TestCodeGen.test(input,expect,501))
    def test_simple_global_variable_declaration_which_has_primitive_type(self):
    	input = """ 
                    boolean a;
                    void main() {
                        return;
                    }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,502))
    def test_hard_global_variable_declaration_which_has_primitive_type(self):
    	input = """ 
                    int hunG;
                    int a; 
                    float b;
                    string c;
                    boolean d;
                    void main() {
                        return;
                    }
                    string e;
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,503))
    def test_simple_global_function_declaration_which_return_primitive_type(self):
    	input = """ 
                    void foo(){}
                    int a; 
                    void main(){
                        foo();
                    }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,504))
    def test_simple_global_function_declaration_which_return_array_pointer_type(self):
    	input = """ 
                    float[] foo(){
                        float hunG[32];
                        return hunG;
                    }
                    int a; 
                    void main(){
                        foo();
                        return;
                    }
                    float b;
                    string c;
                    boolean d;
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,505))
    def test_hard_global_function_declaration_which_return_primitive_type(self):
    	input = """ 
                    int a;
                    int main1() {
                        return 3;
                    } 
                    void main() {
                        main1();
                        return;
                    }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,506))
    def test_hard_global_function_declaration_which_return_array_pointer_type(self):
    	input = """ 
                    int a1; 
                    string[] a(){
                        string e[32];
                        return e;
                    } 
                    float b;
                    string c;
                    boolean d;
                    void main(){
                        return a;
                    }  
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,507))
    def test_very_hard_global_function_declaration(self):
    	input = """ 
                    int a;
                    string[] foo() {
                        string e[32];
                        return e;
                    }
                    int main1(int foo) {
                        return 3;
                    } 
                    void main() {
                        main1(3);
                        float c[24];
                        foo();
                    }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,508))
    def test_simple_local_variable_declaration(self):
    	input = """ 
                    int foo() { return 1; }
                    void main(){
                        int foo;
                        return;
                    }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,509))
    def test_hard_local_variable_declaration(self):
    	input = """ 
                    void main() {
                        int a;
                        float b;
                        string c;
                        boolean d;
                        a = 2;
                        int e[1];
                        float f[2];
                        string g[3];
                        boolean h[4];
                        return;
                   }
                   int b() {
                       int b;
                       b = 2;
                       return 3;
                   }
                    """
    	expect = ""
    	self.assertTrue(TestCodeGen.test(input,expect,510))
    def test_simple_parameter_in_function_declaration_which_has_primitive_type(self):
        input = """ 
                    int a;
                    int main1(int foo) {
                        return 3;
                    } 
                    void main() {
                        main1(2);
                        return;
                    }
                """
        expect = ""
        self.assertTrue(TestCodeGen.test(input,expect,511))
    def test_hard_parameter_in_function_declaration_which_has_primitive_type(self):
        input = """ 
                    void a(float b, string c) {}
                    void b(int a, float b) {}
                    void c(string c, boolean d) {}
                    void d(boolean d, int a) {}
                    void main() {
                        a(2.4, "1");
                        b(2, 2.14);
                        c("13", false);
                        d(true, 2);
                        e();
                        return;
                    }
                    void e() {}
                """
        expect = ""
        self.assertTrue(TestCodeGen.test(input,expect,512))
    def test_simple_parameter_in_function_declaration_which_has_array_pointer_type(self):
        input = """ 
                    int c[23];
                    int[] foo(int foo1[]) {
                        int b;
                        return foo1;
                    } 
                    void main() {
                        foo(c);
                        return;
                    }
                """
        expect = ""
        self.assertTrue(TestCodeGen.test(input,expect,513))
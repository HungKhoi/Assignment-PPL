def push0(frame):
    frame.append("0")
    return 0

def push1(frame):
    frame.append("1")
    return 1

frame = []
check = push0(frame) if (False) else push1(frame)
print(frame)
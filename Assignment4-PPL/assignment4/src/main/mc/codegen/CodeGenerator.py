'''
 *   @author Nguyen Hua Phung
 *   @version 1.0
 *   23/10/2015
 *   This file provides a simple version of code generator
 *
'''
# MSSV: 1711611
from Utils import *
from StaticCheck import *
from StaticError import *
from Emitter import Emitter
from Frame import Frame
from abc import ABC, abstractmethod

class CodeGenerator(Utils):
    def __init__(self):
        self.libName = "io"

    def init(self):
        return [Symbol("getInt", MType(list(), IntType()), CName(self.libName)),
                    Symbol("putInt", MType([IntType()], VoidType()), CName(self.libName)),
                    Symbol("putIntLn", MType([IntType()], VoidType()), CName(self.libName)),
                    Symbol("putFloatLn", MType([FloatType()], VoidType()), CName(self.libName)),
                    Symbol("getFloat",MType([],FloatType()), CName(self.libName)),
                    Symbol("putFloat",MType([FloatType()],VoidType()), CName(self.libName)),
                    Symbol("putBool",MType([BoolType()],VoidType()), CName(self.libName)),
                    Symbol("putBoolLn",MType([BoolType()],VoidType()), CName(self.libName)),
                    Symbol("putString",MType([StringType()],VoidType()), CName(self.libName)),
                    Symbol("putStringLn",MType([StringType()],VoidType()), CName(self.libName)),
                    Symbol("putLn",MType([],VoidType()), CName(self.libName))
                    ]

    def gen(self, ast, dir_):
        #ast: AST
        #dir_: String

        gl = self.init()
        gc = CodeGenVisitor(ast, gl, dir_)
        gc.visit(ast, None)

class ClassType(Type):
    def __init__(self, cname):
        #cname: String
        self.cname = cname

    def __str__(self):
        return "ClassType"

    def accept(self, v, param):
        return v.visitClassType(self, param)

class SubBody():
    def __init__(self, frame, sym):
        #frame: Frame
        #sym: List[Symbol]

        self.frame = frame
        self.sym = sym

class Access():
    def __init__(self, frame, sym, isLeft, isFirst):
        #frame: Frame
        #sym: List[Symbol]
        #isLeft: Boolean
        #isFirst: Boolean

        self.frame = frame
        self.sym = sym
        self.isLeft = isLeft
        self.isFirst = isFirst

class Val(ABC):
    pass

class Index(Val):
    def __init__(self, value):
        #value: Int

        self.value = value

class CName(Val):
    def __init__(self, value):
        #value: String

        self.value = value

class CodeGenVisitor(BaseVisitor, Utils):
    def __init__(self, astTree, env, dir_):
        #astTree: AST
        #env: List[Symbol]
        #dir_: File

        self.astTree = astTree
        self.env = env
        self.className = "MCClass"
        self.path = dir_
        self.emit = Emitter(self.path + "/" + self.className + ".j")

    def visitProgram(self, ast, c):
        #ast: Program
        #c: Any

        self.emit.printout(self.emit.emitPROLOG(self.className, "java.lang.Object"))
        for x in ast.decl:
            if (isinstance(x, VarDecl)):
                self.env.append(Symbol(x.variable, x.varType, CName(self.className)))
            else:
                self.env.append(Symbol(x.name.name, MType([i.varType for i in x.param], x.returnType), CName(self.className)))
        # generate code for each declaration
        for x in ast.decl:
            if (isinstance(x, VarDecl)):
                self.visit(x, None)
        for x in ast.decl:
            if (not isinstance(x, VarDecl)):
                self.visit(x, None)
        # generate default constructor
        self.genMETHOD(FuncDecl(Id("<init>"), list(), None, Block(list())), c, Frame("<init>", VoidType))
        # class init - static field
        self.genMETHOD(FuncDecl(Id("<clinit>"), list(), None, Block(list())), c, Frame("<clinit>", VoidType))
        self.emit.emitEPILOG()
        return c

    def getIndexOfVariable(self, lstSymbol, nameVariable):
        index_Of_Method = -1
        index_Of_Variable = -1
        i = 0
        end = False
        symbol = None
        for obj in lstSymbol[::-1]:
            if (isinstance(obj.mtype, MType)):
                index_Of_Method = i
                if (obj.name == "main"):
                    index_Of_Method = index_Of_Method + 1
                break
            if ((obj.name == nameVariable) and (not isinstance(obj.mtype, MType)) and (not end)):
                index_Of_Variable = i
                symbol = obj
                end = True
            i = i + 1
        if (index_Of_Variable == -1):
            return -1, symbol
        else:
            return index_Of_Method - index_Of_Variable - 1, symbol


    def genMETHOD(self, consdecl, o, frame):
        #consdecl: FuncDecl
        #o: List[List_Array_Decl, List_Symbol]
        #frame: Frame
        methodName = consdecl.name.name
        isInit = consdecl.returnType is None and methodName == "<init>"
        isClassInit = consdecl.returnType is None and methodName == "<clinit>"
        isMain = consdecl.name.name == "main"
        returnType = VoidType() if (isInit or isClassInit) else consdecl.returnType
        intype = [ArrayPointerType(StringType())] if isMain else [x.varType for x in consdecl.param]
        mtype = MType(intype, returnType)
        self.emit.printout(self.emit.emitMETHOD(methodName, mtype, not isInit, frame))

        frame.enterScope(True)

        new_sym = o
        lst_Array_Decl = []
        # Generate code for declarations
        if isInit:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "this", ClassType(self.className), frame.getStartLabel(), frame.getEndLabel(), frame))
        if isMain:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "args", ArrayPointerType(StringType()), frame.getStartLabel(), frame.getEndLabel(), frame))
        for obj in consdecl.param:
            new_sym = new_sym + [Symbol(obj.variable, obj.varType)]
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), obj.variable, obj.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
        body = consdecl.body
        for obj in body.member:
            if (isinstance(obj, VarDecl)):
                new_sym = new_sym + [Symbol(obj.variable, obj.varType)]
                self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), obj.variable, obj.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
                if (isinstance(obj.varType, ArrayType)):
                    lst_Array_Decl.append(Symbol(obj.variable, obj.varType))
        self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(), frame))

        # Initialize address for each array declaration
        if isClassInit:
            for decl in self.env:
                if (isinstance(decl.mtype, ArrayType)):
                    self.emit.printout(self.emit.emitInitStaticArray(decl, frame))
        for decl in lst_Array_Decl:
            index, _ = self.getIndexOfVariable(new_sym, decl.name)
            self.emit.printout(self.emit.emitInitLocalArray(index, decl, frame))

        # Generate code for statements
        if isInit:
            self.emit.printout(self.emit.emitREADVAR("this", ClassType(self.className), 0, frame))
            self.emit.printout(self.emit.emitINVOKESPECIAL(frame))
        
        # Generate code for members in body
        for obj in body.member:
            if (not isinstance(obj, VarDecl)):
                if (isinstance(obj, Expr)): 
                    buffer, exprType = self.visit(obj, Access(frame, new_sym.copy(), False, False))
                    self.emit.printout(buffer)
                else:
                    self.visit(obj, SubBody(frame, new_sym.copy()))
        self.emit.printout(self.emit.emitLABEL(frame.getEndLabel(), frame))
        self.emit.printout(self.emit.emitRETURN(returnType, frame))
        self.emit.printout(self.emit.emitENDMETHOD(frame))
        frame.exitScope()

    def visitVarDecl(self, ast, o):
        # o: Any
        # Generate code for static field        
        self.emit.printout(self.emit.emitATTRIBUTE(ast.variable, ast.varType, False, ""))
        
    def visitFuncDecl(self, ast, o):
        #ast: FuncDecl
        #o: Any

        new_sym = [Symbol(ast.name.name, MType([i.varType for i in ast.param], ast.returnType), CName(self.className))]
        frame = Frame(ast.name, ast.returnType)
        self.genMETHOD(ast, new_sym, frame)

    def visitBlock(self, ast, o):
        new_sym = o.sym
        frame = o.frame
        frame.enterScope(False)
        lst_Array_Decl = []
        for obj in ast.member:
            if (isinstance(obj, VarDecl)):
                self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), obj.variable, obj.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
                if (isinstance(obj.varType, ArrayType)):
                    lst_Array_Decl.append(Symbol(obj.variable, obj.varType))
                new_sym.append(Symbol(obj.variable, obj.varType))
        self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(), frame))

        # Initialize address for local array declaration
        for decl in lst_Array_Decl:
            index, _ = self.getIndexOfVariable(new_sym, decl.name)
            self.emit.printout(self.emit.emitInitLocalArray(index, decl, frame))
        
        # Generate code for members in body
        for obj in ast.member:
            if (not isinstance(obj, VarDecl)):
                if (isinstance(obj, Expr)): 
                    buffer, returnType = self.visit(obj, Access(frame, new_sym.copy(), False, False))
                    self.emit.printout(buffer)
                else:
                    self.visit(obj, SubBody(frame, new_sym.copy()))
        
        self.emit.printout(self.emit.emitLABEL(frame.getEndLabel(), frame))
        frame.exitScope()

    def visitId(self, ast, o):
        frame = o.frame
        sym = o.sym
        isLeft = o.isLeft
        isFirst = o.isFirst
        index, symbol = self.getIndexOfVariable(o.sym, ast.name)
        if (not isLeft):
            if (index == -1):
                symbol = self.lookup(ast.name, self.env, lambda x: x.name)
                if (not isinstance(symbol.mtype, MType)):
                    return self.emit.emitGETSTATIC(symbol.value.value + "/" + symbol.name, symbol.mtype, frame), symbol.mtype
                return "", symbol.mtype
            else:
                if (not isinstance(symbol.mtype, MType)):
                    return self.emit.emitREADVAR(symbol.name, symbol.mtype, index, frame), symbol.mtype
                return "", symbol.mtype
        else:
            if (index == -1):
                symbol = self.lookup(ast.name, self.env , lambda x: x.name)
                if (not isinstance(symbol.mtype, MType)):
                    return self.emit.emitPUTSTATIC(symbol.value.value + "/" + symbol.name, symbol.mtype, frame), symbol.mtype
                return "", symbol.mtype
            else:
                if (not isinstance(symbol.mtype, MType)):
                    return self.emit.emitWRITEVAR(symbol.name, symbol.mtype, index, frame), symbol.mtype
                return "", symbol.mtype

    def visitArrayCell(self, ast, o):
        frame = o.frame
        sym = o.sym
        isLeft = o.isLeft
        isFirst = o.isFirst
        exprBuffer, exprType = self.visit(ast.arr, Access(frame, sym, False, False))
        indexBuffer, indexType = self.visit(ast.idx, Access(frame, sym, False, False))
        buffer = []
        if (not isLeft):
            buffer.append(exprBuffer)
            buffer.append(indexBuffer)
            buffer.append(self.emit.emitREADVAR2("", exprType.eleType, frame))
        else:
            if (isFirst):
                buffer.append(exprBuffer)
                buffer.append(indexBuffer)  
            else:
                buffer.append(self.emit.emitWRITEVAR2("", exprType.eleType, frame))
        return ''.join(buffer), exprType.eleType

    def visitBinaryOp(self, ast, o):
        arithmethic_Op = ["+", "-", "*", "/", "%"]
        boolean_Op = ["||", "&&"]
        relational_Op = ["==", "!=", ">", ">=", "<=", "<"]
        frame = o.frame
        if (ast.op == "="):
            if (isinstance(ast.left, Id)):
                right_Buffer, right_Type = self.visit(ast.right, Access(frame, o.sym, False, False))
                left_Buffer, left_Type = self.visit(ast.left, Access(frame, o.sym, True, False))
                if ((isinstance(right_Type, IntType)) and (isinstance(left_Type, FloatType))):
                    right_Buffer = right_Buffer + self.emit.emitI2F()
                return right_Buffer + left_Buffer, left_Type
            elif (isinstance(ast.left, ArrayCell)):
                left_Buffer1, left_Type1 = self.visit(ast.left, Access(frame, o.sym, True, True))
                right_Buffer, right_Type = self.visit(ast.right, Access(frame, o.sym, False, False))
                left_Buffer2, left_Type2 = self.visit(ast.left, Access(frame, o.sym, True, False))
                if ((isinstance(right_Type, IntType)) and (isinstance(left_Type2, FloatType))):
                    right_Buffer = right_Buffer + self.emit.emitI2F()
                return left_Buffer1 + right_Buffer + left_Buffer2, left_Type2
        else:
            left_Buffer, leftType = self.visit(ast.left, o)
            right_Buffer, rightType = self.visit(ast.right, o)
            type_Op = FloatType() if ((type(leftType) is FloatType) or (type(rightType) is FloatType)) else IntType()
            if (type(type_Op) is FloatType):
                if (type(leftType) is IntType):
                    left_Buffer = left_Buffer + self.emit.emitI2F(frame)
                if (type(rightType) is IntType):
                    right_Buffer = right_Buffer + self.emit.emitI2F(frame)
            if (ast.op in arithmethic_Op):
                buffer_Op = ""
                if (ast.op in ["+", "-"]):
                    buffer_Op = self.emit.emitADDOP(ast.op, type_Op, frame)
                elif (ast.op in ["*", "/"]):
                    buffer_Op = self.emit.emitMULOP(ast.op, type_Op, frame)
                else:
                    buffer_Op = self.emit.emitMOD(frame)
                return left_Buffer + right_Buffer + buffer_Op, type_Op
            elif (ast.op in boolean_Op):
                return self.emit.emitCheckShortCircuit(ast.op, left_Buffer, right_Buffer, frame), BoolType()
            elif (ast.op in relational_Op):
                return left_Buffer + right_Buffer + self.emit.emitREOP(ast.op, type_Op, frame), BoolType()
        
    def visitUnaryOp(self, ast, o):
        frame = o.frame
        buffer, typeExpr = self.visit(ast.body, env)
        if (ast.op == "!"):
            return self.emit.emitNOT(typeExpr, frame), typeExpr
        else:
            return self.emit.emitNEGOP(typeExpr, frame), typeExpr

    def visitCallExpr(self, ast, o):
        #ast: CallExpr
        #o: Any
        result = []
        ctxt = o
        frame = ctxt.frame
        sym = self.lookup(ast.method.name, self.env, lambda x: x.name)
        cname = sym.value.value
    
        ctype = sym.mtype

        in_ = ""
        for i in range(len(ctype.partype)):
            str1, typ1 = self.visit(ast.param[i], Access(frame, o.sym, False, True))
            if ((type(typ1) is IntType) and (type(ctype.partype[i]) is FloatType)):
                str1 = str1 + self.emit.emitI2F(frame)
            in_ = in_ + str1
        result.append(in_)
        result.append(self.emit.emitINVOKESTATIC(cname + "/" + ast.method.name, ctype, frame))
        return ''.join(result), ctype.rettype

    def visitIntLiteral(self, ast, o):
        #ast: IntLiteral
        #o: Any
        ctxt = o
        frame = ctxt.frame
        return self.emit.emitPUSHICONST(ast.value, frame), IntType()

    def visitFloatLiteral(self, ast, o):
        #ast: FloatLiteral
        #o: Any
        return self.emit.emitPUSHFCONST(str(ast.value), o.frame), FloatType()

    def visitStringLiteral(self, ast, o):
        #ast: StringLiteral
        #o: Any
        return self.emit.emitPUSHCONST(ast.value, StringType(), o.frame), StringType()

    def visitBooleanLiteral(self, ast, o):
        return self.emit.emitPUSHICONST("true" if (ast.value) else "false", o.frame), BoolType()

    def visitIf(self, ast, o):
        #ast: If
        #o: SubBody

        frame = o.frame
        sym = o.sym
        exprBuffer, exprType = self.visit(ast.expr, Access(frame, sym, False, False))
        self.emit.printout(exprBuffer)
        label1 = frame.getNewLabel()
        label2 = frame.getNewLabel()
        self.emit.printout(self.emit.emitIFEQ(label1, frame))
        if (isinstance(ast.thenStmt, Expr)):
            thenBuffer, thenType = self.visit(ast.thenStmt, Access(frame, sym, False, False))
            self.emit.printout(thenBuffer)
        else:
            self.visit(ast.thenStmt, o)
        self.emit.printout(self.emit.emitGOTO(label2, frame))
        self.emit.printout(self.emit.emitLABEL(label1, frame))
        if (not ast.elseStmt is None):
            if (isinstance(ast.elseStmt, Expr)):
                elseBuffer, elseType = self.visit(ast.elseStmt, Access(frame, sym, False, False))
                self.emit.printout(elseBuffer)
            else:
                self.visit(ast.elseStmt, o)
        self.emit.printout(self.emit.emitLABEL(label2, frame))

    def visitFor(self, ast, o):
        frame = o.frame
        sym = o.sym
        frame.enterLoop()
        brkLabel = frame.getBreakLabel()
        conLabel = frame.getContinueLabel()
        expr1Buffer, expr1Type = self.visit(ast.expr1, Access(frame, sym, False, False))
        self.emit.printout(expr1Buffer)
        self.emit.printout(self.emit.emitLABEL(conLabel, frame))
        expr2Buffer, expr2Type = self.visit(ast.expr2, Access(frame, sym, False, False))
        self.emit.printout(expr2Buffer)
        self.emit.printout(self.emit.emitIFEQ(brkLabel, frame))
        if (isinstance(ast.loop, Expr)):
            loopBuffer, loopType = self.visit(ast.loop, Access(frame, sym, False, False))
            self.emit.printout(loopBuffer)
        else:
            self.visit(ast.loop, o)
        expr3Buffer, expr3Type = self.visit(ast.expr3, Access(frame, sym, False, False))
        self.emit.printout(expr3Buffer)
        self.emit.printout(self.emit.emitGOTO(conLabel, frame))
        self.emit.printout(self.emit.emitLABEL(brkLabel, frame))
        frame.exitLoop()
    
    def visitBreak(self, ast, o):
        frame = o.frame
        self.emit.printout(self.emit.emitGOTO(frame.getBreakLabel(), frame))

    def visitContinue(self, ast, o):
        frame = o.frame
        self.emit.printout(self.emit.emitGOTO(frame.getContinueLabel(), frame))

    def visitReturn(self, ast, o):
        frame = o.frame
        sym = o.sym
        symbol = o.sym[0]
        if (not ast.expr is None):
            exprBuffer, exprType = self.visit(ast.expr, Access(frame, sym, False, False))
            if ((isinstance(exprType, IntType)) and (isinstance(symbol.mtype.rettype, FloatType))):
                exprBuffer = exprBuffer + self.emit.emitI2F(frame)
            self.emit.printout(exprBuffer)
        self.emit.printout(self.emit.emitGOTO(frame.getEndLabel(), frame))

    def visitDowhile(self, ast, o):
        frame = o.frame
        sym = o.sym
        frame.enterLoop()
        brkLabel = frame.getBreakLabel()
        conLabel = frame.getContinueLabel()
        self.emit.printout(self.emit.emitLABEL(conLabel, frame))
        for obj in ast.sl:
            if (isinstance(obj, Expr)):
                exprBuffer, exprType = self.visit(obj, Access(frame, sym, False, False))
                self.emit.printout(exprBuffer)
            else:
                self.visit(obj, o)
        exprBuffer, exprType = self.visit(ast.expr, Access(frame, sym, False, False))
        self.emit.printout(self.emit.emitIFEQ(brkLabel, frame))
        self.emit.printout(self.emit.emitGOTO(conLabel, frame))
        self.emit.printout(self.emit.emitLABEL(brkLabel, frame))
        frame.exitLoop()

"""
 * @author nhphung
"""
# MSSV: 1711611
from AST import * 
from Visitor import *
from Utils import Utils
from StaticError import *

class MType:
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype
    def __str__(self):
        return "MType([" + ",".join([str(i) for i in self.partype]) + "]," + str(self.rettype) + ")"

class Symbol:
    def __init__(self,name,mtype,value = None):
        self.name = name
        self.mtype = mtype
        self.value = value

class StaticChecker(BaseVisitor,Utils):

    global_envi = [
    Symbol("getInt",MType([],IntType())),
    Symbol("putIntLn",MType([IntType()],VoidType())),
    Symbol("putInt",MType([IntType()], VoidType())),
    Symbol("getFloat",MType([],FloatType())),
    Symbol("putFloat",MType([FloatType()],VoidType())),
    Symbol("putFloatLn",MType([FloatType()],VoidType())),
    Symbol("putBool",MType([BoolType()],VoidType())),
    Symbol("putBoolLn",MType([BoolType()],VoidType())),
    Symbol("putString",MType([StringType()],VoidType())),
    Symbol("putStringLn",MType([StringType()],VoidType())),
    Symbol("putLn",MType([],VoidType()))
    ]
            
    
    def __init__(self,ast):
        #print(ast)
        #print(ast)
        #print()
        self.ast = ast

 
    
    def check(self):
        return self.visit(self.ast,StaticChecker.global_envi)

    def visitProgram(self, ast, env):
        global_ref = env[:]
        checkEntryPoint = False
        for x in ast.decl:
            if (isinstance(x, FuncDecl)):
                if (x.name.name == "main"):
                    checkEntryPoint = True
                check = self.lookup(x.name.name, global_ref, lambda x: x.name)
                if (check is None):
                    global_ref.append(Symbol(x.name.name, MType([i.varType for i in x.param], x.returnType)))
                else:
                    raise Redeclared(Function(), x.name.name)
            else:
                check = self.lookup(x.variable, global_ref, lambda x: x.name)
                if (check is None):
                    global_ref.append(Symbol(x.variable, x.varType))
                else:
                    raise Redeclared(Variable(), x.variable)
        if (not checkEntryPoint):
            raise NoEntryPoint()
        listCallExp = []
        for x in ast.decl:
            self.visit(x, [global_ref.copy(), listCallExp])
        for x in global_ref[len(env):]:
            if ((not isinstance(x.mtype, MType)) or (x.name == "main")):
                continue
            else:
                if (listCallExp.count(x.name) == 0):
                    raise UnreachableFunction(x.name)
                
    def visitVarDecl(self, ast, env):
        return False
    
    def visitFuncDecl(self, ast, env):
        #env: [Global_ref, listCallExp]
        local_ref = []
        for x in ast.param:
            check = self.lookup(x.variable, local_ref, lambda x: x.name)
            if (check is None):
                local_ref.append(Symbol(x.variable, x.varType))
            else:
                raise Redeclared(Parameter(), x.variable) 
        env[0].extend(local_ref)
        isReturn = self.visit(ast.body, [local_ref, True, env[0], False, ast, env[1]])
        if ((not isinstance(ast.returnType, VoidType)) and (not isReturn)):
            raise FunctionNotReturn(ast.name.name)
    
    def visitBlock(self, ast, env):
        # env: [list of paramFunc, isFirstBlockOfFunc, list of declaration in local scope, isBlockOfLoop, FuncDecl, listCallExpInFuncDecl]
        local_ref = []
        if ((len(env) > 1) and (env[1])):
            local_ref = env[0]
        checkReturn = False
        for x in ast.member:
            if (isinstance(x, VarDecl)):
                check = self.lookup(x.variable, local_ref, lambda x: x.name)
                if (check is None):
                    local_ref.append(Symbol(x.variable, x.varType))
                else:
                    raise Redeclared(Variable(), x.variable)  
            envOther = env.copy()
            envOther[2] = env[2].copy()
            envOther[0] = None
            envOther[1] = False
            envOther[2].extend(local_ref)  
            if (self.visit(x, envOther) is True):
                checkReturn = True
        return checkReturn
    
    def visitId(self, ast, env):
        check = self.lookup(ast.name, env[2][::-1], lambda x: x.name)
        if (check is None):
            raise Undeclared(Identifier(), ast.name)
        else:
            return check.mtype

    def visitArrayCell(self, ast, env):
        arrType = self.visit(ast.arr, env)
        idxType = self.visit(ast.idx, env)
        if ((isinstance(arrType, (ArrayType, ArrayPointerType))) and (isinstance(idxType, IntType))):
            return arrType.eleType
        else:
            raise TypeMismatchInExpression(ast)

    def visitBinaryOp(self, ast, env):
        #Check Error NotLeftValue
        if ((ast.op == "=") and (not isinstance(ast.left, Id)) and (not isinstance(ast.left, ArrayCell))):
            raise NotLeftValue(ast.left)
        left = self.visit(ast.left, env)
        right = self.visit(ast.right, env)
        # acceptType: list of all cases. A case has format [LHSType, RHSType].
        # returnType: return type of all cases.
        acceptType = []
        returnType = []
        if (ast.op == "="):
            acceptType.append([[FloatType], [IntType, FloatType]])
            returnType.append(FloatType())
            acceptType.append([[IntType], [IntType]])
            returnType.append(IntType())
            acceptType.append([[BoolType], [BoolType]])
            returnType.append(BoolType())
            acceptType.append([[StringType], [StringType]])
            returnType.append(StringType())
        elif (ast.op in ["||", "&&"]):
            acceptType.append([[BoolType], [BoolType]])
            returnType.append(BoolType())
        elif (ast.op in ["==", "!="]):
            acceptType.append([[BoolType], [BoolType]])
            returnType.append(BoolType())
            acceptType.append([[IntType], [IntType]])
            returnType.append(BoolType())
        elif (ast.op in [">", ">=", "<=", "<"]):
            acceptType.append([[IntType, FloatType], [IntType, FloatType]])
            returnType.append(BoolType())
        elif (ast.op in ["+", "-", "*", "/"]):
            acceptType.append([[IntType], [IntType]])
            returnType.append(IntType())
            acceptType.append([[FloatType], [IntType, FloatType]])
            returnType.append(FloatType())
            acceptType.append([[IntType], [FloatType]])
            returnType.append(FloatType())
        else:
            acceptType.append([[IntType], [IntType]])
            returnType.append(IntType())
        # Check type of left and right 
        check = False
        index = -1
        for i in range(len(acceptType)):
            if ((isinstance(left, tuple(acceptType[i][0]))) and (isinstance(right, tuple(acceptType[i][1])))):
                check = True
                index = i
                break
        if (not check):
            raise TypeMismatchInExpression(ast)
        else:
            return returnType[index]
    
    def visitUnaryOp(self, ast, env):
        typeExpr = self.visit(ast.body, env)
        if (ast.op == "!"):
            if (isinstance(typeExpr, BoolType)):
                return typeExpr
            else:
                raise TypeMismatchInExpression(ast)
        else:
            if (not isinstance(typeExpr, (IntType, FloatType))):
                raise TypeMismatchInExpression(ast)
            else:
                return typeExpr
        
    def visitCallExpr(self, ast, env):
        check = self.lookup(ast.method.name, env[2][::-1], lambda x: x.name)
        if (check is None):
            raise Undeclared(Function(), ast.method.name)
        if (isinstance(check.mtype, MType)):
            size = len(check.mtype.partype)
            if (size != len(ast.param)):
                raise TypeMismatchInExpression(ast)
            for i in range(size):
                paramType = self.visit(ast.param[i], env)
                if (isinstance(check.mtype.partype[i], ArrayPointerType)):
                    if (not isinstance(paramType, (ArrayType, ArrayPointerType))):
                        raise TypeMismatchInExpression(ast)
                    else:
                        if (type(check.mtype.partype[i].eleType) != type(paramType.eleType)):
                            raise TypeMismatchInExpression(ast)
                        else: 
                            continue
                elif (isinstance(check.mtype.partype[i], FloatType)):
                    if (not isinstance(paramType, (IntType, FloatType))):
                        raise TypeMismatchInExpression(ast)
                else:
                    if (type(paramType) != type(check.mtype.partype[i])):
                        raise TypeMismatchInExpression(ast)
            checkCallExp = self.lookup(ast.method.name, env[5], lambda x: x)
            if ((env[4].name.name != ast.method.name) and (checkCallExp is None) and (ast.method.name != "main")):
                env[5].append(ast.method.name)
            return check.mtype.rettype
        else:
            raise TypeMismatchInExpression(ast)
        
    def visitIntLiteral(self, ast, env):
        return IntType()

    def visitFloatLiteral(self, ast, env):
        return FloatType()

    def visitStringLiteral(self, ast, env):
        return StringType()

    def visitBooleanLiteral(self, ast, env):
        return BoolType()

    def visitIf(self, ast, env):
        exprType = self.visit(ast.expr, env)
        if (not isinstance(exprType, BoolType)):
            raise TypeMismatchInStatement(ast)
        if (ast.elseStmt is None):
            self.visit(ast.thenStmt, env)
            return False
        else:
            returnThen = self.visit(ast.thenStmt, env)
            returnElse = self.visit(ast.elseStmt, env)
            if ((returnThen is True) and (returnElse is True)):
                return True
            else:
                return False

    def visitFor(self, ast, env):
        expr1Type = self.visit(ast.expr1, env)
        expr2Type = self.visit(ast.expr2, env)
        expr3Type = self.visit(ast.expr3, env)
        if ((not isinstance(expr2Type, BoolType)) or (not isinstance(expr1Type, IntType)) or (not isinstance(expr3Type, IntType))):
            raise TypeMismatchInStatement(ast)
        env[3] = True
        self.visit(ast.loop, env)
        return False

    def visitBreak(self, ast, env):
        if (not env[3]):
            raise BreakNotInLoop()
        return False

    def visitContinue(self, ast, env):
        if (not env[3]):
            raise ContinueNotInLoop()
        return False

    def visitReturn(self, ast, env):
        if (isinstance(env[4].returnType, VoidType)):
            if (not ast.expr is None):
                raise TypeMismatchInStatement(ast)
        else:
            if (ast.expr is None):
                raise TypeMismatchInStatement(ast)
            exprType = self.visit(ast.expr, env)
            if (isinstance(env[4].returnType, ArrayPointerType)):
                if (not isinstance(exprType, (ArrayType, ArrayPointerType))):
                    raise TypeMismatchInStatement(ast)
                if (type(exprType.eleType) != type(env[4].returnType.eleType)):
                    raise TypeMismatchInStatement(ast)
            elif (isinstance(env[4].returnType, FloatType)):
                if (not isinstance(exprType, (IntType, FloatType))):
                    raise TypeMismatchInStatement(ast)
            else:
                if (not type(exprType) == type(env[4].returnType)):
                    raise TypeMismatchInStatement(ast)
        return True
            
    def visitDowhile(self, ast, env):
        exprType = self.visit(ast.exp, env)
        if (not isinstance(exprType, BoolType)):
            raise TypeMismatchInStatement(ast)
        env[3] = True
        check = False
        for x in ast.sl:
            if (self.visit(x, env) is True):
                check = True
        return check
        

    
    

                    
            
    


from abc import ABC, abstractmethod
class ZeroException(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class Rational:
    def __init__(self, n = None, d = None):
        if (d is None):
            if (n is None):
                self.__init__(n = 0, d = 1)
                return
            self.__init__(n, d = 1)
            return
        if (d == 0):
            raise ZeroDivisionError
        self.n = n
        self.d = d
        self.__g = Rational.__gcd1(abs(self.n), abs(self.d))
        self.numer = self.n/self.__g
        self.denom = self.d/self.__g
    
    def __str__(self):
        return self.numer + "/" + self.denom
    
    def __add__(self, that):
        return Rational(self.numer * that.denom + that.numer * self.denom, self.denom * that.denom)

    @staticmethod
    def gcd(a, b):
        if (b == 0):
            return a
        else:
            return Rational.gcd(b, a%b)
    
    @classmethod
    def __gcd1(cls, a, b):
        if (b == 0):
            return a
        else:
            return Rational.__gcd1(b, a%b)


class Expr(ABC):
    pass
    @abstractmethod
    def eval(self):
        pass

class Var(Expr):
    def __init__(self, name:str):
        self.name = name
    def eval(self):
        return Number(1)

class Number(Expr):
    def __init__(self, num:float):
        self.num = num
    def print(self):
        print(self.num)
    def eval(self):
        return self

class UnOp(Expr):
    def __init__(self, operator:str, arg:Expr):
        self.operator = operator
        self.arg = arg


class BinOp(Expr):
    def __init__(self, operator:str, left:Expr, right:Expr):
        self.operator = operator
        self.left = left
        self.right = right
    def eval(self):
        if (self.operator == "+"):
            return Number(self.left.eval().num + self.right.eval().num)
        elif (self.operator == "-"):
            return Number(self.left.eval().num - self.right.eval().num)
        elif (self.operator == "*"):
            return Number(self.left.eval().num * self.right.eval().num)
        else:
            return Number(self.left.eval().num / self.right.eval().num)

v = Var("x")
op = BinOp("+", Number(1), v)
print(v.name)
print(op.left)
t = BinOp("*", BinOp("+", Var("x"), Number(0.2)), Number(3))
print(t.operator)
t.eval().print()

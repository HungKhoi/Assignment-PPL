# Generated from /Users/new/Desktop/Lab/Assignment2-PPL/initial-2/src/main/mc/parser/MC.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\62")
        buf.write("\u0161\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\3\2\3\2\3\2\3\2\7\2h\n\2")
        buf.write("\f\2\16\2k\13\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\7")
        buf.write("\3v\n\3\f\3\16\3y\13\3\3\3\3\3\3\4\6\4~\n\4\r\4\16\4\177")
        buf.write("\3\5\6\5\u0083\n\5\r\5\16\5\u0084\3\5\3\5\7\5\u0089\n")
        buf.write("\5\f\5\16\5\u008c\13\5\5\5\u008e\n\5\3\5\3\5\6\5\u0092")
        buf.write("\n\5\r\5\16\5\u0093\5\5\u0096\n\5\3\5\3\5\5\5\u009a\n")
        buf.write("\5\3\5\6\5\u009d\n\5\r\5\16\5\u009e\5\5\u00a1\n\5\3\6")
        buf.write("\3\6\3\6\3\6\7\6\u00a7\n\6\f\6\16\6\u00aa\13\6\3\6\3\6")
        buf.write("\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32")
        buf.write("\3\33\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36")
        buf.write("\3\37\3\37\3\37\3 \3 \3!\3!\3!\3\"\3\"\3#\3#\3#\3$\3$")
        buf.write("\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-")
        buf.write("\3-\7-\u0139\n-\f-\16-\u013c\13-\3.\6.\u013f\n.\r.\16")
        buf.write(".\u0140\3.\3.\3/\3/\3/\3/\7/\u0149\n/\f/\16/\u014c\13")
        buf.write("/\3/\5/\u014f\n/\3/\3/\3\60\3\60\3\60\3\60\7\60\u0157")
        buf.write("\n\60\f\60\16\60\u015a\13\60\3\60\3\60\3\60\3\60\3\61")
        buf.write("\3\61\3i\2\62\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13")
        buf.write("\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26")
        buf.write("+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#")
        buf.write("E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62\3\2\13\3\2\f\f\3")
        buf.write("\2\62;\4\2GGgg\t\2$$^^ddhhppttvv\6\2\n\f\16\17$$^^\5\2")
        buf.write("C\\aac|\6\2\62;C\\aac|\5\2\13\f\17\17\"\"\6\3\n\f\16\17")
        buf.write("$$^^\2\u0173\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3")
        buf.write("\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2")
        buf.write("\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2")
        buf.write("\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2")
        buf.write("#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2")
        buf.write("\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65")
        buf.write("\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2")
        buf.write("\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2")
        buf.write("\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2")
        buf.write("\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3")
        buf.write("\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\3c\3\2\2\2\5q")
        buf.write("\3\2\2\2\7}\3\2\2\2\t\u0095\3\2\2\2\13\u00a2\3\2\2\2\r")
        buf.write("\u00ae\3\2\2\2\17\u00b6\3\2\2\2\21\u00bc\3\2\2\2\23\u00c5")
        buf.write("\3\2\2\2\25\u00ca\3\2\2\2\27\u00ce\3\2\2\2\31\u00d4\3")
        buf.write("\2\2\2\33\u00d7\3\2\2\2\35\u00db\3\2\2\2\37\u00e0\3\2")
        buf.write("\2\2!\u00e7\3\2\2\2#\u00ea\3\2\2\2%\u00f0\3\2\2\2\'\u00f5")
        buf.write("\3\2\2\2)\u00fb\3\2\2\2+\u0102\3\2\2\2-\u0104\3\2\2\2")
        buf.write("/\u0106\3\2\2\2\61\u0108\3\2\2\2\63\u010a\3\2\2\2\65\u010c")
        buf.write("\3\2\2\2\67\u010f\3\2\2\29\u0112\3\2\2\2;\u0115\3\2\2")
        buf.write("\2=\u0117\3\2\2\2?\u011a\3\2\2\2A\u011c\3\2\2\2C\u011f")
        buf.write("\3\2\2\2E\u0121\3\2\2\2G\u0124\3\2\2\2I\u0126\3\2\2\2")
        buf.write("K\u0128\3\2\2\2M\u012a\3\2\2\2O\u012c\3\2\2\2Q\u012e\3")
        buf.write("\2\2\2S\u0130\3\2\2\2U\u0132\3\2\2\2W\u0134\3\2\2\2Y\u0136")
        buf.write("\3\2\2\2[\u013e\3\2\2\2]\u0144\3\2\2\2_\u0152\3\2\2\2")
        buf.write("a\u015f\3\2\2\2cd\7\61\2\2de\7,\2\2ei\3\2\2\2fh\13\2\2")
        buf.write("\2gf\3\2\2\2hk\3\2\2\2ij\3\2\2\2ig\3\2\2\2jl\3\2\2\2k")
        buf.write("i\3\2\2\2lm\7,\2\2mn\7\61\2\2no\3\2\2\2op\b\2\2\2p\4\3")
        buf.write("\2\2\2qr\7\61\2\2rs\7\61\2\2sw\3\2\2\2tv\n\2\2\2ut\3\2")
        buf.write("\2\2vy\3\2\2\2wu\3\2\2\2wx\3\2\2\2xz\3\2\2\2yw\3\2\2\2")
        buf.write("z{\b\3\2\2{\6\3\2\2\2|~\t\3\2\2}|\3\2\2\2~\177\3\2\2\2")
        buf.write("\177}\3\2\2\2\177\u0080\3\2\2\2\u0080\b\3\2\2\2\u0081")
        buf.write("\u0083\t\3\2\2\u0082\u0081\3\2\2\2\u0083\u0084\3\2\2\2")
        buf.write("\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u008d\3")
        buf.write("\2\2\2\u0086\u008a\7\60\2\2\u0087\u0089\t\3\2\2\u0088")
        buf.write("\u0087\3\2\2\2\u0089\u008c\3\2\2\2\u008a\u0088\3\2\2\2")
        buf.write("\u008a\u008b\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3")
        buf.write("\2\2\2\u008d\u0086\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u0096")
        buf.write("\3\2\2\2\u008f\u0091\7\60\2\2\u0090\u0092\t\3\2\2\u0091")
        buf.write("\u0090\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0091\3\2\2\2")
        buf.write("\u0093\u0094\3\2\2\2\u0094\u0096\3\2\2\2\u0095\u0082\3")
        buf.write("\2\2\2\u0095\u008f\3\2\2\2\u0096\u00a0\3\2\2\2\u0097\u0099")
        buf.write("\t\4\2\2\u0098\u009a\5-\27\2\u0099\u0098\3\2\2\2\u0099")
        buf.write("\u009a\3\2\2\2\u009a\u009c\3\2\2\2\u009b\u009d\t\3\2\2")
        buf.write("\u009c\u009b\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u009c\3")
        buf.write("\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a1\3\2\2\2\u00a0\u0097")
        buf.write("\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\n\3\2\2\2\u00a2\u00a8")
        buf.write("\7$\2\2\u00a3\u00a4\7^\2\2\u00a4\u00a7\t\5\2\2\u00a5\u00a7")
        buf.write("\n\6\2\2\u00a6\u00a3\3\2\2\2\u00a6\u00a5\3\2\2\2\u00a7")
        buf.write("\u00aa\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2")
        buf.write("\u00a9\u00ab\3\2\2\2\u00aa\u00a8\3\2\2\2\u00ab\u00ac\7")
        buf.write("$\2\2\u00ac\u00ad\b\6\3\2\u00ad\f\3\2\2\2\u00ae\u00af")
        buf.write("\7d\2\2\u00af\u00b0\7q\2\2\u00b0\u00b1\7q\2\2\u00b1\u00b2")
        buf.write("\7n\2\2\u00b2\u00b3\7g\2\2\u00b3\u00b4\7c\2\2\u00b4\u00b5")
        buf.write("\7p\2\2\u00b5\16\3\2\2\2\u00b6\u00b7\7d\2\2\u00b7\u00b8")
        buf.write("\7t\2\2\u00b8\u00b9\7g\2\2\u00b9\u00ba\7c\2\2\u00ba\u00bb")
        buf.write("\7m\2\2\u00bb\20\3\2\2\2\u00bc\u00bd\7e\2\2\u00bd\u00be")
        buf.write("\7q\2\2\u00be\u00bf\7p\2\2\u00bf\u00c0\7v\2\2\u00c0\u00c1")
        buf.write("\7k\2\2\u00c1\u00c2\7p\2\2\u00c2\u00c3\7w\2\2\u00c3\u00c4")
        buf.write("\7g\2\2\u00c4\22\3\2\2\2\u00c5\u00c6\7g\2\2\u00c6\u00c7")
        buf.write("\7n\2\2\u00c7\u00c8\7u\2\2\u00c8\u00c9\7g\2\2\u00c9\24")
        buf.write("\3\2\2\2\u00ca\u00cb\7h\2\2\u00cb\u00cc\7q\2\2\u00cc\u00cd")
        buf.write("\7t\2\2\u00cd\26\3\2\2\2\u00ce\u00cf\7h\2\2\u00cf\u00d0")
        buf.write("\7n\2\2\u00d0\u00d1\7q\2\2\u00d1\u00d2\7c\2\2\u00d2\u00d3")
        buf.write("\7v\2\2\u00d3\30\3\2\2\2\u00d4\u00d5\7k\2\2\u00d5\u00d6")
        buf.write("\7h\2\2\u00d6\32\3\2\2\2\u00d7\u00d8\7k\2\2\u00d8\u00d9")
        buf.write("\7p\2\2\u00d9\u00da\7v\2\2\u00da\34\3\2\2\2\u00db\u00dc")
        buf.write("\7x\2\2\u00dc\u00dd\7q\2\2\u00dd\u00de\7k\2\2\u00de\u00df")
        buf.write("\7f\2\2\u00df\36\3\2\2\2\u00e0\u00e1\7t\2\2\u00e1\u00e2")
        buf.write("\7g\2\2\u00e2\u00e3\7v\2\2\u00e3\u00e4\7w\2\2\u00e4\u00e5")
        buf.write("\7t\2\2\u00e5\u00e6\7p\2\2\u00e6 \3\2\2\2\u00e7\u00e8")
        buf.write("\7f\2\2\u00e8\u00e9\7q\2\2\u00e9\"\3\2\2\2\u00ea\u00eb")
        buf.write("\7y\2\2\u00eb\u00ec\7j\2\2\u00ec\u00ed\7k\2\2\u00ed\u00ee")
        buf.write("\7n\2\2\u00ee\u00ef\7g\2\2\u00ef$\3\2\2\2\u00f0\u00f1")
        buf.write("\7v\2\2\u00f1\u00f2\7t\2\2\u00f2\u00f3\7w\2\2\u00f3\u00f4")
        buf.write("\7g\2\2\u00f4&\3\2\2\2\u00f5\u00f6\7h\2\2\u00f6\u00f7")
        buf.write("\7c\2\2\u00f7\u00f8\7n\2\2\u00f8\u00f9\7u\2\2\u00f9\u00fa")
        buf.write("\7g\2\2\u00fa(\3\2\2\2\u00fb\u00fc\7u\2\2\u00fc\u00fd")
        buf.write("\7v\2\2\u00fd\u00fe\7t\2\2\u00fe\u00ff\7k\2\2\u00ff\u0100")
        buf.write("\7p\2\2\u0100\u0101\7i\2\2\u0101*\3\2\2\2\u0102\u0103")
        buf.write("\7-\2\2\u0103,\3\2\2\2\u0104\u0105\7/\2\2\u0105.\3\2\2")
        buf.write("\2\u0106\u0107\7,\2\2\u0107\60\3\2\2\2\u0108\u0109\7\61")
        buf.write("\2\2\u0109\62\3\2\2\2\u010a\u010b\7#\2\2\u010b\64\3\2")
        buf.write("\2\2\u010c\u010d\7~\2\2\u010d\u010e\7~\2\2\u010e\66\3")
        buf.write("\2\2\2\u010f\u0110\7(\2\2\u0110\u0111\7(\2\2\u01118\3")
        buf.write("\2\2\2\u0112\u0113\7?\2\2\u0113\u0114\7?\2\2\u0114:\3")
        buf.write("\2\2\2\u0115\u0116\7\'\2\2\u0116<\3\2\2\2\u0117\u0118")
        buf.write("\7#\2\2\u0118\u0119\7?\2\2\u0119>\3\2\2\2\u011a\u011b")
        buf.write("\7>\2\2\u011b@\3\2\2\2\u011c\u011d\7>\2\2\u011d\u011e")
        buf.write("\7?\2\2\u011eB\3\2\2\2\u011f\u0120\7@\2\2\u0120D\3\2\2")
        buf.write("\2\u0121\u0122\7@\2\2\u0122\u0123\7?\2\2\u0123F\3\2\2")
        buf.write("\2\u0124\u0125\7?\2\2\u0125H\3\2\2\2\u0126\u0127\7]\2")
        buf.write("\2\u0127J\3\2\2\2\u0128\u0129\7_\2\2\u0129L\3\2\2\2\u012a")
        buf.write("\u012b\7}\2\2\u012bN\3\2\2\2\u012c\u012d\7\177\2\2\u012d")
        buf.write("P\3\2\2\2\u012e\u012f\7=\2\2\u012fR\3\2\2\2\u0130\u0131")
        buf.write("\7*\2\2\u0131T\3\2\2\2\u0132\u0133\7+\2\2\u0133V\3\2\2")
        buf.write("\2\u0134\u0135\7.\2\2\u0135X\3\2\2\2\u0136\u013a\t\7\2")
        buf.write("\2\u0137\u0139\t\b\2\2\u0138\u0137\3\2\2\2\u0139\u013c")
        buf.write("\3\2\2\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b")
        buf.write("Z\3\2\2\2\u013c\u013a\3\2\2\2\u013d\u013f\t\t\2\2\u013e")
        buf.write("\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u013e\3\2\2\2")
        buf.write("\u0140\u0141\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0143\b")
        buf.write(".\2\2\u0143\\\3\2\2\2\u0144\u014a\7$\2\2\u0145\u0146\7")
        buf.write("^\2\2\u0146\u0149\t\5\2\2\u0147\u0149\n\6\2\2\u0148\u0145")
        buf.write("\3\2\2\2\u0148\u0147\3\2\2\2\u0149\u014c\3\2\2\2\u014a")
        buf.write("\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014e\3\2\2\2")
        buf.write("\u014c\u014a\3\2\2\2\u014d\u014f\t\n\2\2\u014e\u014d\3")
        buf.write("\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151\b/\4\2\u0151^\3")
        buf.write("\2\2\2\u0152\u0158\7$\2\2\u0153\u0154\7^\2\2\u0154\u0157")
        buf.write("\t\5\2\2\u0155\u0157\n\6\2\2\u0156\u0153\3\2\2\2\u0156")
        buf.write("\u0155\3\2\2\2\u0157\u015a\3\2\2\2\u0158\u0156\3\2\2\2")
        buf.write("\u0158\u0159\3\2\2\2\u0159\u015b\3\2\2\2\u015a\u0158\3")
        buf.write("\2\2\2\u015b\u015c\7^\2\2\u015c\u015d\n\5\2\2\u015d\u015e")
        buf.write("\b\60\5\2\u015e`\3\2\2\2\u015f\u0160\13\2\2\2\u0160b\3")
        buf.write("\2\2\2\27\2iw\177\u0084\u008a\u008d\u0093\u0095\u0099")
        buf.write("\u009e\u00a0\u00a6\u00a8\u013a\u0140\u0148\u014a\u014e")
        buf.write("\u0156\u0158\6\b\2\2\3\6\2\3/\3\3\60\4")
        return buf.getvalue()


class MCLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    BLOCK_COMMENT = 1
    LINE_COMMENT = 2
    INTLIT = 3
    FPLIT = 4
    STRINGLIT = 5
    BOOLEAN = 6
    BREAK = 7
    CONTINUE = 8
    ELSE = 9
    FOR = 10
    FLOAT = 11
    IF = 12
    INTTYPE = 13
    VOIDTYPE = 14
    RETURN = 15
    DO = 16
    WHILE = 17
    TRUE = 18
    FALSE = 19
    STRING = 20
    ADD = 21
    SUB = 22
    MUL = 23
    DIV = 24
    NOT = 25
    OR = 26
    AND = 27
    EQUAL = 28
    MODULUS = 29
    NOTEQUAL = 30
    LT = 31
    LTE = 32
    GT = 33
    GTE = 34
    ASSIGN = 35
    LSB = 36
    RSB = 37
    LP = 38
    RP = 39
    SEMI = 40
    LB = 41
    RB = 42
    COMMA = 43
    ID = 44
    WS = 45
    UNCLOSE_STRING = 46
    ILLEGAL_ESCAPE = 47
    ERROR_CHAR = 48

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'boolean'", "'break'", "'continue'", "'else'", "'for'", "'float'", 
            "'if'", "'int'", "'void'", "'return'", "'do'", "'while'", "'true'", 
            "'false'", "'string'", "'+'", "'-'", "'*'", "'/'", "'!'", "'||'", 
            "'&&'", "'=='", "'%'", "'!='", "'<'", "'<='", "'>'", "'>='", 
            "'='", "'['", "']'", "'{'", "'}'", "';'", "'('", "')'", "','" ]

    symbolicNames = [ "<INVALID>",
            "BLOCK_COMMENT", "LINE_COMMENT", "INTLIT", "FPLIT", "STRINGLIT", 
            "BOOLEAN", "BREAK", "CONTINUE", "ELSE", "FOR", "FLOAT", "IF", 
            "INTTYPE", "VOIDTYPE", "RETURN", "DO", "WHILE", "TRUE", "FALSE", 
            "STRING", "ADD", "SUB", "MUL", "DIV", "NOT", "OR", "AND", "EQUAL", 
            "MODULUS", "NOTEQUAL", "LT", "LTE", "GT", "GTE", "ASSIGN", "LSB", 
            "RSB", "LP", "RP", "SEMI", "LB", "RB", "COMMA", "ID", "WS", 
            "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    ruleNames = [ "BLOCK_COMMENT", "LINE_COMMENT", "INTLIT", "FPLIT", "STRINGLIT", 
                  "BOOLEAN", "BREAK", "CONTINUE", "ELSE", "FOR", "FLOAT", 
                  "IF", "INTTYPE", "VOIDTYPE", "RETURN", "DO", "WHILE", 
                  "TRUE", "FALSE", "STRING", "ADD", "SUB", "MUL", "DIV", 
                  "NOT", "OR", "AND", "EQUAL", "MODULUS", "NOTEQUAL", "LT", 
                  "LTE", "GT", "GTE", "ASSIGN", "LSB", "RSB", "LP", "RP", 
                  "SEMI", "LB", "RB", "COMMA", "ID", "WS", "UNCLOSE_STRING", 
                  "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    grammarFileName = "MC.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def emit(self):
        tk = self.type
        if tk == self.UNCLOSE_STRING:
            result = super().emit();
            raise UncloseString(result.text);
        elif tk == self.ILLEGAL_ESCAPE:
            result = super().emit();
            raise IllegalEscape(result.text);
        elif tk == self.ERROR_CHAR:
            result = super().emit();
            raise ErrorToken(result.text);
        else:
            return super().emit();


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[4] = self.STRINGLIT_action 
            actions[45] = self.UNCLOSE_STRING_action 
            actions[46] = self.ILLEGAL_ESCAPE_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

                    text_String = str(self.text)
                    self.text = text_String[1:-1]
                
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:

                    terminate_Char = ["\b", "\f", "\r", "\n", "\t", "\\", "\""]
                    self.text = self.text[1:-1] if (self.text[-1] in terminate_Char) else self.text[1:]
                
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

                    text_String = str(self.text)
                    self.text = text_String[1:]
                
     



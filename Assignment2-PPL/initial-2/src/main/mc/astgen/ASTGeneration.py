# ID: 1711611
from MCVisitor import MCVisitor
from MCParser import MCParser
from AST import *

class ASTGeneration(MCVisitor):
    def visitProgram(self, ctx:MCParser.ProgramContext):
        declare_List = []
        for obj in ctx.declaration():
            declare = self.visit(obj)
            if (isinstance(declare, list)):
                declare_List.extend(declare)
            else:
                declare_List.append(declare)
        return Program(declare_List)
    
    def visitDeclaration(self, ctx:MCParser.DeclarationContext):
        if (ctx.var_declaration()):
            return self.visit(ctx.var_declaration())
        else:
            return self.visit(ctx.func_declaration())
    
    def visitVar_declaration(self, ctx:MCParser.Var_declarationContext):
        if (ctx.variable()):
            var = self.visit(ctx.variable())
            if (isinstance(var, list)):
                return VarDecl(var[0], ArrayType(var[1], self.visit(ctx.primitive_type())))
            else:
                return VarDecl(var, self.visit(ctx.primitive_type()))
        else:
            var_List = self.visit(ctx.many_variables())
            varDecl_List = []
            for obj in var_List:
                if (isinstance(obj, list)):
                    varDecl_List.append(VarDecl(obj[0], ArrayType(obj[1], self.visit(ctx.primitive_type()))))
                else:
                    varDecl_List.append(VarDecl(obj, self.visit(ctx.primitive_type())))
            return varDecl_List
    
    def visitFunc_declaration(self, ctx:MCParser.Func_declarationContext):
        return FuncDecl(Id(ctx.ID().getText()), self.visit(ctx.parameter_list()), self.visit(ctx.type_return()), self.visit(ctx.block_statement()))
    
    def visitPrimitive_type(self, ctx:MCParser.Primitive_typeContext):
        if (ctx.BOOLEAN()):
            return BoolType()
        elif (ctx.INTTYPE()):
            return IntType()
        elif (ctx.FLOAT()):
            return FloatType()
        else:
            return StringType()
        
    def visitType_return(self, ctx:MCParser.Type_returnContext):
        if (ctx.VOIDTYPE()):
            return VoidType()
        else:
            if (ctx.getChildCount() == 1):
                return self.visit(ctx.primitive_type())
            else:
                return ArrayPointerType(self.visit(ctx.primitive_type()))
    
    def visitVariable(self, ctx:MCParser.VariableContext):
        if (ctx.getChildCount() == 1):
            return ctx.ID().getText()
        else:
            return [ctx.ID().getText(), int(ctx.INTLIT().getText())]
    
    def visitMany_variables(self, ctx:MCParser.Many_variablesContext):
        return [self.visit(x) for x in ctx.variable()]
    
    def visitParameter(self, ctx:MCParser.ParameterContext):
        if (ctx.getChildCount() == 2):
            return VarDecl(ctx.ID().getText(), self.visit(ctx.primitive_type()))
        else:
            return VarDecl(ctx.ID().getText(), ArrayPointerType(self.visit(ctx.primitive_type())))
    
    def visitParameter_list(self, ctx:MCParser.Parameter_listContext):
        if (ctx.getChildCount() == 0):
            return []
        else:
            return [self.visit(ctx.parameter(i)) for i in range(len(ctx.parameter()))]
    
    def visitBlock_statement(self, ctx:MCParser.Block_statementContext):
        if (ctx.getChildCount() == 2):
            return Block([])
        else:
            block_memList = []
            for obj in ctx.block_member():
                temp = self.visit(obj)
                if (isinstance(temp, list)):
                    block_memList.extend(temp)
                else:
                    block_memList.append(temp)
            return Block(block_memList)
    
    def visitBlock_member(self, ctx:MCParser.Block_memberContext):
        if (ctx.var_declaration()):
            return self.visit(ctx.var_declaration())
        else:
            return self.visit(ctx.statement())
    
    def visitStatement(self, ctx:MCParser.StatementContext):
        if (ctx.if_statement()):
            return self.visit(ctx.if_statement())
        elif (ctx.do_while_statement()):
            return self.visit(ctx.do_while_statement())
        elif (ctx.for_statement()):
            return self.visit(ctx.for_statement())
        elif (ctx.break_statement()):
            return self.visit(ctx.break_statement())
        elif (ctx.continue_statement()):
            return self.visit(ctx.continue_statement())
        elif (ctx.return_statement()):
            return self.visit(ctx.return_statement())
        elif (ctx.expression_statement()):
            return self.visit(ctx.expression_statement())
        else:
            return self.visit(ctx.block_statement())
    
    def visitIf_statement(self, ctx:MCParser.If_statementContext):
        if (ctx.ELSE()):
            return If(self.visit(ctx.exp()), self.visit(ctx.statement(0)), self.visit(ctx.statement(1)))
        else:
            return If(self.visit(ctx.exp()), self.visit(ctx.statement(0)))
    
    def visitDo_while_statement(self, ctx:MCParser.Do_while_statementContext):
        return Dowhile([self.visit(ctx.statement(i)) for i in range(len(ctx.statement()))],self.visit(ctx.exp()))
    
    def visitFor_statement(self, ctx:MCParser.For_statementContext):
        return For(self.visit(ctx.exp(0)), self.visit(ctx.exp(1)),self.visit(ctx.exp(2)), self.visit(ctx.statement()))

    def visitBreak_statement(self, ctx:MCParser.Break_statementContext):
        return Break()
    
    def visitContinue_statement(self, ctx:MCParser.Continue_statementContext):
        return Continue()
    
    def visitReturn_statement(self, ctx:MCParser.Return_statementContext):
        if (ctx.exp()):
            return Return(self.visit(ctx.exp()))
        else:
            return Return()
        
    def visitExpression_statement(self, ctx:MCParser.Expression_statementContext):
        return self.visit(ctx.exp())
    
    def visitExp(self, ctx:MCParser.ExpContext):
        if (ctx.getChildCount() == 3):
            return BinaryOp(ctx.ASSIGN().getText(), self.visit(ctx.exp1()), self.visit(ctx.exp()))
        else:
            return self.visit(ctx.exp1())
    
    def visitExp1(self, ctx:MCParser.Exp1Context):
        if (ctx.getChildCount() == 3):
            return BinaryOp(ctx.OR().getText(), self.visit(ctx.exp1()), self.visit(ctx.exp2()))
        else:
            return self.visit(ctx.exp2())

    def visitExp2(self, ctx:MCParser.Exp2Context):
        if (ctx.getChildCount() == 3):
            return BinaryOp(ctx.AND().getText(), self.visit(ctx.exp2()), self.visit(ctx.exp3()))
        else:
            return self.visit(ctx.exp3())

    def visitExp3(self, ctx:MCParser.Exp3Context):
        if (ctx.getChildCount() == 3):
            return BinaryOp(ctx.EQUAL().getText() if (ctx.EQUAL()) else ctx.NOTEQUAL().getText(), self.visit(ctx.exp4(0)), self.visit(ctx.exp4(1)))
        else:
            return self.visit(ctx.exp4(0))

    def visitExp4(self, ctx:MCParser.Exp4Context):
        op = ""
        if (ctx.GT()):
            op = ctx.GT().getText()
        if (ctx.GTE()):
            op = ctx.GTE().getText()
        if (ctx.LT()):
            op = ctx.LT().getText()
        if (ctx.LTE()):
            op = ctx.LTE().getText()
        if (ctx.getChildCount() == 3):
            return BinaryOp(op, self.visit(ctx.exp5(0)), self.visit(ctx.exp5(1)))
        else:
            return self.visit(ctx.exp5(0))

    def visitExp5(self, ctx:MCParser.Exp5Context):
        if (ctx.getChildCount() == 3):
            return BinaryOp(ctx.ADD().getText() if (ctx.ADD()) else ctx.SUB().getText(), self.visit(ctx.exp5()), self.visit(ctx.exp6()))
        else:
            return self.visit(ctx.exp6())

    def visitExp6(self, ctx:MCParser.Exp6Context):
        op = ""
        if (ctx.DIV()):
            op = ctx.DIV().getText()
        if (ctx.MUL()):
            op = ctx.MUL().getText()
        if (ctx.MODULUS()):
            op = ctx.MODULUS().getText()
        if (ctx.getChildCount() == 3):
            return BinaryOp(op, self.visit(ctx.exp6()), self.visit(ctx.exp7()))
        else:
            return self.visitExp7(ctx.exp7())

    def visitExp7(self, ctx:MCParser.Exp7Context):
        if (ctx.getChildCount() == 2):
            return UnaryOp(ctx.NOT().getText() if (ctx.NOT()) else ctx.SUB().getText(), self.visit(ctx.exp7()))
        else:
            return self.visit(ctx.exp8())

    def visitExp8(self, ctx:MCParser.Exp8Context):
        if (ctx.getChildCount() == 4):
            return ArrayCell(self.visit(ctx.exp9(0)), self.visit(ctx.exp9(1)))
        else:
            return self.visit(ctx.exp9(0))

    def visitExp9(self, ctx:MCParser.Exp9Context):
        return self.visit(ctx.operand())

    def visitOperand(self, ctx:MCParser.OperandContext):
        if (ctx.getChildCount() == 3):
            return self.visit(ctx.exp())
        elif (ctx.getChildCount() == 4):
            return ArrayCell(self.visit(ctx.operand()), self.visit(ctx.exp()))
        else:
            if (ctx.literal()):
                return self.visit(ctx.literal())
            elif (ctx.ID()):
                return Id(ctx.ID().getText())
            else:
                return self.visit(ctx.func_call())

    def visitLiteral(self, ctx:MCParser.LiteralContext):
        if (ctx.INTLIT()):
            return IntLiteral(int(ctx.INTLIT().getText()))
        elif (ctx.STRINGLIT()):
            return StringLiteral(ctx.STRINGLIT().getText())
        elif (ctx.FPLIT()):
            return FloatLiteral(float(ctx.FPLIT().getText()))
        else:
            return self.visit(ctx.booleanlit())

    def visitFunc_call(self, ctx:MCParser.Func_callContext):
        return CallExpr(Id(ctx.ID().getText()), [self.visit(ctx.exp(i)) for i in range(len(ctx.exp()))])

    def visitBooleanlit(self, ctx:MCParser.BooleanlitContext):
        return BooleanLiteral(value = True if ctx.TRUE() else False)

        
    

    


    

    


import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program"""
        input = r"""int main() {}"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,300))

    def test_simple_program1(self):
        """Simple program"""
        input = r"""int main() {}
                   float b;
                   int a[5];
                """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([])),VarDecl(b,FloatType),VarDecl(a,ArrayType(IntType,5))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,301))

    def test_simple_program2(self):
        """Simple program"""
        input = r"""
                    int a;
                    float h,f;
                    string b, c[5];
                """
        expect = "Program([VarDecl(a,IntType),VarDecl(h,FloatType),VarDecl(f,FloatType),VarDecl(b,StringType),VarDecl(c,ArrayType(StringType,5))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,302))
    
    def test_simple_program3(self):
        """Simple program"""
        input = r"""
                    int main(){}
                    float wors(){}
                    boolean sidof(){}
                    int[] sk(){}
                    float main(){}
                """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([])),FuncDecl(Id(wors),[],FloatType,Block([])),FuncDecl(Id(sidof),[],BoolType,Block([])),FuncDecl(Id(sk),[],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(main),[],FloatType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,303))

    def test_simple_program4(self):
        """Simple program"""
        input = r"""
                    string b, c[5];
                    int main(){}
                    float h,f;
                    int[] sk(){}
                    int main(){}
                    float main(){}
                    int a;
                    int skw(int a, float b, float b[], boolean c[]){}
                """
        expect = "Program([VarDecl(b,StringType),VarDecl(c,ArrayType(StringType,5)),FuncDecl(Id(main),[],IntType,Block([])),VarDecl(h,FloatType),VarDecl(f,FloatType),FuncDecl(Id(sk),[],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(main),[],IntType,Block([])),FuncDecl(Id(main),[],FloatType,Block([])),VarDecl(a,IntType),FuncDecl(Id(skw),[VarDecl(a,IntType),VarDecl(b,FloatType),VarDecl(b,ArrayTypePointer(FloatType)),VarDecl(c,ArrayTypePointer(BoolType))],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,304))
    
    def test_var_decl1(self):
        """ Test Variable Declare """
        input = r"""int a,b,c[3],sksad[2310];"""
        expect = "Program([VarDecl(a,IntType),VarDecl(b,IntType),VarDecl(c,ArrayType(IntType,3)),VarDecl(sksad,ArrayType(IntType,2310))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,305))
    
    def test_var_decl2(self):
        """ Test Variable Declare """
        input = r"""int a;
                   int b;
                   int c[3];
                   int sksad[2310];"""
        expect = "Program([VarDecl(a,IntType),VarDecl(b,IntType),VarDecl(c,ArrayType(IntType,3)),VarDecl(sksad,ArrayType(IntType,2310))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,306))
    
    def test_var_decl3(self):
        """ Test Variable Declare """
        input = r"""string a,s[23];
                   boolean iwoeiow[24214];
                   float e[31], k, sd;
                   """
        expect = "Program([VarDecl(a,StringType),VarDecl(s,ArrayType(StringType,23)),VarDecl(iwoeiow,ArrayType(BoolType,24214)),VarDecl(e,ArrayType(FloatType,31)),VarDecl(k,FloatType),VarDecl(sd,FloatType)])"
        self.assertTrue(TestAST.checkASTGen(input,expect,307))
    
    def test_var_decl4(self):
        """ Test Variable Declare """
        input = r"""int a[1], b[5], c[0], d[100];"""
        expect = "Program([VarDecl(a,ArrayType(IntType,1)),VarDecl(b,ArrayType(IntType,5)),VarDecl(c,ArrayType(IntType,0)),VarDecl(d,ArrayType(IntType,100))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,308))
    
    def test_var_decl5(self):
        """ Test Variable Declare """
        input = r"""float a;"""
        expect = "Program([VarDecl(a,FloatType)])"
        self.assertTrue(TestAST.checkASTGen(input,expect,309))
    
    def test_var_decl6(self):
        """ Test Variable Declare """
        input = r"""int a[1], b[5], c, d; float c, d; int e[10];
        string a, b, c, d[5], o[0];
        boolean n, m,  t, abc;
        boolean abc; boolean abc_____xyz;
        """
        expect = "Program([VarDecl(a,ArrayType(IntType,1)),VarDecl(b,ArrayType(IntType,5)),VarDecl(c,IntType),VarDecl(d,IntType),VarDecl(c,FloatType),VarDecl(d,FloatType),VarDecl(e,ArrayType(IntType,10)),VarDecl(a,StringType),VarDecl(b,StringType),VarDecl(c,StringType),VarDecl(d,ArrayType(StringType,5)),VarDecl(o,ArrayType(StringType,0)),VarDecl(n,BoolType),VarDecl(m,BoolType),VarDecl(t,BoolType),VarDecl(abc,BoolType),VarDecl(abc,BoolType),VarDecl(abc_____xyz,BoolType)])"
        self.assertTrue(TestAST.checkASTGen(input,expect,310))
    
    def test_var_decl7(self):
        """ Test Variable Declare """
        input = r"""int a;
        string a; float __a[1]; boolean m[0];
        string abc[2]; string abc; string ac;
        float a,b, c[10], c[10000000000], d[999999999];
        boolean abc[1000], a; int abc, m[199999999];
        """
        expect = "Program([VarDecl(a,IntType),VarDecl(a,StringType),VarDecl(__a,ArrayType(FloatType,1)),VarDecl(m,ArrayType(BoolType,0)),VarDecl(abc,ArrayType(StringType,2)),VarDecl(abc,StringType),VarDecl(ac,StringType),VarDecl(a,FloatType),VarDecl(b,FloatType),VarDecl(c,ArrayType(FloatType,10)),VarDecl(c,ArrayType(FloatType,10000000000)),VarDecl(d,ArrayType(FloatType,999999999)),VarDecl(abc,ArrayType(BoolType,1000)),VarDecl(a,BoolType),VarDecl(abc,IntType),VarDecl(m,ArrayType(IntType,199999999))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,311))
    
    def test_var_decl8(self):
        """ Test Variable Declare """
        input = r"""int a;
        float a; float _____________________skfa[1]; boolean m[0];
        int abc[2]; string abc; string ac;
        boolean a,b, c[10], c[10321124000000], d[42113243243];
        boolean abc[1000324332], dsadas; int abc, sda[1329994249];
        """
        expect = "Program([VarDecl(a,IntType),VarDecl(a,FloatType),VarDecl(_____________________skfa,ArrayType(FloatType,1)),VarDecl(m,ArrayType(BoolType,0)),VarDecl(abc,ArrayType(IntType,2)),VarDecl(abc,StringType),VarDecl(ac,StringType),VarDecl(a,BoolType),VarDecl(b,BoolType),VarDecl(c,ArrayType(BoolType,10)),VarDecl(c,ArrayType(BoolType,10321124000000)),VarDecl(d,ArrayType(BoolType,42113243243)),VarDecl(abc,ArrayType(BoolType,1000324332)),VarDecl(dsadas,BoolType),VarDecl(abc,IntType),VarDecl(sda,ArrayType(IntType,1329994249))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,312))
    
    def test_var_decl9(self):
        """ Test Variable Declare """
        input = r"""string a[5];
        """
        expect = "Program([VarDecl(a,ArrayType(StringType,5))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,313))

    def test_var_decl10(self):
        """ Test Variable Declare """
        input = r"""boolean ______________Phoenix_____________[181299];
        """
        expect = "Program([VarDecl(______________Phoenix_____________,ArrayType(BoolType,181299))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,314))
    
    def test_func_decl1(self):
        """ Test Function Declare """
        input = r"""boolean ____Dsadass___(){}
        """
        expect = "Program([FuncDecl(Id(____Dsadass___),[],BoolType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,315))
    
    def test_func_decl2(self):
        """ Test Function Declare """
        input = r"""int[] main(){}
        float[] main(){}
        string[] main(){}
        boolean[] main(){}"""
        expect = "Program([FuncDecl(Id(main),[],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(main),[],ArrayTypePointer(FloatType),Block([])),FuncDecl(Id(main),[],ArrayTypePointer(StringType),Block([])),FuncDecl(Id(main),[],ArrayTypePointer(BoolType),Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,316))
    
    def test_func_decl3(self):
        """ Test Function Declare """
        input = r"""int a(){}
        float[] b(){}
        string c(){}
        boolean[] d(){}
        float main(){}
        """
        expect = "Program([FuncDecl(Id(a),[],IntType,Block([])),FuncDecl(Id(b),[],ArrayTypePointer(FloatType),Block([])),FuncDecl(Id(c),[],StringType,Block([])),FuncDecl(Id(d),[],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(main),[],FloatType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,317))
    
    def test_func_decl4(self):
        """ Test Function Declare """
        input = r"""int a(){}
        float b(){}
        string c(){}
        boolean d(){}
        int[] d(){}
        float[] s(){}
        """
        expect = "Program([FuncDecl(Id(a),[],IntType,Block([])),FuncDecl(Id(b),[],FloatType,Block([])),FuncDecl(Id(c),[],StringType,Block([])),FuncDecl(Id(d),[],BoolType,Block([])),FuncDecl(Id(d),[],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(s),[],ArrayTypePointer(FloatType),Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,318))
    
    def test_func_decl5(self):
        """ Test Function Declare """
        input = r"""int main(int a, float b, string c, boolean d, int main){}
        """
        expect = "Program([FuncDecl(Id(main),[VarDecl(a,IntType),VarDecl(b,FloatType),VarDecl(c,StringType),VarDecl(d,BoolType),VarDecl(main,IntType)],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,319))
    
    def test_func_decl6(self):
        """ Test Function Declare """
        input = r"""int main(int a){}
        int main(float b){}
        int main(string c){}
        int main(boolean d){}
        int main(int main){}
        """
        expect = "Program([FuncDecl(Id(main),[VarDecl(a,IntType)],IntType,Block([])),FuncDecl(Id(main),[VarDecl(b,FloatType)],IntType,Block([])),FuncDecl(Id(main),[VarDecl(c,StringType)],IntType,Block([])),FuncDecl(Id(main),[VarDecl(d,BoolType)],IntType,Block([])),FuncDecl(Id(main),[VarDecl(main,IntType)],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,320))
    
    def test_func_decl7(self):
        """ Test Function Declare """
        input = r"""int main(int a[], float b, string c, boolean d){}
        float[] main(int a, float b[], string c, boolean d){}
        string[] main(int a, float b, string c[], boolean d){}
        boolean[] main(int a, float b[], string c, boolean d[]){}
        int main(int a[], float b[], string c[], boolean d[]){}
        """
        expect = "Program([FuncDecl(Id(main),[VarDecl(a,ArrayTypePointer(IntType)),VarDecl(b,FloatType),VarDecl(c,StringType),VarDecl(d,BoolType)],IntType,Block([])),FuncDecl(Id(main),[VarDecl(a,IntType),VarDecl(b,ArrayTypePointer(FloatType)),VarDecl(c,StringType),VarDecl(d,BoolType)],ArrayTypePointer(FloatType),Block([])),FuncDecl(Id(main),[VarDecl(a,IntType),VarDecl(b,FloatType),VarDecl(c,ArrayTypePointer(StringType)),VarDecl(d,BoolType)],ArrayTypePointer(StringType),Block([])),FuncDecl(Id(main),[VarDecl(a,IntType),VarDecl(b,ArrayTypePointer(FloatType)),VarDecl(c,StringType),VarDecl(d,ArrayTypePointer(BoolType))],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(main),[VarDecl(a,ArrayTypePointer(IntType)),VarDecl(b,ArrayTypePointer(FloatType)),VarDecl(c,ArrayTypePointer(StringType)),VarDecl(d,ArrayTypePointer(BoolType))],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,321))
    
    def test_func_decl8(self):
        """ Test Function Declare """
        input = r"""int main(){}
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,322))

    def test_func_decl9(self):
        """ Test Function Declare """
        input = r"""int main(string args[]){}
        int[] __str__(string a, string exception, boolean b[], float a){}
        boolean[] __abc(boolean isTrue){}
        float pi(float pi){}
        """
        expect = "Program([FuncDecl(Id(main),[VarDecl(args,ArrayTypePointer(StringType))],IntType,Block([])),FuncDecl(Id(__str__),[VarDecl(a,StringType),VarDecl(exception,StringType),VarDecl(b,ArrayTypePointer(BoolType)),VarDecl(a,FloatType)],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(__abc),[VarDecl(isTrue,BoolType)],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(pi),[VarDecl(pi,FloatType)],FloatType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,323))
    
    def test_func_decl10(self):
        """ Test Function Declare """
        input = r"""int main(string args[]){}
        int[] __str__(string a, string exception, boolean b[], float a){}
        boolean[] __abc(boolean isTrue){}
        float pi(float pi){}
        boolean[] a4a2423(string args[], boolean sdasd____[]){}
        int[] __str1__(string a, string exception, boolean b[], float a){}
        boolean[] __abc(boolean isTrue){}
        float __adassdasas21312ds(int i219ddjasid, float sd_312__21[], string dsoasjdo__12ss__dsasdaa1221[]){}
        """
        expect = "Program([FuncDecl(Id(main),[VarDecl(args,ArrayTypePointer(StringType))],IntType,Block([])),FuncDecl(Id(__str__),[VarDecl(a,StringType),VarDecl(exception,StringType),VarDecl(b,ArrayTypePointer(BoolType)),VarDecl(a,FloatType)],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(__abc),[VarDecl(isTrue,BoolType)],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(pi),[VarDecl(pi,FloatType)],FloatType,Block([])),FuncDecl(Id(a4a2423),[VarDecl(args,ArrayTypePointer(StringType)),VarDecl(sdasd____,ArrayTypePointer(BoolType))],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(__str1__),[VarDecl(a,StringType),VarDecl(exception,StringType),VarDecl(b,ArrayTypePointer(BoolType)),VarDecl(a,FloatType)],ArrayTypePointer(IntType),Block([])),FuncDecl(Id(__abc),[VarDecl(isTrue,BoolType)],ArrayTypePointer(BoolType),Block([])),FuncDecl(Id(__adassdasas21312ds),[VarDecl(i219ddjasid,IntType),VarDecl(sd_312__21,ArrayTypePointer(FloatType)),VarDecl(dsoasjdo__12ss__dsasdaa1221,ArrayTypePointer(StringType))],FloatType,Block([]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,324))
    
    def test_IfStatement1(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
                a = a + 1;
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,325))
    
    def test_IfStatement2(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
                a = a + 1;
            else
                a = a + b;
            }
                
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(+,Id(a),Id(b))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,326))
    
    def test_IfStatement3(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
                a = a + 1;
                a = a + b;
                2 == 3;
                b = 2;
                a || b;
                b && (a || c);
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)))),BinaryOp(=,Id(a),BinaryOp(+,Id(a),Id(b))),BinaryOp(==,IntLiteral(2),IntLiteral(3)),BinaryOp(=,Id(b),IntLiteral(2)),BinaryOp(||,Id(a),Id(b)),BinaryOp(&&,Id(b),BinaryOp(||,Id(a),Id(c)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,327))
    
    def test_IfStatement4(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
            {
                a = a + 1;
                b = 2;
                b && (a || c);
                if (true)
                    a = 1;
                else
                    a = 2;
            }
            else
                a = 4;
            
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),Block([BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(=,Id(b),IntLiteral(2)),BinaryOp(&&,Id(b),BinaryOp(||,Id(a),Id(c))),If(BooleanLiteral(true),BinaryOp(=,Id(a),IntLiteral(1)),BinaryOp(=,Id(a),IntLiteral(2)))]),BinaryOp(=,Id(a),IntLiteral(4)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,328))
    
    def test_IfStatement5(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
            {
                a = a + 1;
                if (b == 1)
                {
                    b = b + 1;
                    if (c == 1)
                    {
                        c = c + 1;
                        if (d == 1)
                            d = d + 1;
                        else
                            d || c;
                    }
                    else
                        c || b;
                }
                else
                    a || b;
            }
            else
                1 || (a + 23 - 321 || 3213);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),Block([BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))),If(BinaryOp(==,Id(b),IntLiteral(1)),Block([BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))),If(BinaryOp(==,Id(c),IntLiteral(1)),Block([BinaryOp(=,Id(c),BinaryOp(+,Id(c),IntLiteral(1))),If(BinaryOp(==,Id(d),IntLiteral(1)),BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1))),BinaryOp(||,Id(d),Id(c)))]),BinaryOp(||,Id(c),Id(b)))]),BinaryOp(||,Id(a),Id(b)))]),BinaryOp(||,IntLiteral(1),BinaryOp(||,BinaryOp(-,BinaryOp(+,Id(a),IntLiteral(23)),IntLiteral(321)),IntLiteral(3213))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,329))
    
    def test_IfStatement6(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
            {
                (2 == 3);
                a == 2;
                a = 2;
            }
            else
            {
                b = a + 1;
                a[2] + 229132 - 212.2313;
                4 / 3 + 234 + a%3214 - 13.2141;
                a / 493849 - 32141 + 321412 - !a + true - false;
                func(23412, 31232, 145 - 3213, 214/3) + !ad - true + false;
            }
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),Block([BinaryOp(==,IntLiteral(2),IntLiteral(3)),BinaryOp(==,Id(a),IntLiteral(2)),BinaryOp(=,Id(a),IntLiteral(2))]),Block([BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(a),IntLiteral(2)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141)),BinaryOp(-,BinaryOp(+,BinaryOp(-,BinaryOp(+,BinaryOp(-,BinaryOp(/,Id(a),IntLiteral(493849)),IntLiteral(32141)),IntLiteral(321412)),UnaryOp(!,Id(a))),BooleanLiteral(true)),BooleanLiteral(false)),BinaryOp(+,BinaryOp(-,BinaryOp(+,CallExpr(Id(func),[IntLiteral(23412),IntLiteral(31232),BinaryOp(-,IntLiteral(145),IntLiteral(3213)),BinaryOp(/,IntLiteral(214),IntLiteral(3))]),UnaryOp(!,Id(ad))),BooleanLiteral(true)),BooleanLiteral(false))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,330))
    
    def test_IfStatement7(self):
        """ Test Statement """
        input = r"""void main(){
            int a;
            a = true;
            if (true){
                if (a == true){
                    if (!a){
                        a = false;
                        string b;
                        b = a;
                    }
                    else{
                        string b;
                        b = a;
                    }
                }
                else{
                    a = false;
                }
            }
        }
        """
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BooleanLiteral(true)),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]),Block([VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]))]),Block([BinaryOp(=,Id(a),BooleanLiteral(false))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,331))
    
    def test_IfStatement8(self):
        """ Test Statement """
        input = r"""int main(){
            int a;
            a = true;
            if (true){
                if (a == true){
                    if (!a){
                        321414 / a + 312314.e6 - 312441 % 2311414;
                        b = a + 1;
                        a[2] + 229132 - 212.2313;
                        4 / 3 + 234 + a%3214 - 13.2141;
                        493849 - 32141 + 321412 - !a + true - false;
                        func(23412, 31232, 145 - 3213, 214/3) + !ad - true + false;
                    }
                    else{
                        a = a + 1;
                        a = a * 2;
                        a = "string";
                    }
                }
                else{
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                }
            }
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BooleanLiteral(true)),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(a),IntLiteral(2)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141)),BinaryOp(-,BinaryOp(+,BinaryOp(-,BinaryOp(+,BinaryOp(-,IntLiteral(493849),IntLiteral(32141)),IntLiteral(321412)),UnaryOp(!,Id(a))),BooleanLiteral(true)),BooleanLiteral(false)),BinaryOp(+,BinaryOp(-,BinaryOp(+,CallExpr(Id(func),[IntLiteral(23412),IntLiteral(31232),BinaryOp(-,IntLiteral(145),IntLiteral(3213)),BinaryOp(/,IntLiteral(214),IntLiteral(3))]),UnaryOp(!,Id(ad))),BooleanLiteral(true)),BooleanLiteral(false))]),Block([BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(*,Id(a),IntLiteral(2))),BinaryOp(=,Id(a),StringLiteral(string))]))]),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,332))
    
    def test_IfStatement9(self):
        """ Test Statement """
        input = r"""int main(){
            if (a == 1)
            {
                a = false + true - false % true || false && true % false;
                321414 / a + 312314.e6 - 312441 % 2311414;
                b = a + 1;
                sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                4 / 3 + 234 + a%3214 - 13.2141;
                if (b == 1)
                {
                    b = b + 1;
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                    if (c == 1)
                    {
                        a = false + true - false % true || false && true % false;
                        321414 / a + 312314.e6 - 312441 % 2311414;
                        b = a + 1;
                        sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                        4 / 3 + 234 + a%3214 - 13.2141;
                        if (d == 1)
                            d = d + 1;
                        else
                            d || c;
                    }
                    else
                        c || b;
                }
                else
                    a || b;
            }
            else
                1 || (a + 23 - 321 || 3213);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BinaryOp(==,Id(a),IntLiteral(1)),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141)),If(BinaryOp(==,Id(b),IntLiteral(1)),Block([BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141)),If(BinaryOp(==,Id(c),IntLiteral(1)),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141)),If(BinaryOp(==,Id(d),IntLiteral(1)),BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1))),BinaryOp(||,Id(d),Id(c)))]),BinaryOp(||,Id(c),Id(b)))]),BinaryOp(||,Id(a),Id(b)))]),BinaryOp(||,IntLiteral(1),BinaryOp(||,BinaryOp(-,BinaryOp(+,Id(a),IntLiteral(23)),IntLiteral(321)),IntLiteral(3213))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,333))
    
    def test_IfStatement10(self):
        """ Test Statement """
        input = r"""void main(){
            int a;
            a = true;
            if (true){
                if (a == true){
                    if (!a){
                        a = false;
                        string b;
                        b = a;
                        if (b){
                            boolean c;
                            c = b;
                            if (!c){
                                int d;
                                d = c;
                                if (d == c || !c){
                                    string e;
                                    e = d;
                                }
                                else{
                                    string e;
                                    e = d;
                                }
                            }
                            else{
                                int d;
                                d = e;
                                boolean t;
                                t = e;
                                if (d && !e){
                                    string t;
                                    t = d;
                                }
                            }
                        }
                    }
                    else{
                        if ((a == b || c != b) && a > b){
                            int e;
                            e = a;
                        }
                    }
                }
            }
        }
        """
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BooleanLiteral(true)),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a)),If(Id(b),Block([VarDecl(c,BoolType),BinaryOp(=,Id(c),Id(b)),If(UnaryOp(!,Id(c)),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(c)),If(BinaryOp(||,BinaryOp(==,Id(d),Id(c)),UnaryOp(!,Id(c))),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]))]),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(e)),VarDecl(t,BoolType),BinaryOp(=,Id(t),Id(e)),If(BinaryOp(&&,Id(d),UnaryOp(!,Id(e))),Block([VarDecl(t,StringType),BinaryOp(=,Id(t),Id(d))]))]))]))]),Block([If(BinaryOp(&&,BinaryOp(||,BinaryOp(==,Id(a),Id(b)),BinaryOp(!=,Id(c),Id(b))),BinaryOp(>,Id(a),Id(b))),Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(a))]))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,334))
        
    def test_Forstatement1(self):
        """ Test For Statement """
        input = r"""
            int hunG() {
                for ( x = 1 ; x < 3 ; x = x + 1 ) a = a + 2 ;
            }
        """
        expect = "Program([FuncDecl(Id(hunG),[],IntType,Block([For(BinaryOp(=,Id(x),IntLiteral(1));BinaryOp(<,Id(x),IntLiteral(3));BinaryOp(=,Id(x),BinaryOp(+,Id(x),IntLiteral(1)));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(2))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,335))
    
    def test_Forstatement2(self):
        """ Test For Statement """
        input = r"""
            void main(){
            int a;
            int b;
            for (a=1; a < 10; a=a+1)
                b = b + 1;
        }
        """
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,IntType),VarDecl(b,IntType),For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(10));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,336))
    
    def test_Forstatement3(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (a=1; a < 10; a=a+1){
                    if (a + 123.4 - 241%a - true/false + false == 0){
                        int d;
                        d = e;
                        boolean t;
                        t = e;
                    }
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(10));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(+,BinaryOp(-,BinaryOp(-,BinaryOp(+,Id(a),FloatLiteral(123.4)),BinaryOp(%,IntLiteral(241),Id(a))),BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false))),BooleanLiteral(false)),IntLiteral(0)),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(e)),VarDecl(t,BoolType),BinaryOp(=,Id(t),Id(e))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,337))
    
    def test_Forstatement4(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (i = 1; -i < 10; i = i + 1)
                {
                    for (j = 1; j < 200; j = j + 1)
                    {
                        if (i == j = a - b + c % 4312 - 1e231)
                        {
                            a = false + true - false % true || false && true % false;
                            321414 / a + 312314.e6 - 312441 % 2311414;
                            b = a + 1;
                            sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                            4 / 3 + 234 + a%3214 - 13.2141;
                        }
                    }   
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,UnaryOp(-,Id(i)),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(200));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([If(BinaryOp(=,BinaryOp(==,Id(i),Id(j)),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),Id(b)),BinaryOp(%,Id(c),IntLiteral(4312))),FloatLiteral(1e+231))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,338))
    
    def test_Forstatement5(self):
        """ Test For Statement """
        input = r"""
                int main(){
                    for (i = 1; -i < 10; i = i - 1)
                    {
                        for (j = 1; j < 200; j = j + 1)
                        {
                            if (i == j = a - b + c % 4312 - 1e231)
                            {
                                a = false + true - false % true || false && true % false;
                                321414 / a + 312314.e6 - 312441 % 2311414;
                                b = a + 1;
                                sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                                4 / 3 + 234 + a%3214 - 13.2141;
                            }   
                            else
                            {
                                int a;
                                a = true;
                                if (true){
                                    if (a == true){
                                        if (!a){
                                            a = false;
                                            string b;
                                            b = a;
                                        }
                                        else{
                                            string b;
                                            b = a;
                                        }
                                    }
                                    else{
                                        a = false;
                                    }
                                }
                            }
                        }   
                    }
                }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,UnaryOp(-,Id(i)),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(-,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(200));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([If(BinaryOp(=,BinaryOp(==,Id(i),Id(j)),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),Id(b)),BinaryOp(%,Id(c),IntLiteral(4312))),FloatLiteral(1e+231))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]),Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BooleanLiteral(true)),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]),Block([VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]))]),Block([BinaryOp(=,Id(a),BooleanLiteral(false))]))]))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,339))
    
    def test_Forstatement6(self):
        """ Test For Statement """
        input = r"""
            int main(){
                int a;
                float b;
                string c;
                for (a=1; a % 10 == 0; a=a+1){
                    if (a % 2 == 0){
                        for (b=0; b != 1;b=b+2){
                            int a;
                            float b;
                            b = a;
                            for (b=1;b==10;b=b+1){
                                string c;
                                c = b;
                                if (c){
                                    float a;
                                    string d;
                                    d = c;
                                }
                            }
                        }
                    }
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),VarDecl(b,FloatType),VarDecl(c,StringType),For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(10)),IntLiteral(0));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(2)),IntLiteral(0)),Block([For(BinaryOp(=,Id(b),IntLiteral(0));BinaryOp(!=,Id(b),IntLiteral(1));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,FloatType),BinaryOp(=,Id(b),Id(a)),For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));Block([VarDecl(c,StringType),BinaryOp(=,Id(c),Id(b)),If(Id(c),Block([VarDecl(a,FloatType),VarDecl(d,StringType),BinaryOp(=,Id(d),Id(c))]))]))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,340))
    
    def test_Fortatement7(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (i = 1; i < 1002401; i = i + 1)
                {
                    for (j = 1; j < 1002401; j = j + 1)
                    {
                        for (k = 1; k < 1002401; k = k + 1)
                        {
                            for (h = 1; h < 1002401; h = h + 1)
                            {
                                for (t = 1; t < 1002401; t = t + 1)
                                {
                                    for (a = 1; a < 1002401; a = a + 1)
                                    {
                                        for (b = 1; b < 1002401; b = b + 1)
                                            for (c = 1; c < 1002401; c = c + 1)
                                                for (d = 1; d < 1002401; d = d + 1)
                                                    for (e = 1; e < 1002401; e = e + 1)
                                                        for (f = 1; f < 1002401; f = f + 1)
                                                            (i + j + k + h + t + a + b + c + d + e + f);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,Id(i),IntLiteral(1002401));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(1002401));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([For(BinaryOp(=,Id(k),IntLiteral(1));BinaryOp(<,Id(k),IntLiteral(1002401));BinaryOp(=,Id(k),BinaryOp(+,Id(k),IntLiteral(1)));Block([For(BinaryOp(=,Id(h),IntLiteral(1));BinaryOp(<,Id(h),IntLiteral(1002401));BinaryOp(=,Id(h),BinaryOp(+,Id(h),IntLiteral(1)));Block([For(BinaryOp(=,Id(t),IntLiteral(1));BinaryOp(<,Id(t),IntLiteral(1002401));BinaryOp(=,Id(t),BinaryOp(+,Id(t),IntLiteral(1)));Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(1002401));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(<,Id(b),IntLiteral(1002401));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));For(BinaryOp(=,Id(c),IntLiteral(1));BinaryOp(<,Id(c),IntLiteral(1002401));BinaryOp(=,Id(c),BinaryOp(+,Id(c),IntLiteral(1)));For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(<,Id(d),IntLiteral(1002401));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));For(BinaryOp(=,Id(e),IntLiteral(1));BinaryOp(<,Id(e),IntLiteral(1002401));BinaryOp(=,Id(e),BinaryOp(+,Id(e),IntLiteral(1)));For(BinaryOp(=,Id(f),IntLiteral(1));BinaryOp(<,Id(f),IntLiteral(1002401));BinaryOp(=,Id(f),BinaryOp(+,Id(f),IntLiteral(1)));BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,Id(i),Id(j)),Id(k)),Id(h)),Id(t)),Id(a)),Id(b)),Id(c)),Id(d)),Id(e)),Id(f)))))))]))]))]))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,341))
    
    def test_Forstatement8(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (a=1; a % 10 == 0; a=a*2){
                        if (a % 2 == 0){
                            for (b=0; b != 1;b=b*2){
                                int a;
                                float b;
                                b = a;
                                for (b=1;b==10;b=b*2){
                                    string c;
                                    c = b;
                                    if (c){
                                        float a;
                                        string d;
                                        d = c;
                                    }
                                }
                            }
                        }
                    }
                for (a=1; a % 10 == 0; a=a+1){
                    if (a % 2 == 0){
                        for (b=0; b != 1;b=b+2){
                            int a;
                            float b;
                            b = a;
                            for (b=1;b==10;b=b+1){
                                string c;
                                c = b;
                                if (b % c == 2){
                                    b = b + 1;
                                    a = false + true - false % true || false && true % false;
                                    321414 / a + 312314.e6 - 312441 % 2311414;
                                    b = a + 1;
                                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                                    4 / 3 + 234 + a%3214 - 13.2141;
                                }
                            }
                        }
                    }
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(10)),IntLiteral(0));BinaryOp(=,Id(a),BinaryOp(*,Id(a),IntLiteral(2)));Block([If(BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(2)),IntLiteral(0)),Block([For(BinaryOp(=,Id(b),IntLiteral(0));BinaryOp(!=,Id(b),IntLiteral(1));BinaryOp(=,Id(b),BinaryOp(*,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,FloatType),BinaryOp(=,Id(b),Id(a)),For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(*,Id(b),IntLiteral(2)));Block([VarDecl(c,StringType),BinaryOp(=,Id(c),Id(b)),If(Id(c),Block([VarDecl(a,FloatType),VarDecl(d,StringType),BinaryOp(=,Id(d),Id(c))]))]))]))]))])),For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(10)),IntLiteral(0));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(2)),IntLiteral(0)),Block([For(BinaryOp(=,Id(b),IntLiteral(0));BinaryOp(!=,Id(b),IntLiteral(1));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,FloatType),BinaryOp(=,Id(b),Id(a)),For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));Block([VarDecl(c,StringType),BinaryOp(=,Id(c),Id(b)),If(BinaryOp(==,BinaryOp(%,Id(b),Id(c)),IntLiteral(2)),Block([BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))]))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,342))
    
    def test_Forstatement9(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (a=100;a>10;a=a/2){
                    for(b=100;b==0;b=b/2){
                        b = b + 1;
                        a = false + true - false % true || false && true % false;
                        321414 / a + 312314.e6 - 312441 % 2311414;
                        b = a + 1;
                        sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                        4 / 3 + 234 + a%3214 - 13.2141;
                    }
                }
                for(d=1;d!=1;d=d+1){
                    int e;
                    e = d;
                    b = b + 1;
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(a),IntLiteral(100));BinaryOp(>,Id(a),IntLiteral(10));BinaryOp(=,Id(a),BinaryOp(/,Id(a),IntLiteral(2)));Block([For(BinaryOp(=,Id(b),IntLiteral(100));BinaryOp(==,Id(b),IntLiteral(0));BinaryOp(=,Id(b),BinaryOp(/,Id(b),IntLiteral(2)));Block([BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))])),For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(!=,Id(d),IntLiteral(1));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(d)),BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,343))
    
    def test_Forstatement10(self):
        """ Test For Statement """
        input = r"""
            int main(){
                for (a=1;a<10;a=a*2){
                    for(b=2;b==10;b=b*2){
                        int a;
                        string b;
                        b = a + 1;
                    }
                }
                for(d=1;d!=1;d=d+1){
                    int e;
                    e = d;
                }
                for(c=100;c!=0;c=c%2){
                    for(d=1000;d>0;d=d%10){
                        int e;
                        e = d;
                        string d;
                        d = e;
                    }
                }
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(10));BinaryOp(=,Id(a),BinaryOp(*,Id(a),IntLiteral(2)));Block([For(BinaryOp(=,Id(b),IntLiteral(2));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(*,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,StringType),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1)))]))])),For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(!=,Id(d),IntLiteral(1));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(d))])),For(BinaryOp(=,Id(c),IntLiteral(100));BinaryOp(!=,Id(c),IntLiteral(0));BinaryOp(=,Id(c),BinaryOp(%,Id(c),IntLiteral(2)));Block([For(BinaryOp(=,Id(d),IntLiteral(1000));BinaryOp(>,Id(d),IntLiteral(0));BinaryOp(=,Id(d),BinaryOp(%,Id(d),IntLiteral(10)));Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(d)),VarDecl(d,StringType),BinaryOp(=,Id(d),Id(e))]))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,344))
    
    def test_Dowhilestatement1(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do print("Hung"); while(true);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)])],BooleanLiteral(true))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,345))
    
    def test_Dowhilestatement2(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do print("Hung"); a + 2; while(true/false = false%true + a - b % 100);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),BinaryOp(+,Id(a),IntLiteral(2))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,346))
    
    def test_Dowhilestatement3(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1)))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,347))
    
    def test_Dowhilestatement4(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                {
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                }
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))])],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,348))
    
    def test_Dowhilestatement5(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                {
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                }
                break;
                {
                    a = false + true - false % true || false && true % false;
                    321414 / a + 312314.e6 - 312441 % 2311414;
                    b = a + 1;
                    sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                    4 / 3 + 234 + a%3214 - 13.2141;
                }
                return;
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]),Break(),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]),Return()],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,349))
    
    def test_Dowhilestatement6(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                do 
                    print("Hung"); 
                    if (a + 2%4) 
                        a = 1; 
                while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1)))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,350))
    
    def test_Dowhilestatement7(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                do 
                    print("Hung"); 
                    if (a + 2%4) 
                        a = 1; 
                while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
                for (i = 1; -i < 10; i = i + 1)
                {
                    for (j = 1; j < 200; j = j + 1)
                    {
                        if (i == j = a - b + c % 4312 - 1e231)
                        {
                            a = false + true - false % true || false && true % false;
                            321414 / a + 312314.e6 - 312441 % 2311414;
                            b = a + 1;
                            sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                            4 / 3 + 234 + a%3214 - 13.2141;
                        }
                    }   
                }
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1)))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321))))),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,UnaryOp(-,Id(i)),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(200));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([If(BinaryOp(=,BinaryOp(==,Id(i),Id(j)),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),Id(b)),BinaryOp(%,Id(c),IntLiteral(4312))),FloatLiteral(1e+231))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))]))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,351))
    
    def test_Dowhilestatement8(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                break;
                continue;
                return;
                do 
                    print("Hung"); 
                    if (a + 2%4) 
                        a = 1; 
                    do 
                        print("Hung"); 
                        if (a + 2%4) 
                            a = 1; 
                    while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
                    for (i = 1; -i < 10; i = i + 1)
                    {
                        for (j = 1; j < 200; j = j + 1)
                        {
                            if (i == j = a - b + c % 4312 - 1e231)
                            {
                                a = false + true - false % true || false && true % false;
                                321414 / a + 312314.e6 - 312441 % 2311414;
                                b = a + 1;
                                sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                                4 / 3 + 234 + a%3214 - 13.2141;
                            }
                        }   
                    }
                while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
                for ( x = 1 ; x < 3 ; x = x + 1 ) a = a + 2;
                (sd1____4121_____112dsa___[13241] + 229132 - 212.2313);
                a + 123.4 - 241%a - true/false + false == 0;
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Break(),Continue(),Return(),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1)))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321))))),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,UnaryOp(-,Id(i)),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(200));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([If(BinaryOp(=,BinaryOp(==,Id(i),Id(j)),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),Id(b)),BinaryOp(%,Id(c),IntLiteral(4312))),FloatLiteral(1e+231))),Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))]))]))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321))))),For(BinaryOp(=,Id(x),IntLiteral(1));BinaryOp(<,Id(x),IntLiteral(3));BinaryOp(=,Id(x),BinaryOp(+,Id(x),IntLiteral(1)));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(2)))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(==,BinaryOp(+,BinaryOp(-,BinaryOp(-,BinaryOp(+,Id(a),FloatLiteral(123.4)),BinaryOp(%,IntLiteral(241),Id(a))),BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false))),BooleanLiteral(false)),IntLiteral(0))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,352))
    
    def test_Dowhilestatement9(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                if (true){
                    if (a == true){
                        if (!a){
                            a = false;
                            string b;
                            b = a;
                        }
                        else{
                            string b;
                            b = a;
                        }
                    }
                    else{
                        a = false;
                    }
                }
                for (i = 1; i < 1002401; i = i + 1)
                {
                    for (j = 1; j < 1002401; j = j + 1)
                    {
                        for (k = 1; k < 1002401; k = k + 1)
                        {
                            for (h = 1; h < 1002401; h = h + 1)
                            {
                                for (t = 1; t < 1002401; t = t + 1)
                                {
                                    for (a = 1; a < 1002401; a = a + 1)
                                    {
                                        for (b = 1; b < 1002401; b = b + 1)
                                            for (c = 1; c < 1002401; c = c + 1)
                                                for (d = 1; d < 1002401; d = d + 1)
                                                    for (e = 1; e < 1002401; e = e + 1)
                                                        for (f = 1; f < 1002401; f = f + 1)
                                                            (i + j + k + h + t + a + b + c + d + e + f);
                                    }
                                }
                            }
                        }
                    }
                }
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]),Block([VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]))]),Block([BinaryOp(=,Id(a),BooleanLiteral(false))]))])),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,Id(i),IntLiteral(1002401));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(1002401));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([For(BinaryOp(=,Id(k),IntLiteral(1));BinaryOp(<,Id(k),IntLiteral(1002401));BinaryOp(=,Id(k),BinaryOp(+,Id(k),IntLiteral(1)));Block([For(BinaryOp(=,Id(h),IntLiteral(1));BinaryOp(<,Id(h),IntLiteral(1002401));BinaryOp(=,Id(h),BinaryOp(+,Id(h),IntLiteral(1)));Block([For(BinaryOp(=,Id(t),IntLiteral(1));BinaryOp(<,Id(t),IntLiteral(1002401));BinaryOp(=,Id(t),BinaryOp(+,Id(t),IntLiteral(1)));Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(1002401));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(<,Id(b),IntLiteral(1002401));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));For(BinaryOp(=,Id(c),IntLiteral(1));BinaryOp(<,Id(c),IntLiteral(1002401));BinaryOp(=,Id(c),BinaryOp(+,Id(c),IntLiteral(1)));For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(<,Id(d),IntLiteral(1002401));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));For(BinaryOp(=,Id(e),IntLiteral(1));BinaryOp(<,Id(e),IntLiteral(1002401));BinaryOp(=,Id(e),BinaryOp(+,Id(e),IntLiteral(1)));For(BinaryOp(=,Id(f),IntLiteral(1));BinaryOp(<,Id(f),IntLiteral(1002401));BinaryOp(=,Id(f),BinaryOp(+,Id(f),IntLiteral(1)));BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,Id(i),Id(j)),Id(k)),Id(h)),Id(t)),Id(a)),Id(b)),Id(c)),Id(d)),Id(e)),Id(f)))))))]))]))]))]))]))]))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,353))
    
    def test_Dowhilestatement10(self):
        """ Test Do-while Statement """
        input = r"""int main(){
            do 
                if (true){
                    if (a == true){
                        if (!a){
                            a = false;
                            string b;
                            b = a;
                            if (b){
                                boolean c;
                                c = b;
                                if (!c){
                                    int d;
                                    d = c;
                                    if (d == c || !c){
                                        string e;
                                        e = d;
                                    }
                                    else{
                                        string e;
                                        e = d;
                                    }
                                }
                                else{
                                    int d;
                                    d = e;
                                    boolean t;
                                    t = e;
                                    if (d && !e){
                                        string t;
                                        t = d;
                                    }
                                }
                            }
                        }
                        else{
                            if ((a == b || c != b) && a > b){
                                int e;
                                e = a;
                            }
                        }
                    }
                }
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([Dowhile([If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a)),If(Id(b),Block([VarDecl(c,BoolType),BinaryOp(=,Id(c),Id(b)),If(UnaryOp(!,Id(c)),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(c)),If(BinaryOp(||,BinaryOp(==,Id(d),Id(c)),UnaryOp(!,Id(c))),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]))]),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(e)),VarDecl(t,BoolType),BinaryOp(=,Id(t),Id(e)),If(BinaryOp(&&,Id(d),UnaryOp(!,Id(e))),Block([VarDecl(t,StringType),BinaryOp(=,Id(t),Id(d))]))]))]))]),Block([If(BinaryOp(&&,BinaryOp(||,BinaryOp(==,Id(a),Id(b)),BinaryOp(!=,Id(c),Id(b))),BinaryOp(>,Id(a),Id(b))),Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(a))]))]))]))]))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,354))
    
    def test_BreakStatement(self):
        """ Test Break Statement """
        input = r"""void main() {
            for(i = 1+a[6];a[i]>=(2/5);i = 10/i) break;
            }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([For(BinaryOp(=,Id(i),BinaryOp(+,IntLiteral(1),ArrayCell(Id(a),IntLiteral(6))));BinaryOp(>=,ArrayCell(Id(a),Id(i)),BinaryOp(/,IntLiteral(2),IntLiteral(5)));BinaryOp(=,Id(i),BinaryOp(/,IntLiteral(10),Id(i)));Break())]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,355))
    
    def test_ContinueStatement(self):
        """ Test Continue Statement """
        input = r"""int main() {
                int a;
                float b;
                string c;
                for (a=1; a % 10 == 0; a=a+1){
                    if (a % 2 == 0){
                        for (b=0; b != 1;b=b+2){
                            int a;
                            float b;
                            b = a;
                            for (b=1;b==10;b=b+1){
                                string c;
                                c = b;
                                if (c){
                                    float a;
                                    string d;
                                    d = c;
                                }
                            }
                        }
                    }
                }
                continue;
            }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),VarDecl(b,FloatType),VarDecl(c,StringType),For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(10)),IntLiteral(0));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(%,Id(a),IntLiteral(2)),IntLiteral(0)),Block([For(BinaryOp(=,Id(b),IntLiteral(0));BinaryOp(!=,Id(b),IntLiteral(1));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,FloatType),BinaryOp(=,Id(b),Id(a)),For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));Block([VarDecl(c,StringType),BinaryOp(=,Id(c),Id(b)),If(Id(c),Block([VarDecl(a,FloatType),VarDecl(d,StringType),BinaryOp(=,Id(d),Id(c))]))]))]))]))])),Continue()]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,356))
    
    def test_ReturnStatement(self):
        """ Test Return Statement """
        input = r"""int main(){
            int a;
            a = true;
            if (true){
                if (a == true){
                    if (!a){
                        a = false;
                        string b;
                        b = a;
                    }
                    else{
                        string b;
                        b = a;
                    }
                }
                else{
                    a = false;
                }
            }
            return;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BooleanLiteral(true)),If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]),Block([VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a))]))]),Block([BinaryOp(=,Id(a),BooleanLiteral(false))]))])),Return()]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,357))

    def test_ExpressionStatement(self):
        """ Test Expression Statement """
        input = r"""int main(){
                for (a=1;a<10;a=a*2){
                    for(b=2;b==10;b=b*2){
                        int a;
                        string b;
                        b = a + 1;
                    }
                }
                for(d=1;d!=1;d=d+1){
                    int e;
                    e = d;
                }
                for(c=100;c!=0;c=c%2){
                    for(d=1000;d>0;d=d%10){
                        int e;
                        e = d;
                        string d;
                        d = e;
                    }
                }
                a = false + true - false % true || false && true % false;
                321414 / a + 312314.e6 - 312441 % 2311414;
                b = a + 1;
                sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
                4 / 3 + 234 + a%3214 - 13.2141;
            }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(10));BinaryOp(=,Id(a),BinaryOp(*,Id(a),IntLiteral(2)));Block([For(BinaryOp(=,Id(b),IntLiteral(2));BinaryOp(==,Id(b),IntLiteral(10));BinaryOp(=,Id(b),BinaryOp(*,Id(b),IntLiteral(2)));Block([VarDecl(a,IntType),VarDecl(b,StringType),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1)))]))])),For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(!=,Id(d),IntLiteral(1));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(d))])),For(BinaryOp(=,Id(c),IntLiteral(100));BinaryOp(!=,Id(c),IntLiteral(0));BinaryOp(=,Id(c),BinaryOp(%,Id(c),IntLiteral(2)));Block([For(BinaryOp(=,Id(d),IntLiteral(1000));BinaryOp(>,Id(d),IntLiteral(0));BinaryOp(=,Id(d),BinaryOp(%,Id(d),IntLiteral(10)));Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(d)),VarDecl(d,StringType),BinaryOp(=,Id(d),Id(e))]))])),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,358))
    
    def test_Statement(self):
        """ Test Statement """
        input = r"""int main(){
            if (true){
                if (a == true){
                    if (!a){
                        a = false;
                        string b;
                        b = a;
                        if (b){
                            boolean c;
                            c = b;
                            if (!c){
                                int d;
                                d = c;
                                if (d == c || !c){
                                    string e;
                                    e = d;
                                }
                                else{
                                    string e;
                                    e = d;
                                }
                            }
                            else{
                                int d;
                                d = e;
                                boolean t;
                                t = e;
                                if (d && !e){
                                    string t;
                                    t = d;
                                }
                            }
                        }
                    }
                    else{
                        if ((a == b || c != b) && a > b){
                            int e;
                            e = a;
                        }
                    }
                }
            }
            do 
                print("Hung"); 
                if (a + 2%4) 
                    a = 1; 
                do 
                    print("Hung"); 
                    if (a + 2%4) 
                        a = 1; 
                while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
            while(true/false = false%true + a - b % 100 = 342 && 3021 - 3213 + a || 321);
            for (i = 1; i < 1002401; i = i + 1)
            {
                for (j = 1; j < 1002401; j = j + 1)
                {
                    for (k = 1; k < 1002401; k = k + 1)
                    {
                        for (h = 1; h < 1002401; h = h + 1)
                        {
                            for (t = 1; t < 1002401; t = t + 1)
                            {
                                for (a = 1; a < 1002401; a = a + 1)
                                {
                                    for (b = 1; b < 1002401; b = b + 1)
                                        for (c = 1; c < 1002401; c = c + 1)
                                            for (d = 1; d < 1002401; d = d + 1)
                                                for (e = 1; e < 1002401; e = e + 1)
                                                    for (f = 1; f < 1002401; f = f + 1)
                                                        (i + j + k + h + t + a + b + c + d + e + f);
                                }
                            }
                        }
                    }
                }
            }
            break;
            continue;
            return;
            return 2;
            (2 + 3 - 1 + a - b / c * d || e - !h);
            {
                int a;
                a = 1;
            }
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([If(BooleanLiteral(true),Block([If(BinaryOp(==,Id(a),BooleanLiteral(true)),Block([If(UnaryOp(!,Id(a)),Block([BinaryOp(=,Id(a),BooleanLiteral(false)),VarDecl(b,StringType),BinaryOp(=,Id(b),Id(a)),If(Id(b),Block([VarDecl(c,BoolType),BinaryOp(=,Id(c),Id(b)),If(UnaryOp(!,Id(c)),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(c)),If(BinaryOp(||,BinaryOp(==,Id(d),Id(c)),UnaryOp(!,Id(c))),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]),Block([VarDecl(e,StringType),BinaryOp(=,Id(e),Id(d))]))]),Block([VarDecl(d,IntType),BinaryOp(=,Id(d),Id(e)),VarDecl(t,BoolType),BinaryOp(=,Id(t),Id(e)),If(BinaryOp(&&,Id(d),UnaryOp(!,Id(e))),Block([VarDecl(t,StringType),BinaryOp(=,Id(t),Id(d))]))]))]))]),Block([If(BinaryOp(&&,BinaryOp(||,BinaryOp(==,Id(a),Id(b)),BinaryOp(!=,Id(c),Id(b))),BinaryOp(>,Id(a),Id(b))),Block([VarDecl(e,IntType),BinaryOp(=,Id(e),Id(a))]))]))]))])),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1))),Dowhile([CallExpr(Id(print),[StringLiteral(Hung)]),If(BinaryOp(+,Id(a),BinaryOp(%,IntLiteral(2),IntLiteral(4))),BinaryOp(=,Id(a),IntLiteral(1)))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321)))))],BinaryOp(=,BinaryOp(/,BooleanLiteral(true),BooleanLiteral(false)),BinaryOp(=,BinaryOp(-,BinaryOp(+,BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true)),Id(a)),BinaryOp(%,Id(b),IntLiteral(100))),BinaryOp(||,BinaryOp(&&,IntLiteral(342),BinaryOp(+,BinaryOp(-,IntLiteral(3021),IntLiteral(3213)),Id(a))),IntLiteral(321))))),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<,Id(i),IntLiteral(1002401));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<,Id(j),IntLiteral(1002401));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([For(BinaryOp(=,Id(k),IntLiteral(1));BinaryOp(<,Id(k),IntLiteral(1002401));BinaryOp(=,Id(k),BinaryOp(+,Id(k),IntLiteral(1)));Block([For(BinaryOp(=,Id(h),IntLiteral(1));BinaryOp(<,Id(h),IntLiteral(1002401));BinaryOp(=,Id(h),BinaryOp(+,Id(h),IntLiteral(1)));Block([For(BinaryOp(=,Id(t),IntLiteral(1));BinaryOp(<,Id(t),IntLiteral(1002401));BinaryOp(=,Id(t),BinaryOp(+,Id(t),IntLiteral(1)));Block([For(BinaryOp(=,Id(a),IntLiteral(1));BinaryOp(<,Id(a),IntLiteral(1002401));BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1)));Block([For(BinaryOp(=,Id(b),IntLiteral(1));BinaryOp(<,Id(b),IntLiteral(1002401));BinaryOp(=,Id(b),BinaryOp(+,Id(b),IntLiteral(1)));For(BinaryOp(=,Id(c),IntLiteral(1));BinaryOp(<,Id(c),IntLiteral(1002401));BinaryOp(=,Id(c),BinaryOp(+,Id(c),IntLiteral(1)));For(BinaryOp(=,Id(d),IntLiteral(1));BinaryOp(<,Id(d),IntLiteral(1002401));BinaryOp(=,Id(d),BinaryOp(+,Id(d),IntLiteral(1)));For(BinaryOp(=,Id(e),IntLiteral(1));BinaryOp(<,Id(e),IntLiteral(1002401));BinaryOp(=,Id(e),BinaryOp(+,Id(e),IntLiteral(1)));For(BinaryOp(=,Id(f),IntLiteral(1));BinaryOp(<,Id(f),IntLiteral(1002401));BinaryOp(=,Id(f),BinaryOp(+,Id(f),IntLiteral(1)));BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(+,Id(i),Id(j)),Id(k)),Id(h)),Id(t)),Id(a)),Id(b)),Id(c)),Id(d)),Id(e)),Id(f)))))))]))]))]))]))]))])),Break(),Continue(),Return(),Return(IntLiteral(2)),BinaryOp(||,BinaryOp(-,BinaryOp(+,BinaryOp(-,BinaryOp(+,IntLiteral(2),IntLiteral(3)),IntLiteral(1)),Id(a)),BinaryOp(*,BinaryOp(/,Id(b),Id(c)),Id(d))),BinaryOp(-,Id(e),UnaryOp(!,Id(h)))),Block([VarDecl(a,IntType),BinaryOp(=,Id(a),IntLiteral(1))])]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,359))
    
    def test_Expression1(self):
        """ Test Expression """
        input = r"""int main(int n){
            n = 1 + n + n * n + n * n * n;
        }"""
        expect = "Program([FuncDecl(Id(main),[VarDecl(n,IntType)],IntType,Block([BinaryOp(=,Id(n),BinaryOp(+,BinaryOp(+,BinaryOp(+,IntLiteral(1),Id(n)),BinaryOp(*,Id(n),Id(n))),BinaryOp(*,BinaryOp(*,Id(n),Id(n)),Id(n))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,360))
    
    def test_Expression2(self):
        """ Test Expression """
        input = r"""int main(int c){
            rlt = 1 + 1 + 1 + c;
        }"""
        expect = "Program([FuncDecl(Id(main),[VarDecl(c,IntType)],IntType,Block([BinaryOp(=,Id(rlt),BinaryOp(+,BinaryOp(+,BinaryOp(+,IntLiteral(1),IntLiteral(1)),IntLiteral(1)),Id(c)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,361))
    
    def test_Expression3(self):
        """ Test Expression """
        input = r"""int main(){
                int a, b;
                float c;
                c = a - b;
                c = c + a;
                if (c == 0)
                    c = 2;
                else 
                    c = true;
                return c;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),VarDecl(b,IntType),VarDecl(c,FloatType),BinaryOp(=,Id(c),BinaryOp(-,Id(a),Id(b))),BinaryOp(=,Id(c),BinaryOp(+,Id(c),Id(a))),If(BinaryOp(==,Id(c),IntLiteral(0)),BinaryOp(=,Id(c),IntLiteral(2)),BinaryOp(=,Id(c),BooleanLiteral(true))),Return(Id(c))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,362))
    
    def test_Expression4(self):
        """ Test Expression """
        input = r"""void main(){
            rlt = 1 + 1 / 2 * 3 % 4 - 5;
            a = true;
            b = rlt + a;
            if (a)
                return;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,Id(rlt),BinaryOp(-,BinaryOp(+,IntLiteral(1),BinaryOp(%,BinaryOp(*,BinaryOp(/,IntLiteral(1),IntLiteral(2)),IntLiteral(3)),IntLiteral(4))),IntLiteral(5))),BinaryOp(=,Id(a),BooleanLiteral(true)),BinaryOp(=,Id(b),BinaryOp(+,Id(rlt),Id(a))),If(Id(a),Return())]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,363))
    
    def test_Expression5(self):
        """ Test Expression """
        input = r"""int main(){
            int a;
            a = (1 % 2 + 3 / 4 - 5 * 40 + !a);
            return a;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BinaryOp(+,BinaryOp(-,BinaryOp(+,BinaryOp(%,IntLiteral(1),IntLiteral(2)),BinaryOp(/,IntLiteral(3),IntLiteral(4))),BinaryOp(*,IntLiteral(5),IntLiteral(40))),UnaryOp(!,Id(a)))),Return(Id(a))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,364))
    
    def test_Expression6(self):
        """ Test Expression """
        input = r"""int main(){
            int a;
            a = 2;
            int c;
            c = 4;
            a = (1 % 2 + 3 / 4 - 5 * 40 + !a) + (a || c - a && c) + (a == 0 || a != 0);
            return (a > c);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),IntLiteral(2)),VarDecl(c,IntType),BinaryOp(=,Id(c),IntLiteral(4)),BinaryOp(=,Id(a),BinaryOp(+,BinaryOp(+,BinaryOp(+,BinaryOp(-,BinaryOp(+,BinaryOp(%,IntLiteral(1),IntLiteral(2)),BinaryOp(/,IntLiteral(3),IntLiteral(4))),BinaryOp(*,IntLiteral(5),IntLiteral(40))),UnaryOp(!,Id(a))),BinaryOp(||,Id(a),BinaryOp(&&,BinaryOp(-,Id(c),Id(a)),Id(c)))),BinaryOp(||,BinaryOp(==,Id(a),IntLiteral(0)),BinaryOp(!=,Id(a),IntLiteral(0))))),Return(BinaryOp(>,Id(a),Id(c)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,365))
    
    def test_Expression7(self):
        """ Test Expression """
        input = r"""void main(){
            s = a + b + c * d;
            d = a && b;
            e = !a;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,Id(s),BinaryOp(+,BinaryOp(+,Id(a),Id(b)),BinaryOp(*,Id(c),Id(d)))),BinaryOp(=,Id(d),BinaryOp(&&,Id(a),Id(b))),BinaryOp(=,Id(e),UnaryOp(!,Id(a)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,366))
    
    def test_Expression8(self):
        """ Test Expression """
        input = r"""void main(){
            a = false + true - false % true || false && true % false;
            321414 / a + 312314.e6 - 312441 % 2311414;
            b = a + 1;
            sd1____4121_____112dsa___[13241] + 229132 - 212.2313;
            4 / 3 + 234 + a%3214 - 13.2141;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(-,BinaryOp(+,BooleanLiteral(false),BooleanLiteral(true)),BinaryOp(%,BooleanLiteral(false),BooleanLiteral(true))),BinaryOp(&&,BooleanLiteral(false),BinaryOp(%,BooleanLiteral(true),BooleanLiteral(false))))),BinaryOp(-,BinaryOp(+,BinaryOp(/,IntLiteral(321414),Id(a)),FloatLiteral(312314000000.0)),BinaryOp(%,IntLiteral(312441),IntLiteral(2311414))),BinaryOp(=,Id(b),BinaryOp(+,Id(a),IntLiteral(1))),BinaryOp(-,BinaryOp(+,ArrayCell(Id(sd1____4121_____112dsa___),IntLiteral(13241)),IntLiteral(229132)),FloatLiteral(212.2313)),BinaryOp(-,BinaryOp(+,BinaryOp(+,BinaryOp(/,IntLiteral(4),IntLiteral(3)),IntLiteral(234)),BinaryOp(%,Id(a),IntLiteral(3214))),FloatLiteral(13.2141))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,367))
    
    def test_Expression9(self):
        """ Test Expression """
        input = r"""int[] main(){
            int a[1];
            if (true) return a;
            else return b;
        }"""
        expect = "Program([FuncDecl(Id(main),[],ArrayTypePointer(IntType),Block([VarDecl(a,ArrayType(IntType,1)),If(BooleanLiteral(true),Return(Id(a)),Return(Id(b)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,368))
    
    def test_Expression10(self):
        """ Test Expression """
        input = r"""boolean[] main(){
            int a[32414];
            if (a[234] + 3214 == 321 * 324 - 3213 / 2341)
                return a[234];
            else
                return a[233];
        }"""
        expect = "Program([FuncDecl(Id(main),[],ArrayTypePointer(BoolType),Block([VarDecl(a,ArrayType(IntType,32414)),If(BinaryOp(==,BinaryOp(+,ArrayCell(Id(a),IntLiteral(234)),IntLiteral(3214)),BinaryOp(-,BinaryOp(*,IntLiteral(321),IntLiteral(324)),BinaryOp(/,IntLiteral(3213),IntLiteral(2341)))),Return(ArrayCell(Id(a),IntLiteral(234))),Return(ArrayCell(Id(a),IntLiteral(233))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,369))
    
    def test_Expression11(self):
        """ Test Expression """
        input = r"""void main(){
            3[3+x] = true[b[2]] +3;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,ArrayCell(IntLiteral(3),BinaryOp(+,IntLiteral(3),Id(x))),BinaryOp(+,ArrayCell(BooleanLiteral(true),ArrayCell(Id(b),IntLiteral(2))),IntLiteral(3)))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,370))
    
    def test_Expression12(self):
        """ Test Expression """
        input = r"""int main(){
            int a;
            a = 24123;
            (a[3123] + a)[3124/123] == false[3214] - 3;
            return (false[24124] - true[a[244]]);
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),IntLiteral(24123)),BinaryOp(==,ArrayCell(BinaryOp(+,ArrayCell(Id(a),IntLiteral(3123)),Id(a)),BinaryOp(/,IntLiteral(3124),IntLiteral(123))),BinaryOp(-,ArrayCell(BooleanLiteral(false),IntLiteral(3214)),IntLiteral(3))),Return(BinaryOp(-,ArrayCell(BooleanLiteral(false),IntLiteral(24124)),ArrayCell(BooleanLiteral(true),ArrayCell(Id(a),IntLiteral(244)))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,371))
    
    def test_Expression13(self):
        """ Test Expression """
        input = r"""void main(){
            float a, b, c, x, y, z;
            a = 9;
            b = 12;
            c = 3;
            x = a - b / 3 + c * 2 - 1;
            y = a - b / (3 + c) * (2 - 1);
            z = a - ( b / (3 + c) * 2) - 1;
            printf (x);
            printf (y);
            printf (z);
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,FloatType),VarDecl(b,FloatType),VarDecl(c,FloatType),VarDecl(x,FloatType),VarDecl(y,FloatType),VarDecl(z,FloatType),BinaryOp(=,Id(a),IntLiteral(9)),BinaryOp(=,Id(b),IntLiteral(12)),BinaryOp(=,Id(c),IntLiteral(3)),BinaryOp(=,Id(x),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),BinaryOp(/,Id(b),IntLiteral(3))),BinaryOp(*,Id(c),IntLiteral(2))),IntLiteral(1))),BinaryOp(=,Id(y),BinaryOp(-,Id(a),BinaryOp(*,BinaryOp(/,Id(b),BinaryOp(+,IntLiteral(3),Id(c))),BinaryOp(-,IntLiteral(2),IntLiteral(1))))),BinaryOp(=,Id(z),BinaryOp(-,BinaryOp(-,Id(a),BinaryOp(*,BinaryOp(/,Id(b),BinaryOp(+,IntLiteral(3),Id(c))),IntLiteral(2))),IntLiteral(1))),CallExpr(Id(printf),[Id(x)]),CallExpr(Id(printf),[Id(y)]),CallExpr(Id(printf),[Id(z)]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,372))
    
    def test_Expression14(self):
        """ Test Expression """
        input = r"""void main(){
            float a, b, c, x, y, z;
            int a1[5], b1[2], c1[24], x1[241], y1[151], z1[415];
            a = 9;
            b = 12;
            c = 3;
            x = a - b / 3 + c * 2 - 1;
            y = a - b / (3 + c) * (2 - 1);
            z = a - ( b / (3 + c) * 2) - 1;
            func(x1[4214]) = func(a[421]) - func(b[421]) / 3 + func(c[51]) * 3 - 1;
            func(y1[23]) = func(a[42]) - func(b[52]) / (3 + c) * (2 - 1);
            func(z1[124]) = func(a[626]) -  (func(b[525]) / (3 + func(c[253])) * 2) - 1;
            return func(x1, y1, z1, a, b, c);
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,FloatType),VarDecl(b,FloatType),VarDecl(c,FloatType),VarDecl(x,FloatType),VarDecl(y,FloatType),VarDecl(z,FloatType),VarDecl(a1,ArrayType(IntType,5)),VarDecl(b1,ArrayType(IntType,2)),VarDecl(c1,ArrayType(IntType,24)),VarDecl(x1,ArrayType(IntType,241)),VarDecl(y1,ArrayType(IntType,151)),VarDecl(z1,ArrayType(IntType,415)),BinaryOp(=,Id(a),IntLiteral(9)),BinaryOp(=,Id(b),IntLiteral(12)),BinaryOp(=,Id(c),IntLiteral(3)),BinaryOp(=,Id(x),BinaryOp(-,BinaryOp(+,BinaryOp(-,Id(a),BinaryOp(/,Id(b),IntLiteral(3))),BinaryOp(*,Id(c),IntLiteral(2))),IntLiteral(1))),BinaryOp(=,Id(y),BinaryOp(-,Id(a),BinaryOp(*,BinaryOp(/,Id(b),BinaryOp(+,IntLiteral(3),Id(c))),BinaryOp(-,IntLiteral(2),IntLiteral(1))))),BinaryOp(=,Id(z),BinaryOp(-,BinaryOp(-,Id(a),BinaryOp(*,BinaryOp(/,Id(b),BinaryOp(+,IntLiteral(3),Id(c))),IntLiteral(2))),IntLiteral(1))),BinaryOp(=,CallExpr(Id(func),[ArrayCell(Id(x1),IntLiteral(4214))]),BinaryOp(-,BinaryOp(+,BinaryOp(-,CallExpr(Id(func),[ArrayCell(Id(a),IntLiteral(421))]),BinaryOp(/,CallExpr(Id(func),[ArrayCell(Id(b),IntLiteral(421))]),IntLiteral(3))),BinaryOp(*,CallExpr(Id(func),[ArrayCell(Id(c),IntLiteral(51))]),IntLiteral(3))),IntLiteral(1))),BinaryOp(=,CallExpr(Id(func),[ArrayCell(Id(y1),IntLiteral(23))]),BinaryOp(-,CallExpr(Id(func),[ArrayCell(Id(a),IntLiteral(42))]),BinaryOp(*,BinaryOp(/,CallExpr(Id(func),[ArrayCell(Id(b),IntLiteral(52))]),BinaryOp(+,IntLiteral(3),Id(c))),BinaryOp(-,IntLiteral(2),IntLiteral(1))))),BinaryOp(=,CallExpr(Id(func),[ArrayCell(Id(z1),IntLiteral(124))]),BinaryOp(-,BinaryOp(-,CallExpr(Id(func),[ArrayCell(Id(a),IntLiteral(626))]),BinaryOp(*,BinaryOp(/,CallExpr(Id(func),[ArrayCell(Id(b),IntLiteral(525))]),BinaryOp(+,IntLiteral(3),CallExpr(Id(func),[ArrayCell(Id(c),IntLiteral(253))]))),IntLiteral(2))),IntLiteral(1))),Return(CallExpr(Id(func),[Id(x1),Id(y1),Id(z1),Id(a),Id(b),Id(c)]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,373))
    
    def test_Expression15(self):
        """ Test Expression """
        input = r"""void main(){
            int a;
            a = a + 1;
            print(a);
            a = a* 1;
            a = a / 1 && 1 || 1 % 1;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),BinaryOp(+,Id(a),IntLiteral(1))),CallExpr(Id(print),[Id(a)]),BinaryOp(=,Id(a),BinaryOp(*,Id(a),IntLiteral(1))),BinaryOp(=,Id(a),BinaryOp(||,BinaryOp(&&,BinaryOp(/,Id(a),IntLiteral(1)),IntLiteral(1)),BinaryOp(%,IntLiteral(1),IntLiteral(1))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,374))
    
    def test_Expression16(self):
        """ Test Expression """
        input = r"""int main(){
            4134 = 123.e2 || true && false + func(2341, 32, a, a[44]);
            a = 1434 - 41e2 / fact(342, a[4342]) * a[432] / (2 + 5) - !a[432];
            432 = -a[4324];
            return 3241;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([BinaryOp(=,IntLiteral(4134),BinaryOp(||,FloatLiteral(12300.0),BinaryOp(&&,BooleanLiteral(true),BinaryOp(+,BooleanLiteral(false),CallExpr(Id(func),[IntLiteral(2341),IntLiteral(32),Id(a),ArrayCell(Id(a),IntLiteral(44))]))))),BinaryOp(=,Id(a),BinaryOp(-,BinaryOp(-,IntLiteral(1434),BinaryOp(/,BinaryOp(*,BinaryOp(/,FloatLiteral(4100.0),CallExpr(Id(fact),[IntLiteral(342),ArrayCell(Id(a),IntLiteral(4342))])),ArrayCell(Id(a),IntLiteral(432))),BinaryOp(+,IntLiteral(2),IntLiteral(5)))),UnaryOp(!,ArrayCell(Id(a),IntLiteral(432))))),BinaryOp(=,IntLiteral(432),UnaryOp(-,ArrayCell(Id(a),IntLiteral(4324)))),Return(IntLiteral(3241))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,375))
    
    def test_Expression17(self):
        """ Test Expression """
        input = r"""void main(){
            a = !(a && b || c);
            e = a / b *c / (10 * c % d);
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,Id(a),UnaryOp(!,BinaryOp(||,BinaryOp(&&,Id(a),Id(b)),Id(c)))),BinaryOp(=,Id(e),BinaryOp(/,BinaryOp(*,BinaryOp(/,Id(a),Id(b)),Id(c)),BinaryOp(%,BinaryOp(*,IntLiteral(10),Id(c)),Id(d))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,376))
    
    def test_Expression18(self):
        """ Test Expression """
        input = r"""void b(int a[],int b){
            int a;
            a = 1;
            println(a);
            {
                int b;
                b = 1;
                println(b);
            }
        }"""
        expect = "Program([FuncDecl(Id(b),[VarDecl(a,ArrayTypePointer(IntType)),VarDecl(b,IntType)],VoidType,Block([VarDecl(a,IntType),BinaryOp(=,Id(a),IntLiteral(1)),CallExpr(Id(println),[Id(a)]),Block([VarDecl(b,IntType),BinaryOp(=,Id(b),IntLiteral(1)),CallExpr(Id(println),[Id(b)])])]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,377))
    
    def test_Expression19(self):
        """ Test Expression """
        input = r"""int main () {
            putIntLn(4);
            ar[12];
            foo(a[10],r);
            break;continue;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([CallExpr(Id(putIntLn),[IntLiteral(4)]),ArrayCell(Id(ar),IntLiteral(12)),CallExpr(Id(foo),[ArrayCell(Id(a),IntLiteral(10)),Id(r)]),Break(),Continue()]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,378))
    
    def test_Expression20(self):
        """ Test Expression """
        input = r"""void main(){
            a = !(a && b || c);
            e = a / b *c / (10 * c % d) && !(1) || (1) / 2;
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([BinaryOp(=,Id(a),UnaryOp(!,BinaryOp(||,BinaryOp(&&,Id(a),Id(b)),Id(c)))),BinaryOp(=,Id(e),BinaryOp(||,BinaryOp(&&,BinaryOp(/,BinaryOp(*,BinaryOp(/,Id(a),Id(b)),Id(c)),BinaryOp(%,BinaryOp(*,IntLiteral(10),Id(c)),Id(d))),UnaryOp(!,IntLiteral(1))),BinaryOp(/,IntLiteral(1),IntLiteral(2))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,379))
    
    def test_Overall1(self):
        """ Test Overall """
        input = r"""int main(){
            int x;
            float y, z;
            string g, h;
            float arr_nodes[1000];
        }
        int x1, s2, ____dsaas2123____;
        int a, b;
        float g, f, t;
        void main(int a, float c, boolean d[]){
            float a,b, c;
            boolean p;
            string p;
            int i, j;
            boolean fafs___[2000];
            main(23, 3124);
        }
        """
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(x,IntType),VarDecl(y,FloatType),VarDecl(z,FloatType),VarDecl(g,StringType),VarDecl(h,StringType),VarDecl(arr_nodes,ArrayType(FloatType,1000))])),VarDecl(x1,IntType),VarDecl(s2,IntType),VarDecl(____dsaas2123____,IntType),VarDecl(a,IntType),VarDecl(b,IntType),VarDecl(g,FloatType),VarDecl(f,FloatType),VarDecl(t,FloatType),FuncDecl(Id(main),[VarDecl(a,IntType),VarDecl(c,FloatType),VarDecl(d,ArrayTypePointer(BoolType))],VoidType,Block([VarDecl(a,FloatType),VarDecl(b,FloatType),VarDecl(c,FloatType),VarDecl(p,BoolType),VarDecl(p,StringType),VarDecl(i,IntType),VarDecl(j,IntType),VarDecl(fafs___,ArrayType(BoolType,2000)),CallExpr(Id(main),[IntLiteral(23),IntLiteral(3124)])]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,380))
    
    def test_Overall2(self):
        """ Test Overall """
        input = r"""int func1(int n){
            if (n == 0)
                return 1;
            else
                return n * func1(n - 1);
        }
        int func2(int x, int y){
            if (x > y)
                return func1(x)/func1(y);
            else
                return func1(y)/func1(x);
        }
        """
        expect = "Program([FuncDecl(Id(func1),[VarDecl(n,IntType)],IntType,Block([If(BinaryOp(==,Id(n),IntLiteral(0)),Return(IntLiteral(1)),Return(BinaryOp(*,Id(n),CallExpr(Id(func1),[BinaryOp(-,Id(n),IntLiteral(1))]))))])),FuncDecl(Id(func2),[VarDecl(x,IntType),VarDecl(y,IntType)],IntType,Block([If(BinaryOp(>,Id(x),Id(y)),Return(BinaryOp(/,CallExpr(Id(func1),[Id(x)]),CallExpr(Id(func1),[Id(y)]))),Return(BinaryOp(/,CallExpr(Id(func1),[Id(y)]),CallExpr(Id(func1),[Id(x)]))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,381))
    
    def test_Overall3(self):
        """ Test Overall """
        input = r"""int func1(int n){
            if (n == 0)
                return 1;
            else
                return n * func1(n - 1);
        }
        int func2(int x){
            return func1(2)[3+x] = a[b[f+y[2]]] + 3;
        }"""
        expect = "Program([FuncDecl(Id(func1),[VarDecl(n,IntType)],IntType,Block([If(BinaryOp(==,Id(n),IntLiteral(0)),Return(IntLiteral(1)),Return(BinaryOp(*,Id(n),CallExpr(Id(func1),[BinaryOp(-,Id(n),IntLiteral(1))]))))])),FuncDecl(Id(func2),[VarDecl(x,IntType)],IntType,Block([Return(BinaryOp(=,ArrayCell(CallExpr(Id(func1),[IntLiteral(2)]),BinaryOp(+,IntLiteral(3),Id(x))),BinaryOp(+,ArrayCell(Id(a),ArrayCell(Id(b),BinaryOp(+,Id(f),ArrayCell(Id(y),IntLiteral(2))))),IntLiteral(3))))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,382))
    
    def test_Overall4(self):
        """ Test Overall """
        input = r"""int foo () {
            if (a+1) {{{{if(b+a) foo();}}}} else {if (c+d) t+a; else func(a(b(c)))[f+6*d()];}
        }"""
        expect = "Program([FuncDecl(Id(foo),[],IntType,Block([If(BinaryOp(+,Id(a),IntLiteral(1)),Block([Block([Block([Block([If(BinaryOp(+,Id(b),Id(a)),CallExpr(Id(foo),[]))])])])]),Block([If(BinaryOp(+,Id(c),Id(d)),BinaryOp(+,Id(t),Id(a)),ArrayCell(CallExpr(Id(func),[CallExpr(Id(a),[CallExpr(Id(b),[Id(c)])])]),BinaryOp(+,Id(f),BinaryOp(*,IntLiteral(6),CallExpr(Id(d),[])))))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,383))
    
    def test_Overall5(self):
        """ Test Overall """
        input = r"""void main(){
            int oddSum, evenSum,arr[10],i;
            oddSum = evenSum =0;
            for(i=0;i<10;i=i+1)
                arr[i]=i;
            for(i=0;i<10;i=i+1){
                if(arr[i]%2==0)
                    evenSum = evenSum + arr[i];
                else
                    oddSum = oddSum + arr[i];
            }        
        }"""
        expect = "Program([FuncDecl(Id(main),[],VoidType,Block([VarDecl(oddSum,IntType),VarDecl(evenSum,IntType),VarDecl(arr,ArrayType(IntType,10)),VarDecl(i,IntType),BinaryOp(=,Id(oddSum),BinaryOp(=,Id(evenSum),IntLiteral(0))),For(BinaryOp(=,Id(i),IntLiteral(0));BinaryOp(<,Id(i),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));BinaryOp(=,ArrayCell(Id(arr),Id(i)),Id(i))),For(BinaryOp(=,Id(i),IntLiteral(0));BinaryOp(<,Id(i),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(%,ArrayCell(Id(arr),Id(i)),IntLiteral(2)),IntLiteral(0)),BinaryOp(=,Id(evenSum),BinaryOp(+,Id(evenSum),ArrayCell(Id(arr),Id(i)))),BinaryOp(=,Id(oddSum),BinaryOp(+,Id(oddSum),ArrayCell(Id(arr),Id(i)))))]))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,384))
    
    def test_Overall6(self):
        """ Test Overall """
        input = r"""int main()
        {
            int i, n;
            int S;
            S = 0;
            i = 1;
            do
            {
                S = S + i;
                i = i + 1;
            }while(i <= n);
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(i,IntType),VarDecl(n,IntType),VarDecl(S,IntType),BinaryOp(=,Id(S),IntLiteral(0)),BinaryOp(=,Id(i),IntLiteral(1)),Dowhile([Block([BinaryOp(=,Id(S),BinaryOp(+,Id(S),Id(i))),BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)))])],BinaryOp(<=,Id(i),Id(n))),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,385))
    
    def test_Overall7(self):
        """ Test Overall """
        input = r"""int main()
            {
                int i, n;
                float x, S, T;
                float M;
                do
                {
                    if(n < 1)
                    {
                        a + b - 3213;
                    }
                }while(n < 1);
                S = 0;
                T = 1;
                M = 0;
                i = 1;

                do
                {
                    T = T * x;
                    M = M + i;
                    S = S + pow(-1, i) * T/M;
                    i = i + 1;
                }
                while(i <= n);
                getch();
                return 0;
            }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(i,IntType),VarDecl(n,IntType),VarDecl(x,FloatType),VarDecl(S,FloatType),VarDecl(T,FloatType),VarDecl(M,FloatType),Dowhile([Block([If(BinaryOp(<,Id(n),IntLiteral(1)),Block([BinaryOp(-,BinaryOp(+,Id(a),Id(b)),IntLiteral(3213))]))])],BinaryOp(<,Id(n),IntLiteral(1))),BinaryOp(=,Id(S),IntLiteral(0)),BinaryOp(=,Id(T),IntLiteral(1)),BinaryOp(=,Id(M),IntLiteral(0)),BinaryOp(=,Id(i),IntLiteral(1)),Dowhile([Block([BinaryOp(=,Id(T),BinaryOp(*,Id(T),Id(x))),BinaryOp(=,Id(M),BinaryOp(+,Id(M),Id(i))),BinaryOp(=,Id(S),BinaryOp(+,Id(S),BinaryOp(/,BinaryOp(*,CallExpr(Id(pow),[UnaryOp(-,IntLiteral(1)),Id(i)]),Id(T)),Id(M)))),BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)))])],BinaryOp(<=,Id(i),Id(n))),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,386))

    def test_Overall8(self):
        """ Test Overall """
        input = r"""
            int KiemTraNamNhuan(int nam)
            {
                return (nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0);
            }
            int TimSoNgayTrongThang(int thang, int nam)
            {
                int NgayTrongThang;
                if (thang == 2)
                {
                    int Check;
                    Check = KiemTraNamNhuan(nam);
                    if(Check == 1)
                    {
                        NgayTrongThang = 29;
                    }
                    else
                    {
                        NgayTrongThang = 28;
                    }
                }
                else
                {
                    NgayTrongThang = 31;
                }
                return NgayTrongThang;
            }

            void TimNgayKeTiep(int Ngay, int Thang, int Nam)
            {
                int NgayTrongThang;
                NgayTrongThang = TimSoNgayTrongThang(Thang, Nam);
                if (Ngay < NgayTrongThang) 
                {
                    Ngay = Ngay + 1;
                }
                else if (Thang < 12)     
                {
                    Ngay = 1; Thang = Thang + 1;
                }
                else                      
                {
                    Ngay = Thang = 1;
                    Nam = Nam + 1;
                }
                printf("Ngay ke tiep la: %d - %d - %d ", Ngay, Thang, Nam);
            }
            int main()
            {
                int Ngay, Thang, Nam;

                do
                {
                    printf("Nhap vao nam: ");
                    if(Nam < minYear || Nam > maxYear)
                    {
                        printf("Du lieu nam khong hop le. Xin kiem tra lai!");
                    }
                }while(Nam < minYear || Nam > maxYear);

                do
                {
                    printf("Nhap vao thang: ");
                    if (Thang < 1 || Thang > 12)
                        printf("Du lieu thang khong hop le. Xin kiem tra lai!");
                }while(Thang < 1 || Thang > 12);

                int NgayTrongThang;
                NgayTrongThang = TimSoNgayTrongThang(Thang, Nam);
                do
                {
                    printf("Nhap vao ngay: ");
                    if(Ngay < 1 || Ngay > NgayTrongThang)
                    {
                        printf("Du lieu ngay khong hop le. Xin kiem tra lai!");
                    }
                }while(Ngay < 1 || Ngay > NgayTrongThang);
                
                TimNgayKeTiep(Ngay, Thang, Nam);

                getch();
                return 0;
            }"""
        expect = "Program([FuncDecl(Id(KiemTraNamNhuan),[VarDecl(nam,IntType)],IntType,Block([Return(BinaryOp(||,BinaryOp(&&,BinaryOp(==,BinaryOp(%,Id(nam),IntLiteral(4)),IntLiteral(0)),BinaryOp(!=,BinaryOp(%,Id(nam),IntLiteral(100)),IntLiteral(0))),BinaryOp(==,BinaryOp(%,Id(nam),IntLiteral(400)),IntLiteral(0))))])),FuncDecl(Id(TimSoNgayTrongThang),[VarDecl(thang,IntType),VarDecl(nam,IntType)],IntType,Block([VarDecl(NgayTrongThang,IntType),If(BinaryOp(==,Id(thang),IntLiteral(2)),Block([VarDecl(Check,IntType),BinaryOp(=,Id(Check),CallExpr(Id(KiemTraNamNhuan),[Id(nam)])),If(BinaryOp(==,Id(Check),IntLiteral(1)),Block([BinaryOp(=,Id(NgayTrongThang),IntLiteral(29))]),Block([BinaryOp(=,Id(NgayTrongThang),IntLiteral(28))]))]),Block([BinaryOp(=,Id(NgayTrongThang),IntLiteral(31))])),Return(Id(NgayTrongThang))])),FuncDecl(Id(TimNgayKeTiep),[VarDecl(Ngay,IntType),VarDecl(Thang,IntType),VarDecl(Nam,IntType)],VoidType,Block([VarDecl(NgayTrongThang,IntType),BinaryOp(=,Id(NgayTrongThang),CallExpr(Id(TimSoNgayTrongThang),[Id(Thang),Id(Nam)])),If(BinaryOp(<,Id(Ngay),Id(NgayTrongThang)),Block([BinaryOp(=,Id(Ngay),BinaryOp(+,Id(Ngay),IntLiteral(1)))]),If(BinaryOp(<,Id(Thang),IntLiteral(12)),Block([BinaryOp(=,Id(Ngay),IntLiteral(1)),BinaryOp(=,Id(Thang),BinaryOp(+,Id(Thang),IntLiteral(1)))]),Block([BinaryOp(=,Id(Ngay),BinaryOp(=,Id(Thang),IntLiteral(1))),BinaryOp(=,Id(Nam),BinaryOp(+,Id(Nam),IntLiteral(1)))]))),CallExpr(Id(printf),[StringLiteral(Ngay ke tiep la: %d - %d - %d ),Id(Ngay),Id(Thang),Id(Nam)])])),FuncDecl(Id(main),[],IntType,Block([VarDecl(Ngay,IntType),VarDecl(Thang,IntType),VarDecl(Nam,IntType),Dowhile([Block([CallExpr(Id(printf),[StringLiteral(Nhap vao nam: )]),If(BinaryOp(||,BinaryOp(<,Id(Nam),Id(minYear)),BinaryOp(>,Id(Nam),Id(maxYear))),Block([CallExpr(Id(printf),[StringLiteral(Du lieu nam khong hop le. Xin kiem tra lai!)])]))])],BinaryOp(||,BinaryOp(<,Id(Nam),Id(minYear)),BinaryOp(>,Id(Nam),Id(maxYear)))),Dowhile([Block([CallExpr(Id(printf),[StringLiteral(Nhap vao thang: )]),If(BinaryOp(||,BinaryOp(<,Id(Thang),IntLiteral(1)),BinaryOp(>,Id(Thang),IntLiteral(12))),CallExpr(Id(printf),[StringLiteral(Du lieu thang khong hop le. Xin kiem tra lai!)]))])],BinaryOp(||,BinaryOp(<,Id(Thang),IntLiteral(1)),BinaryOp(>,Id(Thang),IntLiteral(12)))),VarDecl(NgayTrongThang,IntType),BinaryOp(=,Id(NgayTrongThang),CallExpr(Id(TimSoNgayTrongThang),[Id(Thang),Id(Nam)])),Dowhile([Block([CallExpr(Id(printf),[StringLiteral(Nhap vao ngay: )]),If(BinaryOp(||,BinaryOp(<,Id(Ngay),IntLiteral(1)),BinaryOp(>,Id(Ngay),Id(NgayTrongThang))),Block([CallExpr(Id(printf),[StringLiteral(Du lieu ngay khong hop le. Xin kiem tra lai!)])]))])],BinaryOp(||,BinaryOp(<,Id(Ngay),IntLiteral(1)),BinaryOp(>,Id(Ngay),Id(NgayTrongThang)))),CallExpr(Id(TimNgayKeTiep),[Id(Ngay),Id(Thang),Id(Nam)]),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,387))
    
    def test_Overall9(self):
        """ Test Overall """
        input = r"""
        int main()
        {
            int i, j, k;
            for (i = 0; i <= 200; i = i + 1)
                for (j = 0; j <= 100; j = j + 1)
                    for (k = 0; k <= 40; k = k + 1)
                        if (i * 1000 + j * 2000 + k * 5000 == 200000)
                            printf("%d to 1000(VND) -  %d to 2000(VND) - %d to 5000(VND) ", i, j, k);
        
            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(i,IntType),VarDecl(j,IntType),VarDecl(k,IntType),For(BinaryOp(=,Id(i),IntLiteral(0));BinaryOp(<=,Id(i),IntLiteral(200));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));For(BinaryOp(=,Id(j),IntLiteral(0));BinaryOp(<=,Id(j),IntLiteral(100));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));For(BinaryOp(=,Id(k),IntLiteral(0));BinaryOp(<=,Id(k),IntLiteral(40));BinaryOp(=,Id(k),BinaryOp(+,Id(k),IntLiteral(1)));If(BinaryOp(==,BinaryOp(+,BinaryOp(+,BinaryOp(*,Id(i),IntLiteral(1000)),BinaryOp(*,Id(j),IntLiteral(2000))),BinaryOp(*,Id(k),IntLiteral(5000))),IntLiteral(200000)),CallExpr(Id(printf),[StringLiteral(%d to 1000(VND) -  %d to 2000(VND) - %d to 5000(VND) ),Id(i),Id(j),Id(k)]))))),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,388))
    
    def test_Overall10(self):
        """ Test Overall """
        input = r"""
        float Power_n(float x, int n)
        {
            float result;
            result = 1;
            do
            {
                n = n - 1;
                result = result * x;
            }while(n > 0);
            return result;
        }
        float qPower_n(float x, int n)
        {
            float result;
            result = 1;
            do
            {
                if(n % 2 == 1)
                {
                    result = result * x;
                }
                x = x * x;
                n = n / 2;
            }while(n);
            return result;
        }
        int main()
        {
            float x;
            x = 4;
            int n;
            n = 2;
            float z;
            z = qPower_n(x, n);
            printf("z = %f", z);

            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(Power_n),[VarDecl(x,FloatType),VarDecl(n,IntType)],FloatType,Block([VarDecl(result,FloatType),BinaryOp(=,Id(result),IntLiteral(1)),Dowhile([Block([BinaryOp(=,Id(n),BinaryOp(-,Id(n),IntLiteral(1))),BinaryOp(=,Id(result),BinaryOp(*,Id(result),Id(x)))])],BinaryOp(>,Id(n),IntLiteral(0))),Return(Id(result))])),FuncDecl(Id(qPower_n),[VarDecl(x,FloatType),VarDecl(n,IntType)],FloatType,Block([VarDecl(result,FloatType),BinaryOp(=,Id(result),IntLiteral(1)),Dowhile([Block([If(BinaryOp(==,BinaryOp(%,Id(n),IntLiteral(2)),IntLiteral(1)),Block([BinaryOp(=,Id(result),BinaryOp(*,Id(result),Id(x)))])),BinaryOp(=,Id(x),BinaryOp(*,Id(x),Id(x))),BinaryOp(=,Id(n),BinaryOp(/,Id(n),IntLiteral(2)))])],Id(n)),Return(Id(result))])),FuncDecl(Id(main),[],IntType,Block([VarDecl(x,FloatType),BinaryOp(=,Id(x),IntLiteral(4)),VarDecl(n,IntType),BinaryOp(=,Id(n),IntLiteral(2)),VarDecl(z,FloatType),BinaryOp(=,Id(z),CallExpr(Id(qPower_n),[Id(x),Id(n)])),CallExpr(Id(printf),[StringLiteral(z = %f),Id(z)]),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,389))
    
    def test_Overall11(self):
        """ Test Overall """
        input = r"""
        float Power_n(float x, int n)
        {
            float result;
            result = 1;
            do
            {
                result = result * x;
            }while(n);
            return result;
        }
        float qPower_n(float x, int n)
        {
            float result;
            result = 1;
            do
            {
                if(n % 2 == 1)
                {
                    result = result * x;
                }
                x = x * x;
                n = n / 2;
            }while(n);
            return result;
        }
        int main()
        {
            float x;
            x = 4;
            int n;
            n = 2;
            float z;
            z = qPower_n(x, n);
            printf("z = %f", z);

            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(Power_n),[VarDecl(x,FloatType),VarDecl(n,IntType)],FloatType,Block([VarDecl(result,FloatType),BinaryOp(=,Id(result),IntLiteral(1)),Dowhile([Block([BinaryOp(=,Id(result),BinaryOp(*,Id(result),Id(x)))])],Id(n)),Return(Id(result))])),FuncDecl(Id(qPower_n),[VarDecl(x,FloatType),VarDecl(n,IntType)],FloatType,Block([VarDecl(result,FloatType),BinaryOp(=,Id(result),IntLiteral(1)),Dowhile([Block([If(BinaryOp(==,BinaryOp(%,Id(n),IntLiteral(2)),IntLiteral(1)),Block([BinaryOp(=,Id(result),BinaryOp(*,Id(result),Id(x)))])),BinaryOp(=,Id(x),BinaryOp(*,Id(x),Id(x))),BinaryOp(=,Id(n),BinaryOp(/,Id(n),IntLiteral(2)))])],Id(n)),Return(Id(result))])),FuncDecl(Id(main),[],IntType,Block([VarDecl(x,FloatType),BinaryOp(=,Id(x),IntLiteral(4)),VarDecl(n,IntType),BinaryOp(=,Id(n),IntLiteral(2)),VarDecl(z,FloatType),BinaryOp(=,Id(z),CallExpr(Id(qPower_n),[Id(x),Id(n)])),CallExpr(Id(printf),[StringLiteral(z = %f),Id(z)]),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,390))
    
    def test_Overall12(self):
        """ Test Overall """
        input = r"""
        int main()
        {
            int i, j;
            for(i = 1; i <= 10; i = i + 1)
            {
                for(j = 2; j <= 9; j = j + 1)
                {
                    printf("%c %d x %2d = %2d", 179, j, i, i * j);
                }
                printf("%c", 179);
            }

            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(i,IntType),VarDecl(j,IntType),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<=,Id(i),IntLiteral(10));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(2));BinaryOp(<=,Id(j),IntLiteral(9));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));Block([CallExpr(Id(printf),[StringLiteral(%c %d x %2d = %2d),IntLiteral(179),Id(j),Id(i),BinaryOp(*,Id(i),Id(j))])])),CallExpr(Id(printf),[StringLiteral(%c),IntLiteral(179)])])),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,391))
    
    def test_Overall13(self):
        """ Test Overall """
        input = r"""
        int main()
        {
            int a, b;
            printf("Nhap a: ");


            printf("Nhap b: ");


            printf("UCLN cua %d va %d la: ", a, b);
            do
            {
                if(a > b)
                    a = a - b;
                else
                    b = b - a;
            }while(a != b);
            printf("%d", a);
            
            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),VarDecl(b,IntType),CallExpr(Id(printf),[StringLiteral(Nhap a: )]),CallExpr(Id(printf),[StringLiteral(Nhap b: )]),CallExpr(Id(printf),[StringLiteral(UCLN cua %d va %d la: ),Id(a),Id(b)]),Dowhile([Block([If(BinaryOp(>,Id(a),Id(b)),BinaryOp(=,Id(a),BinaryOp(-,Id(a),Id(b))),BinaryOp(=,Id(b),BinaryOp(-,Id(b),Id(a))))])],BinaryOp(!=,Id(a),Id(b))),CallExpr(Id(printf),[StringLiteral(%d),Id(a)]),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,392))
    
    def test_Overall14(self):
        """ Test Overall """
        input = r"""
        int main()
        {
            float a, b, c; 

            printf("Nhap vao a = ");

            printf("Nhap vao b = ");

            printf("Nhap vao c = ");

            if (a == 0)
            {
                if (b == 0) 
                {
                    if (c == 0)
                    {
                        printf("Phuong trinh co vo so nghiem");
                    }
                    else
                    {
                        printf("Phuong trinh vo nghiem");
                    }
                }
                else
                {

                    float x;
                    x = -c/b;
                    printf("Phuong trinh co nghiem duy nhat x = %f", x);
                }
            }
            else
            {
                float Denta;
                Denta = b * b - 4 * a * c;
                if (Denta < 0)
                {
                    printf("Phuong trinh vo nghiem");
                }
                else 
                {
                    float x1;
                    x1 = (-b + sqrt(Denta)) / (2 * a);
                    float x2;
                    x2 = (-b - sqrt(Denta)) / (2 * a);
                    printf("Phuong trinh co 2 nghiem phan biet:x1 = %fx2 = %f", x1, x2);
                }
            }
            getch();
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,FloatType),VarDecl(b,FloatType),VarDecl(c,FloatType),CallExpr(Id(printf),[StringLiteral(Nhap vao a = )]),CallExpr(Id(printf),[StringLiteral(Nhap vao b = )]),CallExpr(Id(printf),[StringLiteral(Nhap vao c = )]),If(BinaryOp(==,Id(a),IntLiteral(0)),Block([If(BinaryOp(==,Id(b),IntLiteral(0)),Block([If(BinaryOp(==,Id(c),IntLiteral(0)),Block([CallExpr(Id(printf),[StringLiteral(Phuong trinh co vo so nghiem)])]),Block([CallExpr(Id(printf),[StringLiteral(Phuong trinh vo nghiem)])]))]),Block([VarDecl(x,FloatType),BinaryOp(=,Id(x),BinaryOp(/,UnaryOp(-,Id(c)),Id(b))),CallExpr(Id(printf),[StringLiteral(Phuong trinh co nghiem duy nhat x = %f),Id(x)])]))]),Block([VarDecl(Denta,FloatType),BinaryOp(=,Id(Denta),BinaryOp(-,BinaryOp(*,Id(b),Id(b)),BinaryOp(*,BinaryOp(*,IntLiteral(4),Id(a)),Id(c)))),If(BinaryOp(<,Id(Denta),IntLiteral(0)),Block([CallExpr(Id(printf),[StringLiteral(Phuong trinh vo nghiem)])]),Block([VarDecl(x1,FloatType),BinaryOp(=,Id(x1),BinaryOp(/,BinaryOp(+,UnaryOp(-,Id(b)),CallExpr(Id(sqrt),[Id(Denta)])),BinaryOp(*,IntLiteral(2),Id(a)))),VarDecl(x2,FloatType),BinaryOp(=,Id(x2),BinaryOp(/,BinaryOp(-,UnaryOp(-,Id(b)),CallExpr(Id(sqrt),[Id(Denta)])),BinaryOp(*,IntLiteral(2),Id(a)))),CallExpr(Id(printf),[StringLiteral(Phuong trinh co 2 nghiem phan biet:x1 = %fx2 = %f),Id(x1),Id(x2)])]))])),CallExpr(Id(getch),[]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,393))
    
    def test_Overall15(self):
        """ Test Overall """
        input = r"""
        int factorial(int n) {
            int f;

            for(f = 1; n > 1; n = n - 1)
                f = f*n;

            return f;
        }

        int npr(int n,int r) {
            return factorial(n)/factorial(n-r);
        }

        int main() {
            int n, r;

            n = 4;
            r = 3;
            
            printf("Tinh hoan vi:");
            printf("%dp%d = %d ", n, r, npr(n,r));

            return 0;
        }"""
        expect = "Program([FuncDecl(Id(factorial),[VarDecl(n,IntType)],IntType,Block([VarDecl(f,IntType),For(BinaryOp(=,Id(f),IntLiteral(1));BinaryOp(>,Id(n),IntLiteral(1));BinaryOp(=,Id(n),BinaryOp(-,Id(n),IntLiteral(1)));BinaryOp(=,Id(f),BinaryOp(*,Id(f),Id(n)))),Return(Id(f))])),FuncDecl(Id(npr),[VarDecl(n,IntType),VarDecl(r,IntType)],IntType,Block([Return(BinaryOp(/,CallExpr(Id(factorial),[Id(n)]),CallExpr(Id(factorial),[BinaryOp(-,Id(n),Id(r))])))])),FuncDecl(Id(main),[],IntType,Block([VarDecl(n,IntType),VarDecl(r,IntType),BinaryOp(=,Id(n),IntLiteral(4)),BinaryOp(=,Id(r),IntLiteral(3)),CallExpr(Id(printf),[StringLiteral(Tinh hoan vi:)]),CallExpr(Id(printf),[StringLiteral(%dp%d = %d ),Id(n),Id(r),CallExpr(Id(npr),[Id(n),Id(r)])]),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,394))
    
    def test_Overall16(self):
        """ Test Overall """
        input = r"""
        int main() {
            int n,i,j,k;

            k = 1;

            n = 5;

            printf("Ve tam giac Floyd: ");
            for(i = 1; i <= n; i = i + 1) {
                for(j=1;j <= i; j = j + 1)
                    printf("%3d", k = k + 1);
                printf("");
            }
            
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(n,IntType),VarDecl(i,IntType),VarDecl(j,IntType),VarDecl(k,IntType),BinaryOp(=,Id(k),IntLiteral(1)),BinaryOp(=,Id(n),IntLiteral(5)),CallExpr(Id(printf),[StringLiteral(Ve tam giac Floyd: )]),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<=,Id(i),Id(n));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([For(BinaryOp(=,Id(j),IntLiteral(1));BinaryOp(<=,Id(j),Id(i));BinaryOp(=,Id(j),BinaryOp(+,Id(j),IntLiteral(1)));CallExpr(Id(printf),[StringLiteral(%3d),BinaryOp(=,Id(k),BinaryOp(+,Id(k),IntLiteral(1)))])),CallExpr(Id(printf),[StringLiteral()])])),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,395))
    
    def test_Overall17(self):
        """ Test Overall """
        input = r"""
        int main() {
            int even;
            int odd;
            even = 24;
            odd = 31;
            if (even % 2 == 0) {
                printf("%d la so chan", even);
            } else {
                printf("%d la so le", even);
            }
            if (odd % 2 != 0 ) {
                printf("%d la so le", odd);
            } else {
                printf("%d la so chan", odd);
            }
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(even,IntType),VarDecl(odd,IntType),BinaryOp(=,Id(even),IntLiteral(24)),BinaryOp(=,Id(odd),IntLiteral(31)),If(BinaryOp(==,BinaryOp(%,Id(even),IntLiteral(2)),IntLiteral(0)),Block([CallExpr(Id(printf),[StringLiteral(%d la so chan),Id(even)])]),Block([CallExpr(Id(printf),[StringLiteral(%d la so le),Id(even)])])),If(BinaryOp(!=,BinaryOp(%,Id(odd),IntLiteral(2)),IntLiteral(0)),Block([CallExpr(Id(printf),[StringLiteral(%d la so le),Id(odd)])]),Block([CallExpr(Id(printf),[StringLiteral(%d la so chan),Id(odd)])])),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,396))
    
    def test_Overall18(self):
        """ Test Overall """
        input = r"""
        int main() {
            int number;
            number = -2;
            if (number >= 0)
                printf("%d la so duong", number);
            else
                printf("%d la so am", number);
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(number,IntType),BinaryOp(=,Id(number),UnaryOp(-,IntLiteral(2))),If(BinaryOp(>=,Id(number),IntLiteral(0)),CallExpr(Id(printf),[StringLiteral(%d la so duong),Id(number)]),CallExpr(Id(printf),[StringLiteral(%d la so am),Id(number)])),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,397))
    
    def test_Overall19(self):
        """ Test Overall """
        input = r"""
        int main() { 
            int loop, number;
            int prime;
            prime = 1;
            number = 19;

            for(loop = 2; loop < number; loop = loop + 1) {
                if((number % loop) == 0) {
                    prime = 0;
                }
            }

            if (prime == 1)
                printf("So %d la so nguyen to.", number);
            else
                printf("So %d khong phai la so nguyen to.", number);
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(loop,IntType),VarDecl(number,IntType),VarDecl(prime,IntType),BinaryOp(=,Id(prime),IntLiteral(1)),BinaryOp(=,Id(number),IntLiteral(19)),For(BinaryOp(=,Id(loop),IntLiteral(2));BinaryOp(<,Id(loop),Id(number));BinaryOp(=,Id(loop),BinaryOp(+,Id(loop),IntLiteral(1)));Block([If(BinaryOp(==,BinaryOp(%,Id(number),Id(loop)),IntLiteral(0)),Block([BinaryOp(=,Id(prime),IntLiteral(0))]))])),If(BinaryOp(==,Id(prime),IntLiteral(1)),CallExpr(Id(printf),[StringLiteral(So %d la so nguyen to.),Id(number)]),CallExpr(Id(printf),[StringLiteral(So %d khong phai la so nguyen to.),Id(number)])),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,398))
    
    def test_Overall20(self):
        """ Test Overall """
        input = r"""
        int main() {
            int a, b, c, i, n;

            n = 6;

            a = b = 1;
            printf("In day Fibonacci: ");
            printf("%d %d ",a,b);

            for(i = 1; i <= n-2; i = i + 1) {
                c = a + b;
                printf("%d ", c);
                
                a = b;
                b = c;
            }
            
            return 0;
        }"""
        expect = "Program([FuncDecl(Id(main),[],IntType,Block([VarDecl(a,IntType),VarDecl(b,IntType),VarDecl(c,IntType),VarDecl(i,IntType),VarDecl(n,IntType),BinaryOp(=,Id(n),IntLiteral(6)),BinaryOp(=,Id(a),BinaryOp(=,Id(b),IntLiteral(1))),CallExpr(Id(printf),[StringLiteral(In day Fibonacci: )]),CallExpr(Id(printf),[StringLiteral(%d %d ),Id(a),Id(b)]),For(BinaryOp(=,Id(i),IntLiteral(1));BinaryOp(<=,Id(i),BinaryOp(-,Id(n),IntLiteral(2)));BinaryOp(=,Id(i),BinaryOp(+,Id(i),IntLiteral(1)));Block([BinaryOp(=,Id(c),BinaryOp(+,Id(a),Id(b))),CallExpr(Id(printf),[StringLiteral(%d ),Id(c)]),BinaryOp(=,Id(a),Id(b)),BinaryOp(=,Id(b),Id(c))])),Return(IntLiteral(0))]))])"
        self.assertTrue(TestAST.checkASTGen(input,expect,399))

    
    
   
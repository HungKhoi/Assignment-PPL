# ID: 1711611
import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program: int main() {} """
        input = """int main() {}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,201))

    def test_more_complex_program(self):
        """More complex program"""
        input = """int main () {
            putIntLn(4);
        }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,202))
    
    def test_wrong_miss_close(self):
        """Miss ) int main( {}"""
        input = """int main( {}"""
        expect = "Error on line 1 col 10: {"
        self.assertTrue(TestParser.checkParser(input,expect,203))
    
    def test_program_structure(self):
        input = """boolean hung() {}
                   int a;
                   void test() {}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,204))
                
    def test_variable_declaration1(self):
        input = """int a;
                   float b;
                   string c;
                   boolean d;
                   int float;
                """
        expect = "Error on line 5 col 23: float"
        self.assertTrue(TestParser.checkParser(input,expect,205))
    
    def test_variable_declaration2(self):
        input = """int a;
                   float b
                   string c;
                """
        expect = "Error on line 3 col 19: string"
        self.assertTrue(TestParser.checkParser(input,expect,206))
    
    def test_variable_declaration3(self):
        input = """int a;
                   floa b;
                   string c;
                """
        expect = "Error on line 2 col 19: floa"
        self.assertTrue(TestParser.checkParser(input,expect,207))
    
    def test_variable_declaration4(self):
        input = """int a;
                   float b;
                   string c;
                   bool d;
                """
        expect = "Error on line 4 col 19: bool"
        self.assertTrue(TestParser.checkParser(input,expect,208))
    
    def test_variable_declaration5(self):
        input = """int a;
                   float b;
                   string c;
                   boolean d;
                   int 94208;
                """
        expect = "Error on line 5 col 23: 94208"
        self.assertTrue(TestParser.checkParser(input,expect,209))
    
    def test_variable_declaration6(self):
        input = """int a;float b;string c;boolean d[5];
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,210))
    
    def test_variable_declaration7(self):
        input = """int i,j,k[5];
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,211))
    
    def test_variable_declaration8(self):
        input = """int i,j,k[];
                """
        expect = "Error on line 1 col 10: ]"
        self.assertTrue(TestParser.checkParser(input,expect,212))
    
    def test_function_declaration1(self):
        input = """int name () {}
                   int[] name () {}
                   void name () {}
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,213))
    
    def test_function_declaration2(self):
        input = """int[2] name () {}
                """
        expect = "Error on line 1 col 4: 2"
        self.assertTrue(TestParser.checkParser(input,expect,214))
    
    def test_function_declaration3(self):
        input = """void int () {}
                """
        expect = "Error on line 1 col 5: int"
        self.assertTrue(TestParser.checkParser(input,expect,215))
    
    def test_function_declaration4(self):
        input = """void () {}
                """
        expect = "Error on line 1 col 5: ("
        self.assertTrue(TestParser.checkParser(input,expect,216))
        
    def test_function_declaration5(self):
        input = """void a42151(int a, int b, float c, string d[]) {}
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,217))
    
    def test_function_declaration6(self):
        input = """void a42151(int a, b, c, d[]) {}
                """
        expect = "Error on line 1 col 19: b"
        self.assertTrue(TestParser.checkParser(input,expect,218))
    
    def test_function_declaration7(self):
        input = """void a42151(int a, int b, float c, string d[2]) {}
                """
        expect = "Error on line 1 col 44: 2"
        self.assertTrue(TestParser.checkParser(input,expect,219))
    
    def test_function_declaration8(self):
        input = """boolean[] iodoa(int a, int b, float 342, string d[2]) {}
                """
        expect = "Error on line 1 col 36: 342"
        self.assertTrue(TestParser.checkParser(input,expect,220))
    
    def test_statement1(self):
        input = """int hunG()
                {
                    if (1 = 1) {break; continue; return a241;} else return;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,221))
    
    def test_statement2(self):
        input = """int hunG()
                {
                    (1 = 1) {break; continue; return a241;} else return;
                }
                """
        expect = "Error on line 3 col 28: {"
        self.assertTrue(TestParser.checkParser(input,expect,222))
    
    def test_statement3(self):
        input = """int hunG()
                {
                    if (1 = 1) {break; continue; return a241;} return;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,223))
    
    def test_statement4(self):
        input = """int hunG()
                {
                    if {a = 412 + 234 * 2343[42=4]} {break; continue; return a241;} else return;
                }
                """
        expect = "Error on line 3 col 23: {"
        self.assertTrue(TestParser.checkParser(input,expect,224))
    
    def test_statement5(self):
        input = """int hunG()
                {
                    do { a = 4; 4 = 20-10-2;} while (924041 = !23421 - 240 * 24[320 = 2454 - 234]);
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,225))
    
    def test_statement6(self):
        input = """int hunG()
                {
                    do a = 2 d = 2 and Idon'tknow while (924041 = !23421 - 240 * 24[320 = 2454 - 234]);
                }
                """
        expect = "Error on line 3 col 29: d"
        self.assertTrue(TestParser.checkParser(input,expect,226))
    
    def test_statement7(self):
        input = """int hunG()
                {
                    do { a = 4; 4 = 20-10-2;} if you don't understand don't worry;
                }
                """
        expect = "Error on line 3 col 49: you"
        self.assertTrue(TestParser.checkParser(input,expect,227))

    def test_statement8(self):
        input = """int hunG()
                {
                    do while (2 = 425[2424/24-41] - 2324 * 4342 + !(344 + 2324))
                }
                """
        expect = "Error on line 3 col 23: while"
        self.assertTrue(TestParser.checkParser(input,expect,228))

    def test_statement9(self):
        input = """int hunG()
                {
                    for (1 = 1; a = 2; 241[5215]) continue;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,229))
    
    def test_statement10(self):
        input = """int hunG()
                {
                    for (a = 2; 123[424]) {dllds = 2414;}
                }
                """
        expect = "Error on line 3 col 40: )"
        self.assertTrue(TestParser.checkParser(input,expect,230))
    
    def test_statement11(self):
        input = """int hunG()
                {
                    for (1 = 1; a = 2; 241[5215]) continue;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,231))
    
    def test_statement12(self):
        input = """int hunG()
                {
                    a = x;
                    x = a;
                    a = 2;
                    int i;
                    for (i = 0; i < i + 1; i = i + 1) hunG();
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,232))
    
    def test_statement13(self):
        input = """int hunG()
                {
                    do {2 = 3;} if (a == 2) a = 3; else a = a + 1; a = 2; while (a == a);
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,233))
    
    def test_statement14(self):
        input = """int hunG()
                {
                    a[42];
                    a[42][415][424] + 2314[23];
                    do break; continue; return; while (4124[4215]);
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,234))

    def test_statement15(self):
        input = """int hunG()
                {
                    (a == 2);
                    if (!21421[245]) {2 = 5;} else continue;
                    for (2314 = 24; 24214; func(2414)[4141]) break;
                    do {func(432)[52352][4324] + 434243;} return a; while a[func(321)[4214]];
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,235))
    
    def test_expression1(self):
        input = """int hunG()
                {
                    a = 2;
                    dsd = 324;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,236))
    
    def test_expression2(self):
        input = """int hunG()
                {
                    for ( x = 1 ; x < 3 ; x = x + 1 ) {
                        a = a + 2 ;
                        break ;
                    }
                    a = 2;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,237))
    
    def test_expression3(self):
        input = """int hunG()
                {
                    int a, b;
                    float c;
                    c = a - b;
                    c = c + a;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,238))
    
    def test_expression4(self):
        input = """int hunG()
                {
                    a = 2 || a4324223 && 392014905e10;
                    a = (a == 2) && (a != 2341);
                    b = 4124124 < a24124 > func(4324[42])[!4234] <= 3214124%2312 >= arr[4214 - 3213];
                }
                """
        expect = "Error on line 5 col 41: >"
        self.assertTrue(TestParser.checkParser(input,expect,239))
    
    def test_expression5(self):
        input = """int hunG()
                {
                    int a;
                    boolean x;
                    a = -69;
                    x = !true;
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,240))
    
    def test_expression6(self):
        input = """
                int a;
                int x;
                x = a + 9;  
                """
        expect = "Error on line 4 col 16: x"
        self.assertTrue(TestParser.checkParser(input,expect,241))
    
    def test_expression7(self):
        input = """int hunG()
                {
                    {
                        int a;
                        a = a * a;
                        return a;
                        void[342];
                    }
                }
                """
        expect = "Error on line 7 col 24: void"
        self.assertTrue(TestParser.checkParser(input,expect,242))
    
    def test_expression8(self):
        input = """int hunG()
                {
                    int a, k[2], b, dsada, c[4];
                    return;
                    a = 2342 + 23241 - 3214 * func(a, 3 - 4, func(a, 3, b[]));
                }
                """
        expect = "Error on line 5 col 74: ]"
        self.assertTrue(TestParser.checkParser(input,expect,243))
    
    def test_expression9(self):
        input = """int hunG()
                {
                    int a, k[2], b, dsada, c[4];
                    return;
                    a = 2341 + 231424[23] - 42421 * 2312**2;
                }
                """
        expect = "Error on line 5 col 57: *"
        self.assertTrue(TestParser.checkParser(input,expect,244))
    
    def test_expression10(self):
        input = """int hunG()
                {
                    // Begin
                    {
                        /* code here */
                        int x;
                        // end
                        sdf = expr[4124]!;
                    }
                }
                """
        expect = "Error on line 8 col 40: !"
        self.assertTrue(TestParser.checkParser(input,expect,245))
    
    def test_expression11(self):
        input = """int hunG()
                {
                    // kdsoakofkoafkoaof
                    4913 = 24141 != 234124;
                    forr[4214] = s3214 + 421424 - 1242;
                    int + 2;
                }
                """
        expect = "Error on line 6 col 24: +"
        self.assertTrue(TestParser.checkParser(input,expect,246))
    
    def test_expression12(self):
        input = """int hunG()
                {
                    /*
                        Comment
                    */
                    4913 = 24141 != 234124 == a42141;
                    forr[4214] = s3214 + 421424%1242;
                    func(4321,24124,(2%4)) != 2314 // 23;
                }
                """
        expect = "Error on line 6 col 43: =="
        self.assertTrue(TestParser.checkParser(input,expect,247))
    
    def test_expression13(self):
        input = """int hunG()
                {
                    {
                        /* daiasjdodfkdo */
                        a = 2;
                        if (a == 2) a = 3; else a % 3;
                    }
                    {
                        int a;
                        a = 4;
                        do {a = 4;} {a = 5;} {a = a[45];} while (32141);
                        abc += 24;
                    }
                }
                """
        expect = "Error on line 12 col 29: ="
        self.assertTrue(TestParser.checkParser(input,expect,248))
    
    def test_expression14(self):
        input = """int hunG()
                {
                    int a, b, c, d;
                    a *= 23;
                }
                """
        expect = "Error on line 4 col 23: ="
        self.assertTrue(TestParser.checkParser(input,expect,249))
    
    def test_expression15(self):
        input = """int hunG()
                {
                    a = 2314;
                    a = a * 23;
                    32 == 324 /= 324;
                }
                """
        expect = "Error on line 5 col 31: ="
        self.assertTrue(TestParser.checkParser(input,expect,250))
    
    def test_stmt_if_nested_parenthesis(self):
        """If statement nested with parenthesis"""
        input = """void main() {
            if ( a == true )
            {
                if ( b == true ) return b;
                else return c;
            }
        }
        """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 251))
    
    def test_stmt_if_semi(self):
        """Missing semicolon inside if else statement"""
        input = """void main() {
    if ( a == true ) return b else return c;
}
"""
        expect = "Error on line 2 col 30: else"
        self.assertTrue(TestParser.checkParser(input, expect, 252))

    def test_stmt_if_block(self):
        """If else statement with block statement"""
        input = """void main() {
    if ( a == true ) { return b; } else { return c; }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 253))

    def test_block_semi(self):
        """Block statements end with semicolon"""
        input = """void main() {
    if ( a == true ) { return b; } else { return c }
}
"""
        expect = "Error on line 2 col 51: }"
        self.assertTrue(TestParser.checkParser(input, expect, 254))

    def test_stmt_if_else_nested(self):
        """If else nested inside else statement"""
        input = """void main() {
    if ( a == true )
        return b;
    else {
        if ( b == true )
            return x;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 255))

    def test_if_else_nested(self):
        """If else nested inside if & else statement"""
        input = """void main() {
    if ( a == true ) {
        if ( b == false )
            return b;
    } else {
        if ( b == true )
            return a;
        else
            return b;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 256))

    def test_do_while_single(self):
        """Do while with 1 statement"""
        input = """void main(int a) {
    do a = a + 1; while a < 10 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 257))

    def test_do_while_many(self):
        """Do while with many statement"""
        input = """void main(int a) {
    do a = a + 1; b = b + 1; while a < 10 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 258))

    def test_do_while_block(self):
        """Do while with block statement"""
        input = """void main(int a) {
    do { a = a + 1; b = b + 1; } while a < 10 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 259))

    def test_do_while_exp(self):
        """Do while with many expression"""
        input = """void main(int a) {
    do a = a + 1; while a < 10; b > 2 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 260))

    def test_do_while_exp_block(self):
        """Do while with many expression in block"""
        input = """void main(int a) {
    do a = a + 1; while { a < 10; b > 2; }
}
"""
        expect = "Error on line 2 col 24: {"
        self.assertTrue(TestParser.checkParser(input, expect, 261))

    def test_do_while_stmt(self):
        """Do while with no statement"""
        input = """void main(int a) {
    do while a < 10 ;
}
"""
        expect = "Error on line 2 col 7: while"
        self.assertTrue(TestParser.checkParser(input, expect, 262))

    def test_do_while_empty_block(self):
        """Do while with empty block statement"""
        input = """void main() {
    do { } while a < 10 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 263))
    # TODO: Do while with empty block statement

    def test_do_while_block_nested(self):
        """Do while nested inside block statement"""
        input = """void main(int a) {
    do {
        do x = x + 1;
        while true ;
    }
    while x < 10 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 264))

    def test_do_while_infinity(self):
        """Do while overflow"""
        input = """void main(int a) {
    do {
        x = x + 1;
    }
    while true ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 265))

    def test_for_stmt(self):
        """For statement standard"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) a = a + 2 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 266))

    def test_for_block(self):
        """For statement with block statement"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
        b = b + 1;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 267))

    def test_for_no_stmt(self):
        """For statement with no statement"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) ;
}
"""
        expect = "Error on line 2 col 38: ;"
        self.assertTrue(TestParser.checkParser(input, expect, 268))

    def test_for_exp(self):
        """For statement with wrong first expression"""
        input = """int hunG() {
    for ( x + 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 269))
    # TODO: For statement with wrong first expression

    def test_for_structure(self):
        """For expression with wrong structures"""
        input = """int hunG() {
    for ( x = 1 , x < 3 , x = x + 1 ) {
        a = a + 2 ;
    }
}
"""
        expect = "Error on line 2 col 16: ,"
        self.assertTrue(TestParser.checkParser(input, expect, 270))

    def test_for_nested(self):
        """For statement nested"""
        input = """int hunG() {
    for ( x = 1 ; x < 5 ; x = x + 1 ) {
        for ( a = 1 ; a < 3 ; a = a + 1 ) b = b * b ;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 271))

    def test_for_missing(self):
        """For statement missing expression 2"""
        input = """int hunG() {
    for ( x = 1 ; ; x = x + 1 ) {
        a = a + 2 ;
        b = b + 1;
    }
}
"""
        expect = "Error on line 2 col 18: ;"
        self.assertTrue(TestParser.checkParser(input, expect, 272))

    def test_for_missing_another(self):
        """For statement missing expression 3"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; ) {
        a = a + 2 ;
    }
}
"""
        expect = "Error on line 2 col 26: )"
        self.assertTrue(TestParser.checkParser(input, expect, 273))

    def test_for_missing_first(self):
        """For statement missing expression 1"""
        input = """int hunG() {
    for ( ; x < 3 ; x = x + 1 ) {
        b = b + 1;
    }
}
"""
        expect = "Error on line 2 col 10: ;"
        self.assertTrue(TestParser.checkParser(input, expect, 274))

    def test_break(self):
        """Break statement"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
        break ;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 275))

    def test_break_loop(self):
        """Break statement appear outside loop"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
    }
    break ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 276))

    def test_break_arg(self):
        """Break statement with argument"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
        break(1);
    }
}
"""
        expect = "Error on line 4 col 13: ("
        self.assertTrue(TestParser.checkParser(input, expect, 277))

    def test_continue(self):
        """Continue statement"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
        continue ;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 278))

    def test_continue_loop(self):
        """Continue statement appear outside of a loop"""
        input = """int hunG() {
    continue ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 279))

    def test_continue_nested(self):
        """Continue statements are nested"""
        input = """int hunG() {
    for ( x = 1 ; x < 3 ; x = x + 1 ) {
        a = a + 2 ;
        continue(continue;);
    }
}
"""
        expect = "Error on line 4 col 16: ("
        self.assertTrue(TestParser.checkParser(input, expect, 280))

    def test_continue_semi(self):
        """Continue statement missed semicolon"""
        input = """int hunG() {
    continue
}
"""
        expect = "Error on line 3 col 0: }"
        self.assertTrue(TestParser.checkParser(input, expect, 281))

    def test_return_void_func(self):
        """Return statement with void type"""
        input = """void main() {
    return ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 282))

    def test_return_value_to_void(self):
        """Return statement with value to void"""
        input = """void main(int x) {
    return x;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 283))

    def test_return_array_pointer(self):
        """Return statement with array pointer type"""
        input = """int[] main() {
    int a[2];
    return a[];
}
"""
        expect = "Error on line 3 col 13: ]"
        self.assertTrue(TestParser.checkParser(input, expect, 284))
    
    def test_random(self):
        input = """int hunG()
                   {
                       /*
                            What is this?
                       */
                        int x;
                        {
                            {
                                {
                                    x = 4;
                                }
                            }
                        }
                   }
                   boolean[] func(boolean a[])
                   {
                       return a[2];
                   }
        """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 285))

    def test_param_with_array(self):
        """Array length in a parameter declaration"""
        input = """void f(int a[5]) {}
"""
        expect = "Error on line 1 col 13: 5"
        self.assertTrue(TestParser.checkParser(input, expect, 286))

    def test_array_pointer_as_param(self):
        """Array pointer in a parameter declaration"""
        input = """void f(int a[]) {}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 287))

    def test_block_stmt_exp(self):
        """Block statement with expressions"""
        input = """int hunG(){
    i = 2;
    a = "true";
    69;
    foo(i, a);
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 288))

    def test_block_block(self):
        """Block of block statement"""
        input = """int hunG(){
    i = 2; {
        a = "true"; {
            69;
        }
    }
    foo(i, a);
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 289))

    def test_intlit(self):
        """Wrong value to integer"""
        input = """void main() {
    int a;
    a = 3.14159;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 290))

    def test_local_scope(self):
        """Local scope"""
        input = """void main() {
    putInt(a);
}
    int foo(){
        int a;
        a = 1;
    }
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 291))

    def test_global_scope(self):
        """Global scope"""
        input = """int a;
void main() {
    a = 0;
    putInt(a);
}
int foo(){
    a = 1;
    return a;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 292))

    def test_if_stmt_for(self):
        """If statement and for statement are nested"""
        input = """void main() {
    if ( a == 0 ) {
        for ( x = 0 ; x < 3 ; x = x + 1 ) a = a - 1;
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 293))

    def test_infinity_loop(self):
        """Infinity loop"""
        input = """void main(int x) {
    int a;
    a = 0;
    for ( x = 0 ; a < 3 ; x = x + 1 ) x = x - 1 ;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 294))

    def test_stmt_after_return(self):
        """Statement after return statement will be ignore"""
        input = """void main() {
    int a;
    a = 2;
    return a;
    a = a + 1;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 295))

    def test_block_stmt_semi(self):
        """Block statement has not end with semicolon"""
        input = """void main() {
    int a;
    int b
}
"""
        expect = "Error on line 4 col 0: }"
        self.assertTrue(TestParser.checkParser(input, expect, 296))

    def test_func_block_stmt(self):
        """Function declaration with many block statement"""
        input = """void main(){
    // first block
    {
        // second block
        {
            // third block
            {
                // fourth block
            }
        }
    }
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 297))

    def test_func_nothing(self):
        """Function does not do anything"""
        input = """int f() {
    return 69;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 298))

    def test_myth(self):
        """Myth program"""
        input = """void main () { int a, b;}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 299))
    
    def test_literal_list(self):
        """List of literal"""
        input = """void main() {
    //declaration
    int a;
    float b;
    string c;
    boolean d;
    //statements
    a = 69;
    b = - 6.9e96;
    c = "this is a";
    d = false;
}
"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 300))

def lstSquare(n:int):
    if (n == 0):
        return []
    return lstSquare(n-1) + [n * n]
print(lstSquare(7))

def lstSquare(n:int):
    return map(lambda x: x*x, range(1, n + 1))

print(lstSquare(7))